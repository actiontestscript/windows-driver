﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using windowsdriver;
using windowsdriver.utils;
using windowsdriver.Utils.Logger;
using windowsdriver.Services.CommandProcessing;
using System.Collections.Generic;
using System.Linq;

public static class DesktopDriver
{
    public static readonly string[] dlls = { "Interop.UIAutomationClient", "FlaUI.Core", "FlaUI.UIA3" };

    private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
    {
        string assemblyName = new AssemblyName(args.Name).Name;
        if (Array.Exists(dlls, element => element == assemblyName))
        {
            return Assembly.LoadFrom(Path.Combine(Application.StartupPath, assemblyName + ".dll"));
        }
        throw new Exception();
    }

    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblySapVersion : Attribute
    {
        public string Value { get; private set; }

        public AssemblySapVersion() : this("") { }
        public AssemblySapVersion(string value) { Value = value; }
    }

    public static int Main(String[] args)
    {
        //-----------------------------------------------------------------------
        // Parse command line
        //-----------------------------------------------------------------------

        var commands = new List<CommandBase>
        {
            new VersionCommand(),
            new LogSilentCommand(),
            new AllWebDriver(),
            new PortCommand(),
            new LocalCommand(),
            new AllowedIpsCommand(),
            new GlobalIpCommand(), 
            new UsageCommand()
        };

        var commandResult = new CommandResult
        {
            ListCommands = commands,
            indexOfHelp = args.Select(arg => arg.ToLower().TrimStart(new char[] { '-', '/' })).ToList().IndexOf("help"),
            port = Network.DEFAULT_PORT,
        };

        if (args.Length > 0)
        {
            foreach (var arg in args)
            {
                var commandToExecute = commands.FirstOrDefault(command => command.CanExecute(arg, args, commandResult));
                if (commandToExecute is null) continue;

                commandToExecute.Execute(arg, commandResult);
            }
        }

        //-----------------------------------------------------------------------
        // Init logger
        //-----------------------------------------------------------------------

        AtsLogger.Init(commandResult.logSilent);

        //-----------------------------------------------------------------------
        // Kill all windowsdrivers running
        //-----------------------------------------------------------------------

        Process currentProcess = Process.GetCurrentProcess();
        int pid = currentProcess.Id;
        string moduleName = currentProcess.MainModule.ModuleName.Replace(".exe", "");

        //-----------------------------------------------------------------------
        // Start application console
        //-----------------------------------------------------------------------

        AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", "libs//" + moduleName + ".exe.config");

        //-----------------------------------------------------------------------------
        // Init desktop and capabilities
        //-----------------------------------------------------------------------------

        DesktopManager desktop = new DesktopManager();
        Capabilities capabilities = new Capabilities(desktop, commandResult);

        //-----------------------------------------------------------------------
        // Start web server
        //-----------------------------------------------------------------------
        /*
        List<IPAddress> localAddresses = Network.GetLocalIPAddresses();
        foreach (IPAddress addr in localAddresses)
        {
            bool isInNetwork = Network.IsIpInNetwork("192.168.0.2", addr.ToString());
            

            Console.WriteLine(addr.ToString());
            Console.WriteLine(isInNetwork);
        }
        */

        /**************** for debug ****************/
//        Console.WriteLine("Appuyez sur une touche pour quitter...");
//        Console.ReadKey();
//        Environment.Exit(0);
        /*****************************************/

        new WebServer(commandResult.port, desktop, capabilities,commandResult).Run();

#if !DEBUG
        Console.SetOut(TextWriter.Null);
        Console.SetError(TextWriter.Null);
#endif

        return 0;
    }

    /*
    private static int GetArgsPort(String[] args)
    {
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i].IndexOf("--port=") == 0)
            {
                String[] dataPort = args[i].Split('=');
                if (dataPort.Length >= 2)
                {
                    if (int.TryParse(dataPort[1], out int port))
                    {
                        if (port >= Network.DEFAULT_PORT && port < IPEndPoint.MaxPort)
                        {
                            if (Network.CheckFreePort(port))
                            {
                                return port;
                            }
                            else
                            {
                                Console.WriteLine("Port {0} already used, Windowsdriver cannot be started !", port);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please select port value between {0} and {1}, Windowsdriver cannot be started !", Network.DEFAULT_PORT, IPEndPoint.MaxPort);
                        }
                        Environment.Exit(0);
                    }
                }
            }
        }

        return Network.DEFAULT_PORT;
    }
 */
}