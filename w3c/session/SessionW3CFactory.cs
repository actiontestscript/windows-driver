﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.session
{
    public class SessionW3CFactory
    {
        public SessionW3c Create(SessionW3c.SessionW3CType? sessionW3CType, HttpListenerContext context)
        {

            //string commandString = SessionW3c.GetStringFromSessionW3CType(sessionW3CType);
            var commandString = "";
            if(sessionW3CType != null)
            {
                commandString = sessionW3CType.ToString().ToLower();
            }
            else
            {
                var urlData = context.Request.Url.AbsolutePath.Substring(1).Split('/');
                if (urlData.Length > 4)
                {
                    commandString = (urlData[2] + urlData[4]).ToLower();
                    sessionW3CType = SessionW3c.GetEnumSessionW3cType(commandString);
                }
                else if (urlData[2].ToLower().Equals(SessionW3c.COOKIE))
                {
                    commandString = urlData[2].ToLower();
                    sessionW3CType = SessionW3c.GetEnumSessionW3cType(commandString);
                }
            }
            
            if(commandString.StartsWith(SessionW3c.WINDOW))
            {
                return new SessionWindow(sessionW3CType,context);
            }
            else if (commandString.Equals(SessionW3c.ELEMENTS))
            {
                return new SessionElements(context);    
            }
            else if (!commandString.Equals(SessionW3c.ELEMENTS) && commandString.StartsWith(SessionW3c.ELEMENT)) {
                return new SessionElement(sessionW3CType, context);
            }
            else if (commandString.StartsWith(SessionW3c.EXECUTE))
            {
                return new SessionExecute(sessionW3CType, context);
            }
            else if (commandString.StartsWith(SessionW3c.SHADOW))
            {
                return new SessionShadow(sessionW3CType, context);
            }
            else if (commandString.StartsWith(SessionW3c.ALERT))
            {
                return new SessionAlert(sessionW3CType, context);
            }
            else if (commandString.StartsWith(SessionW3c.FRAME))
            {
                return new SessionFrame(sessionW3CType, context);
            }

            switch (sessionW3CType)
            {
                case SessionW3c.SessionW3CType.Session:
                    return new SessionSession(context);
                case SessionW3c.SessionW3CType.Status:
                    return new SessionStatus(context);
                case SessionW3c.SessionW3CType.Timeouts:
                    return new SessionTimeouts(context);
                case SessionW3c.SessionW3CType.Url:
                    return new SessionUrl(context);
                case SessionW3c.SessionW3CType.Back:
                    return new SessionBack(context);
                case SessionW3c.SessionW3CType.Forward:
                    return new SessionForward(context);
                case SessionW3c.SessionW3CType.Refresh:
                    return new SessionRefresh(context);
                case SessionW3c.SessionW3CType.Title:
                    return new SessionTitle(context);
                case SessionW3c.SessionW3CType.Source:
                    return new SessionSource(context);
                case SessionW3c.SessionW3CType.Cookie:
                    return new SessionCookie(context);
                case SessionW3c.SessionW3CType.Actions:
                    return new SessionActions(context);
                case SessionW3c.SessionW3CType.Screenshot:
                    return new SessionScreenshot(context);
                case SessionW3c.SessionW3CType.Print:
                    return new SessionPrint(context);
                default:
                    return new SessionW3COthers(context);
            }
        }
    }
}