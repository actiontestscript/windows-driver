﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.session
{
    
    public abstract class SessionW3c : JsonResponse
    {
        public static readonly string WINDOW = "window";
        public static readonly string ELEMENT = "element";
        public static readonly string ELEMENTS = "elements";
        public static readonly string EXECUTE = "execute";
        public static readonly string ALERT = "alert";
        public static readonly string SHADOW = "shadow";
        public static readonly string FRAME = "frame";
        public static readonly string COOKIE = "cookie";

        public enum SessionW3CType
        {
            Session = 0,
            Status = 1,
            Timeouts = 2,
            Url = 3,
            Back = 4,
            Forward = 5,
            Refresh = 6,
            Title = 7,
            Window = 8,
            Frame = 9,
            Element = 10,
            Elements = 11,
            Shadow = 12,
            Source = 13,
            Execute = 14,
            Cookie = 15,
            Actions = 16,
            Alert = 17,
            Screenshot = 18,
            Print = 19,
            WindowHandles = 20,
            WindowNew = 21,
            FrameParent = 22,
            WindowRect = 23,
            WindowMaximize = 24,
            WindowMinimize = 25,
            WindowFullscreen = 26,
            ElementActive = 27,
            ElementShadow = 28,
            ElementElement = 29,
            ElementElements = 30,
            ShadowElement = 31,
            ShadowElements = 32,
            ElementSelected = 33,
            ElementAttribute = 34,
            ElementProperty = 35,
            ElementCss = 36,
            ElementText = 37,
            ElementName = 38,
            ElementRect = 39,
            ElementEnabled = 40,
            ElementComputeDrole = 41,
            ElementComputedLabel = 42,
            ElementClick = 43,
            ElementClear = 44,
            ElementValue = 45,
            ExecuteSync = 46,
            ExecuteAsync = 47,
            AlertDismiss = 48,
            AlertAccept = 49,
            AlertText = 50,
            ElementScreenshot = 51
        };

        public static readonly Dictionary<string, int> SessionW3cTypeDict = new Dictionary<string, int>()
        {
            {"session",0},
            {"status", 1},
            {"timeouts", 2},
            {"url", 3},
            {"back", 4},
            {"forward", 5},
            {"refresh", 6},
            {"title", 7},
            {"window", 8},
            {"frame", 9},
            {"element", 10},
            {"elements", 11},
            {"shadow", 12},
            {"source", 13},
            {"execute", 14},
            {"cookie", 15},
            {"actions", 16},
            {"alert", 17},
            {"screenshot", 18},
            {"print", 19},
            {"windowhandles", 20},
            {"windownew", 21},
            {"frameparent", 22},
            {"windowrect", 23},
            {"windowmaximize", 24},
            {"windowminimize", 25},
            {"windowfullscreen", 26},
            {"elementactive", 27},
            {"elementshadow", 28},
            {"elementelement", 29},
            {"elementelements", 30},
            {"shadowelement", 31},
            {"shadowelements", 32},
            {"elementselected", 33},
            {"elementattribute", 34},
            {"elementproperty", 35},
            {"elementcss", 36},
            {"elementtext", 37},
            {"elementname", 38},
            {"elementrect", 39},
            {"elementenabled", 40},
            {"elementcomputedrole", 41},
            {"elementcomputedlabel", 42},
            {"elementclick", 43},
            {"elementclear", 44},
            {"elementvalue", 45},
            {"executesync", 46},
            {"executeasync", 47},
            {"alertdismiss", 48},
            {"alertaccept", 49},
            {"alerttext", 50},
            {"elementscreenshot", 51}
        };

        public static SessionW3CType? GetEnumSessionW3cType(string type)
        {
            type = type.ToLower();
            SessionW3CType? sessionW3CType = null;
            foreach (var pair in SessionW3cTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    sessionW3CType = (SessionW3CType) pair.Value;
                    break;
                }
            }
            return sessionW3CType;
        }

        public static Dictionary<int, string> InvertDictionary(Dictionary<string, int> dictionary)
        {
            var invertedDict = new Dictionary<int, string>();
            foreach (var item in dictionary)
            {
                if (!invertedDict.ContainsKey(item.Value))
                {
                    invertedDict.Add(item.Value, item.Key);
                }
            }
            return invertedDict;
        }

        public static string GetStringFromSessionW3CType(SessionW3CType? type)
        {
            var invertedDict = InvertDictionary(SessionW3cTypeDict);
            if (invertedDict.TryGetValue((int)type, out var stringDict))
            {
                return stringDict;
            }
            return "";
        }

        public SessionW3c(HttpListenerContext context) : base(context){ }

        public virtual bool Execute()
        {
            return base.SetJsonValue();
        }
        public bool SendErrorMethod()
        {
            SetMsgErrorCode("unknown_method", "Method " + getHttpMethod() + " none implemented");
            Send();
            return true;
        }

        public virtual bool MethodGet()
        {
            Send();
            return true;
        }
        public virtual bool MethodPost()
        {
            Send();
            return true;
        }
        public virtual bool MethodDelete()
        {
            Send();
            return true;
        }
    }
}
