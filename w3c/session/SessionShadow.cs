﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.session
{
    public class SessionShadow : SessionW3c
    {
        protected SessionW3CType? sessionW3cType;
        protected HttpListenerContext context;
        public SessionShadow(SessionW3CType? sessionW3cType, HttpListenerContext context) : base(context)
        {
            this.sessionW3cType = sessionW3cType;
            this.context = context;
        }

        public override bool Execute()
        {
            var ShadowFactory = new windowsdriver.w3c.session.shadow.ShadowFactory();
            var ShadowCommand = ShadowFactory.Create(sessionW3cType, context);
            if (ShadowCommand != null)
            {
                return ShadowCommand.Execute();
            }
            return false;
        }
    }
}
