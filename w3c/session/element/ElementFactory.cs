﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.session.element
{
    public class ElementFactory
    {
        public ElementCommand Create(SessionW3c.SessionW3CType? sessionW3CType, HttpListenerContext context)
        {
            switch (sessionW3CType)
            {
                case SessionW3c.SessionW3CType.Element:
                    return new Element(context);
                case SessionW3c.SessionW3CType.ElementElements:
                    return new ElementElements(context);
                case SessionW3c.SessionW3CType.ElementElement:
                    return new ElementElement(context);
                case SessionW3c.SessionW3CType.ElementActive:
                    return new ElementActive(context);
                case SessionW3c.SessionW3CType.ElementShadow:
                    return new ElementShadow(context);
                case SessionW3c.SessionW3CType.ElementSelected:
                    return new ElementSelected(context);
                case SessionW3c.SessionW3CType.ElementAttribute:
                    return new ElementAttribute(context);
                case SessionW3c.SessionW3CType.ElementProperty:
                    return new ElementProperty(context);
                case SessionW3c.SessionW3CType.ElementText:
                    return new ElementText(context);
                case SessionW3c.SessionW3CType.ElementName:
                    return new ElementName(context);
                case SessionW3c.SessionW3CType.ElementRect:
                    return new ElementRect(context);
                case SessionW3c.SessionW3CType.ElementEnabled:
                    return new ElementEnabled(context);
                case SessionW3c.SessionW3CType.ElementComputeDrole:
                    return new ElementComputeDrole(context);
                case SessionW3c.SessionW3CType.ElementComputedLabel:
                    return new ElementComputedLabel(context);
                case SessionW3c.SessionW3CType.ElementClick:
                    return new ElementClick(context);
                case SessionW3c.SessionW3CType.ElementClear:
                    return new ElementClear(context);
                case SessionW3c.SessionW3CType.ElementValue:
                    return new ElementValue(context);
                case SessionW3c.SessionW3CType.ElementCss:
                    return new ElementCss(context);
                case SessionW3c.SessionW3CType.ElementScreenshot:
                    return new ElementScreenshot(context);
                default:
                    return new ElementOthers(context);
            }
        }
    }
}
