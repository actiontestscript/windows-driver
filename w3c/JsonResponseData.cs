﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Collections.Generic;

namespace windowsdriver.w3c
{
    internal class JsonResponseData
    {
        public class ErrorCode
        {
            public int HttpCode { get; set; }
            public string JsonErrorCode { get; set; }
            public string MessageErrorCode { get; set; }
        }


        private Dictionary<string, ErrorCode> errorMap;

        public JsonResponseData()
        {
            errorMap = new Dictionary<string, ErrorCode>
            {
                {"element_click_intercepted", new ErrorCode { HttpCode = 400, JsonErrorCode = "element click intercepted", MessageErrorCode = "The Element Click command could not be completed because the element receiving the events is obscuring the element that was requested clicked." }},
                {"element_not_interactable", new ErrorCode { HttpCode = 400, JsonErrorCode = "element not interactable", MessageErrorCode = "A command could not be completed because the element is not pointer- or keyboard interactable." }},
                {"insecure_certificate", new ErrorCode { HttpCode = 400, JsonErrorCode = "insecure certificate", MessageErrorCode = "Navigate caused the user agent to hit a certificate warning, which is usually the result of an expired or invalid TLS certificate." }},
                {"invalid_argument", new ErrorCode { HttpCode = 400, JsonErrorCode = "invalid argument", MessageErrorCode = "The arguments passed to a command are either invalid or malformed." }},
                {"invalid_cookie_domain", new ErrorCode { HttpCode = 400, JsonErrorCode = "invalid cookie domain", MessageErrorCode = "An illegal attempt was made to set a cookie under a different domain than the current page." }},
                {"invalid_element_state", new ErrorCode { HttpCode = 400, JsonErrorCode = "invalid element state", MessageErrorCode = "A command could not be completed because the element is in an invalid state, e.g. attempting to click an element that is no longer attached to the document." }},
                {"invalid_selector", new ErrorCode { HttpCode = 400, JsonErrorCode = "invalid selector", MessageErrorCode = "Argument was an invalid selector (e.g. XPath/CSS)." }},
                {"invalid_session_id", new ErrorCode { HttpCode = 404, JsonErrorCode = "invalid session id", MessageErrorCode = "Occurs if the given session id is not in the list of active sessions, meaning the session either does not exist or that it’s not active." }},
                {"javascript_error", new ErrorCode { HttpCode = 500, JsonErrorCode = "javascript error", MessageErrorCode = "An error occurred while executing JavaScript supplied by the user." }},
                { "move_target_out_of_bounds", new ErrorCode { HttpCode = 500, JsonErrorCode = "move target out of bounds" , MessageErrorCode = "The target for mouse interaction is not in the browser’s viewport and cannot be brought into that viewport."} },
                { "no_such_alert", new ErrorCode { HttpCode = 404, JsonErrorCode = "no such alert" ,MessageErrorCode = "An attempt was made to operate on a modal dialog when one was not open."} },
                { "no_such_cookie", new ErrorCode { HttpCode = 404, JsonErrorCode = "no such cookie" ,MessageErrorCode = "No cookie matching the given path name was found amongst the associated cookies of the current browsing context’s active document."} },
                { "no_such_element",new ErrorCode  { HttpCode = 404, JsonErrorCode = "no such element" ,MessageErrorCode = "An element could not be located on the page using the given search parameters."} },
                { "no_such_frame", new ErrorCode { HttpCode = 404, JsonErrorCode = "no such frame" ,MessageErrorCode = "A request to switch to a frame could not be satisfied because the frame could not be found."} },
                { "no_such_window", new ErrorCode  { HttpCode = 404, JsonErrorCode = "no such window" ,MessageErrorCode = "A request to switch to a different window could not be satisfied because the window could not be found."} },
                { "no_such_shadow_root", new ErrorCode  { HttpCode = 404, JsonErrorCode = "no such shadow root" ,MessageErrorCode = "The shadow root of the element could not be found."} },
                { "script_timeout_error", new ErrorCode  { HttpCode = 500, JsonErrorCode = "script timeout" ,MessageErrorCode = "A script did not complete before its timeout expired."} },
                { "session_not_created", new ErrorCode { HttpCode = 500, JsonErrorCode = "session not created" ,MessageErrorCode = "A new session could not be created."} },
                { "stale_element_reference", new ErrorCode { HttpCode = 404, JsonErrorCode = "stale element reference" ,MessageErrorCode = "An element command failed because the referenced element is no longer attached to the DOM."} },
                { "detached_shadow_root",new ErrorCode { HttpCode = 404, JsonErrorCode = "Detached shadow root" ,MessageErrorCode = "The shadow root of the element is detached."} },
                { "timeout", new ErrorCode { HttpCode = 500, JsonErrorCode = "timeout" ,MessageErrorCode = "An operation did not complete before its timeout expired."} },
                { "unable_to_set_cookie", new ErrorCode { HttpCode = 500, JsonErrorCode = "unable to set cookie" ,MessageErrorCode = "A request to set a cookie’s value could not be satisfied."} },
                { "unable_to_capture_screen", new ErrorCode { HttpCode = 500, JsonErrorCode = "unable to capture screen" ,MessageErrorCode = "A screen capture was made impossible."} },
                { "unexpected_alert_open", new ErrorCode { HttpCode = 500, JsonErrorCode = "unexpected alert open" ,MessageErrorCode = "A modal dialog was open, blocking this operation."} },
                { "unknown_command", new ErrorCode { HttpCode = 404, JsonErrorCode = "unknown command" ,MessageErrorCode = "The requested command matched a known URL but did not match an method for that URL."} },
                { "unknown_error", new ErrorCode { HttpCode = 500, JsonErrorCode = "unknown error" ,MessageErrorCode = "An unknown server-side error occurred while processing the command."} },
                { "unknown_method", new ErrorCode { HttpCode = 405, JsonErrorCode = "unknown method" ,MessageErrorCode = "The requested command matched a known URL but did not match an method for that URL."} },
                { "method_delete_not_implemented", new ErrorCode { HttpCode = 405, JsonErrorCode = "method delete not implemented" ,MessageErrorCode = "This Method Delete is not implemented"} },
                { "unsupported_operation", new ErrorCode { HttpCode = 500, JsonErrorCode = "unsupported operation" , MessageErrorCode = "Indicates that a command that should have executed properly cannot be supported for some reason."} },
                //ats error
                { "ats_error_browser_not_found", new ErrorCode { HttpCode = 404, JsonErrorCode = "browser not found", MessageErrorCode = "web browser not found or not installed"} },
                { "ats_error_driver_download", new ErrorCode   { HttpCode = 404,JsonErrorCode = "error download web driver", MessageErrorCode = "web driver download error compatible with your browser"} },
                { "ats_error_driver_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "web driver not found", MessageErrorCode = "web driver not found for your browser"} },
                { "ats_error_unzip_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "unzip not installed", MessageErrorCode = "unzip not found or not installed"} },
                { "ats_error_wget_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "wget not installed", MessageErrorCode = "wget not found or not installed"} },
                { "ats_error_driver_start", new ErrorCode {  HttpCode = 404,JsonErrorCode = "error drivers start", MessageErrorCode = "unknown error driver start"} },
                { "ats_snap_firefox", new ErrorCode {  HttpCode = 404,JsonErrorCode = "firefox snap install", MessageErrorCode = "the greckodriver dont work with firefox snap installation."} },
                { "ats_error_remoteDriver_incorrect", new ErrorCode {  HttpCode = 404,JsonErrorCode = "remoteDriver name incorrect", MessageErrorCode = "RemoteDriver name is incorrect"} },
                { "ats_error_remoteDriver", new ErrorCode {  HttpCode = 404,JsonErrorCode = "remote Driver", MessageErrorCode = "RemoteDriver name is incorrect"} },
                { "ats_error_port_range", new ErrorCode {  HttpCode = 404,JsonErrorCode = "error port range", MessageErrorCode = "port range unavailable"} },
                { "ats_error_profile_userProfile_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "userProfile not found in Json", MessageErrorCode = ""} },
                { "ats_error_profile_browserName_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "browserName not found in Json", MessageErrorCode = ""} },
                { "ats_error_profile_userProfile_directory_not_exist", new ErrorCode {  HttpCode = 404,JsonErrorCode = "userProfile directory not exists", MessageErrorCode = ""} },
                { "ats_error_profile_userProfile_directory_not_writable", new ErrorCode {  HttpCode = 404,JsonErrorCode = "userProfile directory is not writable", MessageErrorCode = ""} },
                { "ats_error_profile_userProfile_directory_not_created", new ErrorCode {  HttpCode = 404,JsonErrorCode = "userProfile directory is not created",MessageErrorCode = ""} },
                { "ats_error_profile_homePath_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "system homePath not found",MessageErrorCode = ""} },
                { "ats_error_profile_firefox_directory_not_found", new ErrorCode {  HttpCode = 404,JsonErrorCode = "firefox profile directory not found ",MessageErrorCode = ""} },
                { "ats_not_enough_space", new ErrorCode {  HttpCode = 507,JsonErrorCode = "not enough space available ",MessageErrorCode = ""} },
                { "ats_directory_not_writable", new ErrorCode {  HttpCode = 403,JsonErrorCode = "directory is not writable ",MessageErrorCode = ""} },
                { "ats_directory_not_created", new ErrorCode {  HttpCode = 403,JsonErrorCode = "directory is not created ",MessageErrorCode = ""} },
                { "ats_json_data_null", new ErrorCode {  HttpCode = 400 ,JsonErrorCode = "Bad Request",MessageErrorCode = "json malformed or null value mandatory"} },
                { "ats_recorder_create_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Create VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_image_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Image VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_value_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Value VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_data_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Data VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_status_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Status VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_element_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Element VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_position_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Position VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_create_mobile_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Create Mobile VisualRecorder",MessageErrorCode = ""} },
                { "ats_recorder_image_mobile_failed", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to Image Mobile VisualRecorder",MessageErrorCode = ""} },
                { "ats_key_enter_error", new ErrorCode {  HttpCode = 500 ,JsonErrorCode = "Failed to key Enter text data command error",MessageErrorCode = ""} },
                { "ats_desktop_response_error", new ErrorCode {  HttpCode = 200 ,JsonErrorCode = "ErrorAtsDesktopResponse",MessageErrorCode = ""} },
                { "ats_remote_agent_locked", new ErrorCode {  HttpCode = 423 ,JsonErrorCode = "remote agent locked",MessageErrorCode = ""} }
            };
        }

        public ErrorCode getErrorCode(string strKey)
        {
            ErrorCode result;
            if(errorMap.TryGetValue(strKey, out result)){
                return result;
            }
            
            return null;
        }
    }
}
