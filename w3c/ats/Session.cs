﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using windowsdriver.Services.RemoteAgentService;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats
{
    public class SessionData
    {
        public bool ats = false;
        public string remoteDriver = null;
        public string applicationPath = null;
        public string operatingSystem = null;
    }
    public class Session : JsonResponse
    {
        private SessionData datas = new SessionData();

        public Session(HttpListenerContext listener, string method, Capabilities caps, List<string> urlData) :
            base(listener)
        {
            Init(listener, method, caps, urlData);
        }
        public Session(HttpListenerContext listener, string method, Capabilities caps, List<string> urlData, string jsonContent) : base(listener,jsonContent)
        {
            Init(listener, method, caps, urlData);
        }

        public void Init(HttpListenerContext listener, string method, Capabilities caps, List<string> urlData)
        {
            if (DELETE.Equals(method))
            {
                urlData.RemoveRange(0, 2);
            }
            else if (GET.Equals(method))
            {
                value = new SessionValue(caps.data);
            }
            else if (POST.Equals(method))
            {
                if (base.SetJsonValue())
                {
                    datas = JsonConvert.DeserializeObject<SessionData>(jsonValue);
                }
                /*
                if (remoteAgentManager.IsLocked())
                {
                    SetMsgErrorCode("ats_error_remoteDriver", string.Format("RemoteDriver is locked by ip '{0}' ", remoteAgentManager.GetLockedBy()));
                    return;
                }
                */
                //remoteAgentManager.LockAgent(remoteAgentManager.GetRequestAddressIp(listener));

                value = CreateSession(caps);
            }
        }

        #region CreateSession
        private object CreateSession(Capabilities caps)
        {

            if (datas.remoteDriver != null)
            {

                RemoteDriver driver = caps.GetRemoteDriver(datas.remoteDriver, datas.applicationPath);

                if (driver != null)
                {
                    if (driver.url != null)
                    {
                        return new SessionValue(
                            driver.id,
                            new Dictionary<string, string>
                                {
                                    { "driverUrl", driver.url },
                                    { "driverVersion", driver.version },
                                    { "driverName", driver.application.name },
                                    { "applicationVersion", driver.application.version },
                                    { "applicationPath", driver.application.path }
                                }
                            );
                    }
                    else
                    {
                        SetMsgErrorCode("ats_error_remoteDriver",string.Format("RemoteDriver '{0}' start failed with error -> {1}", datas.remoteDriver, driver.startError) );
                        return this.value;
                        //return new Error("remote driver", string.Format("RemoteDriver '{0}' start failed with error -> {1}", remoteDriver, driver.startError), "");
                    }
                }
                else
                {
                    SetMsgErrorCode("ats_error_remoteDriver",string.Format("RemoteDriver '{0}' start failed", datas.remoteDriver, driver.startError) );
                    return this.value;
//                    return new Error("remote driver", string.Format("RemoteDriver '{0}' start failed", remoteDriver, driver.startError), "");
                }
            }
            else
            {
                SetMsgErrorCode("ats_json_data_null", "'remoteDriver' not sent in post data");
                return this.value;
//                return new Error("json data error", "'remoteDriver' not sent in post data", "");
            }
        }
        #endregion

        public class SessionValue
        {
            public string sessionId;
            public Dictionary<string, string> capabilities;

            public SessionValue(string sessionId, Dictionary<string, string> capabilities)
            {
                this.sessionId = sessionId;
                this.capabilities = capabilities;
            }

            public SessionValue(Dictionary<string, string> capabilities)
            {
                this.sessionId = "root";
                this.capabilities = capabilities;
            }
        }
    }
}
