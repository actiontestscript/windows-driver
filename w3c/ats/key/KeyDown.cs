﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;

namespace windowsdriver.w3c.ats.key
{
    class KeyDownData
    {
        public int codePoint = -1;
    }
    class KeyDown : Key
    {
        public KeyDown(HttpListenerContext context, ActionKeyboard action, bool keyDown) : base(context, action, keyDown) { }
        private KeyDownData datas = new KeyDownData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<KeyDownData>(jsonValue);
                if (datas.codePoint != -1)
                {
                    action.Down(datas.codePoint.ToString());
                    Send();
                    return true;
                }
                else
                {
                    {
                        SetMsgErrorCode("ats_key_enter_error");
                        //response.SetError(errorCode, "enter text data command error");
                    }
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
