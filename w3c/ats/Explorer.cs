﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats
{
    public class Explorer : JsonResponse
    {
        private static byte[] EXPLORER_ICON = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH5wUeDREppgAGaQAAA +JJREFUWMPlls+LXTUcxT/Jy3SsWnEGOhbr1LEqrXbjyo1bQf8FcVOEoisXgrjWpUVQ6MIfIKLiH+NGdCXiFClOa+nIWDu86cx7+R4Xyc1N7rstUyy48MHlJu8mOed7viffBP7jn/vtS/8+8B6wfJdxBvwCfEy0zzcuoPtFwB8CvBt3FviUib94PxXw0ViOBvfwvPPrF/78fUvBj5f8oeR0VVswM+Mr4Ma/wD4QfBN29+6ZM2sbLy2dPL1xwXuX6MhAEWkGNgM7QDYDVW2bpe8yAOI8svX7zmthd8/hJw/jw0MDIFWQwvnAo8fXeeL0U6yuBjS7WYChI2Cp7SJ4Q6Q3cqAJyKE8fmnJsX5q5Ux47NlXOffCGSYT1wvtACkv3L0NbB/N/0bzaSaYHwlJ/djq//TYwnhkHDkC4ezzJ/DzP1D0OCbgPDiXo7PEWAbE8t/iwougwkZIWktSwgc3BRwOl4BhZJEeVEPQ0egPRxKMkDA7+VUIqJO9Yl0IVSoskrREqCFpoyQlEcDl1Cvjd+D1hH7xNqL6/2GUNpL/4XgRSsQNiYH5ZK30eYHi+mpxjciuEZLKJEMve7X91EqtEm0lc5OOwbh67ijJXsmAtJCCJsrGC0MAO0Saxsb2aQrFlSX6/qMWFq3SMQDWELgYr893Z7yaUGjAh3uWsUWtAr4Xf9iIP4yQGn30QgsG7LdSC3wnf9w1TYMdErrDobi12Ua2GPnQD8MUUaWjAh5Kn+qKCCnPtFWq2oZmhmyEALECqonG0ncyvBvxR0Uo9KZrozczbu/P2Z4+zp97J9MEy6efYg+U+w24DIexenSL1aNXeOCImPjxNIWmUmUSZsaNWyt8feU8n/y1wX5M4IoxqREjWN+WGTTf0nvZZrx74jKvn/uOtWNXmTjrS3om6ltZU0S7e44PL7/NxZtPcyCKq+ttlTKUa0fXl3IcqX9bng+2TvPR929wa28pr9Gq4BnkNkbjh+1X+Ha6lmS1rEx+l34mJFn+lsY2/Zy2z7aP89PVl4m2WMxKCpRNN4/G5vTJPDmDzg7QwX7ygBmKltsRl01KNJxl+c3SvcIySRObO+u8eMqYhEEhktrSaDEytVBNNrT5M9zcASkd3J3Udfku9Sw15o+sMHvwWDaumM5DIlZtR5VCVJ9Sqk61bsIzz0E0ZBHld+k3xmu/dUGUY876StgZMaRLZXsGNBeLnFMVP3R9tWmywdg6gHLadjWgx/DIrjVG7BRpjFUB1Vctu4MJM3gdfTpyF07Vax70JrLrdbl15OpXO774pLo6ONI90uf7pKe/V7r6Ubrrlmudgew68Bb/+98/cLtdX2lFVbMAAAAASUVORK5CYII=");

        public Explorer(HttpListenerContext listener, string method, Capabilities caps, DesktopManager desktop) : base(listener)
        {
            if (!GET.Equals(method))
            {
                return;
            }
            
            var uidName = "ats_" + Guid.NewGuid().ToString();
            var uniqueTempDir = Path.GetFullPath(Path.Combine(Path.GetTempPath(), uidName));
            Directory.CreateDirectory(uniqueTempDir);

            Process.Start("explorer.exe", uniqueTempDir).WaitForExit(2000);

            var win = desktop.GetWindowByTitle(uidName);
            win.App = new AppData("Windows explorer", "explorer.exe", EXPLORER_ICON);

            value = win;
        }
    }
}
