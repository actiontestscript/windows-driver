﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using windowsdriver.items;

namespace windowsdriver.w3c.ats.element
{
    public class ElementParentData
    {
        public string id = null;
    }
    public class ElementParent : Element
    {
        public ElementParent(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementParentData datas = new ElementParentData();
        
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementParentData>(jsonValue);

                if (datas.id != null)
                {
                    element = CachedElements.Instance.GetElementById(datas.id);
                    desktopResponse.Elements = element.GetParents();
                    Dispose();
                    SendDesktopResponse();
                    return true;
                }
                else
                {
                    desktopResponse.SetError(-73, "cached element not found");
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}
