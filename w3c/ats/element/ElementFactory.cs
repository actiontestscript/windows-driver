﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.ats.element
{
    public class ElementFactory
    {
        public Element Create(Element.ElementType? elementType, HttpListenerContext context, DesktopManager desktop)
        {
            switch (elementType)
            {
                case Element.ElementType.Childs:
                    return new ElementChilds(context, desktop);
                case Element.ElementType.Parents:
                    return new ElementParent(context, desktop);
                case Element.ElementType.Find:
                    return new ElementFind(context, desktop);
                case Element.ElementType.Attributes:
                    return new ElementAttributes(context, desktop);
                case Element.ElementType.Select:
                    return new ElementSelect(context, desktop);
                case Element.ElementType.FromPoint:
                    return new ElementFromPoint(context, desktop);
                case Element.ElementType.Script:
                    return new ElementScript(context, desktop);
                case Element.ElementType.Root:
                    return new ElementRoot(context, desktop);
                case Element.ElementType.LoadTree:
                    return new ElementLoadTree(context, desktop);
                case Element.ElementType.ListItems:
                    return new ElementListItems(context, desktop);
                case Element.ElementType.DialogBox:
                    return new ElementDialogBox(context, desktop);
                case Element.ElementType.Focus:
                    return new ElementFocus(context, desktop);
                case Element.ElementType.ContextMenu:
                    return new ElementContextMenu(context, desktop);
                default:
                    return new ElementOthers(context, desktop);
            }
        }
    }
}