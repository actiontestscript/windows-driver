﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using System.Text.RegularExpressions;
using windowsdriver.items;
using FlaUI.Core.AutomationElements;

namespace windowsdriver.w3c.ats.element
{
    public class ElementSelectData
    {
        public string id = null;
        public string type = null;
        public string value = null;
        public string regexp = null;
    }
    public class ElementSelect : Element
    {
        public ElementSelect(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementSelectData datas = new ElementSelectData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementSelectData>(jsonValue);

                if (datas.id != null)
                {
                    element = CachedElements.Instance.GetElementById(datas.id);
                    bool regexp = false;

                    if(datas.regexp != null)
                    {
                        _ = bool.TryParse(datas.regexp, out regexp);
                    }

                    string error = null;
                    if ("index".Equals(datas.type))
                    {
                        _ = int.TryParse(datas.value, out int index);
                        error = element.SelectItem(index, desktop);
                    }
                    else
                    {
                        bool byValue = "value".Equals(datas.type);
                        if (regexp)
                        {
                            Regex rx = new Regex(@datas.value);
                            if (byValue)
                            {
                                error = element.SelectItem((AutomationElement e) => { return e.Patterns.Value.IsSupported && rx.IsMatch(e.Patterns.Value.ToString()); }, desktop);
                            }
                            else
                            {
                                error = element.SelectItem((AutomationElement e) => { return rx.IsMatch(e.Name); }, desktop);
                            }
                        }
                        else
                        {
                            if (byValue)
                            {
                                error = element.SelectItem((AutomationElement e) => { return e.Patterns.Value.IsSupported && e.Patterns.Value.ToString() == datas.value; }, desktop);
                            }
                            else
                            {
                                error = element.SelectItem((AutomationElement e) => { return e.Name == datas.value; }, desktop);
                            }
                        }
                    }

                    if (error != null)
                    {
                        element.SafeClick();
                        desktopResponse.ErrorCode = -199;
                    }

                    desktopResponse.ErrorMessage = error;
                    Dispose();
                    SendDesktopResponse();
                    return true;
                }
                else
                {
                    desktopResponse.SetError(-73, "cached element not found");
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}