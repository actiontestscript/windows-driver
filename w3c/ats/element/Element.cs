﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.ats.element
{
    public abstract class Element : JsonResponse
    {
        public enum ElementType
        {
            Childs = 0,
            Parents = 1,
            Find = 2,
            Attributes = 3,
            Select = 4,
            FromPoint = 5,
            Script = 6,
            Root = 7,
            LoadTree = 8,
            ListItems = 9,
            DialogBox = 10,
            Focus = 11,
            ContextMenu = 12
        };

        public static readonly Dictionary<string, int> elementTypeDict = new Dictionary<string, int>()
    {
        {"Childs", 0},
        {"Parents", 1},
        {"Find", 2},
        {"Attributes", 3},
        {"Select", 4},
        {"FromPoint", 5},
        {"Script", 6},
        {"Root", 7},
        {"LoadTree", 8},
        {"ListItems", 9},
        {"DialogBox", 10},
        {"Focus", 11},
        {"ContextMenu", 12}
    };

        public DesktopManager desktop;
        protected DesktopResponse desktopResponse;
        protected AtsElement element;

        public static ElementType? getEnumElementType(string type)
        {
            type = type.ToLower();
            ElementType? elementType = null;

            foreach (var pair in elementTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    elementType = (ElementType)pair.Value;
                    return elementType; 
                }
            }

            return elementType;
        }

        public Element(HttpListenerContext context, DesktopManager desktop) : base(context)
        {
            this.desktop = desktop;
            this.desktopResponse = new DesktopResponse(context);
        }

        public virtual Boolean Execute()
        {
            return base.SetJsonValue();
        }
        protected void SendDesktopResponse(DesktopWindow[] value)
        {
            desktopResponse.SetWindows(value);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse(DesktopWindow value)
        {
            desktopResponse.SetWindow(value);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse()
        {
            desktopResponse.Send(desktopResponse);
        }

        public AtsElement[] GetAtsElementsArray(HashSet<AtsElement> elements)
        {
            AtsElement[] result = new AtsElement[elements.Count];
            elements.CopyTo(result);
            return result;
        }

        public override void Dispose()
        {
            element = null;
            desktop = null;
            base.Dispose();
        }


    }
}
