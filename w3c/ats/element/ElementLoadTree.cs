﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace windowsdriver.w3c.ats.element
{
    
    public class ElementLoadTreeData
    {
        public int handle = 0;
        public int pid = 0;
    }
    public class ElementLoadTree : Element
    {
        public ElementLoadTree(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementLoadTreeData datas = new ElementLoadTreeData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementLoadTreeData>(jsonValue);
          
                if (datas.handle != 0 )
                {
                    
                    DesktopWindow window = desktop.GetWindowByHandle(datas.handle);
                    
                    if (window == null)
                    {
                        window = desktop.GetWindowIndexByPid(datas.pid, 0);
                    }

                    if (window != null)
                    {
                        window.Focus();
                        Task<AtsElement[]> task = Task.Run(() =>
                        {
                            return window.GetElementsTree(desktop);
                        });

                        task.Wait(TimeSpan.FromSeconds(40));
                        desktopResponse.Elements = task.Result;
                    }
                    else
                    {
                        desktopResponse.Elements = new AtsElement[0];
                    }
                    SendDesktopResponse();
                    return true;
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}