﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using FlaUI.Core.AutomationElements;

namespace windowsdriver.w3c.ats.element
{
    public class ElementContextMenuData
    {
        public int pid = 0;
    }

    /// <summary>
    /// Specific to SAP for menu detection
    /// </summary>
    public class ElementContextMenu : Element
    {
        public ElementContextMenu(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementContextMenuData datas = new ElementContextMenuData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementContextMenuData>(jsonValue);
                AutomationElement[] elements = null;

                if (datas.pid != -1)
                {
                    try
                    {
                        elements = desktop.GetContextMenu();
                    }
                    catch {}

                    Stack<AtsElement> stack = new Stack<AtsElement>();
                    if (elements != null)
                    {
                        foreach (AutomationElement elem in elements)
                        {
                            int idxMenuItem = 0;
                            string baseId = string.Empty;

                            foreach (AutomationElement item in elem.FindAllChildren())
                            {
                                AtsElement menuItem = new AtsElement(item, idxMenuItem);
                                stack.Push(menuItem);
                                idxMenuItem++;
                            }
                        }
                    }
                    desktopResponse.Elements = stack.ToArray();
                    Dispose();
                    SendDesktopResponse();
                    return true;
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}