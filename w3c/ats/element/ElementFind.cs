﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.element
{
    public class ElementFindData
    {
        public int handle = -1;
        public string tag = null;
        public string[] attributes = null;
    }
    public class ElementFind : Element
    {
        public ElementFind(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementFindData datas = new ElementFindData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementFindData>(jsonValue);

                if (datas.tag != null)
                {
                    AtsLogger.WriteAll("element-find -> {0}", datas.tag);
                    if( datas.attributes != null)
                    {
                        DesktopWindow window = desktop.GetWindowByHandle(datas.handle);
                        if (window != null)
                        {
                            window.Focus();
                            desktopResponse.Elements = GetAtsElementsArray(window.GetElements(false, datas.tag, datas.attributes, null, desktop));
                        }
                        else
                        {
                            desktopResponse.Elements = new AtsElement[0];
                        }
                    }
                    SendDesktopResponse();
                    return true;
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}