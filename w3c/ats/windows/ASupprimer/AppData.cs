﻿using System;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;
using FlaUI.Core.AutomationElements;
namespace windowsdriver.w3c.ats
{
 /*   public class AppData
    {
        private static string WINDOWS_ICON = @"iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QArgDOAOl0s3seAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH5wUdCB4FBz4bQAAAAzJJREFUSMetlcuLXEUUxn9Vt+7tx8y03T3PTEZHiQMiuhECbhVXEVf+Df4D7iWLbAVBEFy6ceE+OyUEXIkQER1Ek7iI42RemaSn0337UXWOi3u7b/dMJiahD9w+XYe69dX3fafqmk++DixsHF0eern6eKDvSdBqEMUrBFG8jLIShKlc/Bd8yGto21l+gHDtj5NLv5hPr+9fPumG69uHg5WuF4JAUM1ztrjkeXLsx/VivmhWiy2sVc1ubORDO/RydftwsNILyqxiKHC/o+t94TP7eKDvd72M0RXQPI9/x2MmqsXIAvUIXnYGk1e9Qur1A4doJUjxoqgWIPkzGWslw+tzEVu1iMVaxGuLCZuLCfPzjqEIH311h39sBYAg1FymbbFTZ2DOGl4qwau1iHdWS7zSjPltt8OVtxusL5Yox5ZqKSKyZgp85zjF9FsQKSRVgijO50YBVCx8eWWRNy5UaM7FWAPGGIwB83OP9UbMci15ugEKpK2MQTnB+bw7ACywtVpm5UmLKCjP2ggKvRZCFRfyPgcQY5hZL6kS0lbGIF8fUX0q8xcByUyeYDDLUMD5/MQC9EX59e9H7M7ZMzNv/9smsdCcTwpGp2gdnAzoiU69l5k8ZgAudsSJPaNN5CJc7HCJO2V8EVESTu1LM5NHoE4Nb27Mc7FROkP33mGXtzZrrNQr50qyc5xStqaQRDVj4HMENc8g6ot4IAUgM24jXAg6PsliZ4ugY5PzrfcCbN9rMwxCc6FExZnxfTPaxPOGG32VAA4EPv5un3rvmHqsbK7McWmpwnKjwp8HKRebZRRDkjhKSYSzhshCZM7nmJ/k4vbvENGJ6+x0HvL73RbcbY0nf3trj9WKY6sWs1lLuLBcZbVRYalepl6v0vVKeurD4YaqbVEWpmBtDNUGdB+C+CkX9lLPXur5cT+F2xm4za/4kjE88JNahq51lu9j+wRuUQ5io//VWRTaQTnyMiGTQr9zy0K4tlY1u86cB9IE657/Fhp0H3Dw1zfR0btf7C0lRzfKjuXY6kZstOSM4IxgNWAM2Mhhhl2sDDHqMRJAA0Z9JuHoCR6kn5I++on7259z5+b2f4av5EUIWEuLAAAAAElFTkSuQmCC";

        private string name = null;
        private string build = null;
        private string version = null;
        private string path = null;
        private byte[] icon = null;

        [JsonProperty(PropertyName = "version")]
        public string Version
        {
            get
            {
                return version == string.Empty ? null : version;
            }
            set
            {
                version = value;
            }
        }

        [JsonProperty(PropertyName = "build")]
        public string Build
        {
            get
            {
                return build == string.Empty ? null : build;
            }
            set
            {
                build = value;
            }
        }

        [JsonProperty(PropertyName = "path")]
        public string Path
        {
            get
            {
                return path == string.Empty ? null : path;
            }
            set
            {
                path = value;
            }
        }

        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get
            {
                return name == string.Empty ? null : name;
            }
            set
            {
                name = value;
            }
        }

        [JsonProperty(PropertyName = "icon")]
        public byte[] Icon
        {
            get
            {
                return icon ?? Convert.FromBase64String(WINDOWS_ICON);
            }
            set
            {
                icon = value;
            }
        }

        public AppData()
        {
            name = @"Windows desktop";
        }

        public AppData(Process process)
        {
            try
            {
                FileVersionInfo info = process.MainModule.FileVersionInfo;
                name = info.FileDescription;

                version = info.FileVersion;
                build = info.FileMajorPart + "." + info.FileMinorPart + "." + info.FileBuildPart + "." + info.FilePrivatePart;
                path = process.MainModule.FileName;

                Icon ic = System.Drawing.Icon.ExtractAssociatedIcon(path);
                using (var stream = new MemoryStream())
                {
                    ic.ToBitmap().Save(stream, ImageFormat.Png);
                    icon = stream.ToArray();
                }
            }
            catch { }
        }

        public AppData(string nm)
        {
            name = nm;
        }

        public AppData(AutomationElement elem)
        {
            try
            {
                name = elem.Name;
            }
            catch { }
        }

        public AppData(string nm, string pa, byte[] ic) : this(nm)
        {
            path = pa;
            icon = ic;
        }
    }
 */
}
