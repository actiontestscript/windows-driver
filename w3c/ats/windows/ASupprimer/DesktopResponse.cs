﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace windowsdriver.w3c.ats
{
    /*
    class DesktopResponse : JsonResponse
    {
        private DesktopWindow[] _windows;
        private DesktopData[] _attributes;
        private AtsElement[] _elements;
        private byte[] _image;

        public DesktopWindow[] Windows
        {
            get
            {
                if (_windows == null)
                {
                    return null;
                }

                List<DesktopWindow> wins = new List<DesktopWindow>();
                for (int i = 0; i < _windows.Length; i++)
                {
                    try
                    {
                        if (_windows[i].App != null)
                        {
                            wins.Add(_windows[i]);
                        }
                    }
                    catch { }
                }
                return wins.ToArray();
            }
            set { }
        }

        [JsonProperty(PropertyName = "image", NullValueHandling = NullValueHandling.Include)]
        public byte[] Image
        {
            get
            {
                return _image?.Length == 0 ? null : _image;
            }
            set
            {
                _image = value;
            }
        }

        [JsonProperty(PropertyName = "elements", NullValueHandling = NullValueHandling.Include)]
        public AtsElement[] Elements
        {
            get
            {
                return _elements?.Length == 0 ? null : _elements;
            }
            set
            {
                _elements = value;
            }
        }

        [JsonProperty(PropertyName = "attributes", NullValueHandling = NullValueHandling.Include)]
        public DesktopData[] Attributes
        {
            get
            {
                return _attributes?.Length == 0 ? null : _attributes;
            }
            set
            {
                _attributes = value;
            }
        }

        [JsonProperty(PropertyName = "errorCode")]
        public int ErrorCode;

        private string errorMessage = null;

        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return errorMessage == string.Empty ? null : errorMessage;
            }
            set
            {
                errorMessage = value;
            }
        }

    }
*/
}
