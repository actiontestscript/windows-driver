﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using System.Threading;

namespace windowsdriver.w3c.ats.windows
{
    public class WindowKeysData
    {
        public int handle = -1;
        public string keys = null;
    }

    public class WindowKeys : Window
    {
        public WindowKeys(HttpListenerContext context, ActionKeyboard keyboard, DesktopManager desktop, VisualRecorder recorder) : base(context, keyboard, desktop, recorder) { }
        private WindowKeysData datas = new WindowKeysData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<WindowKeysData>(jsonValue);
                if (datas.handle != -1 && datas.keys != null)
                {
                    window = desktop.GetWindowByHandle(datas.handle);
                    if (window != null)
                    {
                        window.SetMouseFocus(desktop);
                        Thread.Sleep(200);
                    }
                    keyboard.RootKeys(datas.keys);
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
