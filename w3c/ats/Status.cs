﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats
{
    public class Status : JsonResponse
    {
        public Status(HttpListenerContext listener, bool ready, Capabilities caps) : base(listener)
        {
            
            value = new StatusValue(ready, caps);
        }
    }

    public class StatusValue
    {
        public bool ready;
        public string message = "ATS WindowsDriver ready for new sessions.";
        public StatusOs os;
        public StatusBuild build;
        public StatusRemoteAgent remoteAgent;

        public StatusValue(bool ready, Capabilities caps)
        {
            this.ready = ready;
            this.os = new StatusOs(caps);
            this.build = new StatusBuild(caps);
            this.remoteAgent = new StatusRemoteAgent();
        }
    }

    public class StatusOs
    {
        public string arch;
        public string name;
        public string version;
        public string machineName;
        public StatusOs(Capabilities caps)
        {
            this.arch = caps.GetValue("CpuName");
            this.name = caps.GetValue("Name");
            this.version = caps.GetValue("Version");
            this.machineName = caps.GetValue("MachineName");
        }
    }

    public class StatusBuild
    {
        public string version;
        public StatusBuild(Capabilities caps)
        {
            this.version = caps.GetValue("DriverVersion");
        }
    }

    public class StatusRemoteAgent
    {
        public string usedBy;
        public int port;
     
        public StatusRemoteAgent()
        {
            this.usedBy = WsAgent.UsedBy;
            this.port = WsAgent.Port;
        }
    }
}
