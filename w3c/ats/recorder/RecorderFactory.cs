﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.ats.recorder
{
    public class RecorderFactory
    {
        public Recorder Create(Recorder.RecordType? recordType, HttpListenerContext context, VisualRecorder recorder)
        {
            switch (recordType)
            {
                case Recorder.RecordType.Stop:
                    return new RecorderStop(context, recorder);
                case Recorder.RecordType.Screenshot:
                    return new RecorderScreenShot(context, recorder);
                case Recorder.RecordType.Start:
                    return new RecorderStart(context, recorder);
                case Recorder.RecordType.Create:
                    return new RecorderCreate(context, recorder);
                case Recorder.RecordType.Image:
                    return new RecorderImage(context, recorder);
                case Recorder.RecordType.Value:
                    return new RecorderValue(context, recorder);
                case Recorder.RecordType.Data:
                    return new RecorderData(context, recorder);
                case Recorder.RecordType.Status:
                    return new RecorderStatus(context, recorder);
                case Recorder.RecordType.Element:
                    return new RecorderElement(context, recorder);
                case Recorder.RecordType.Position:
                    return new RecorderPosition(context, recorder);
                case Recorder.RecordType.Download:
                    return new RecorderDownload(context, recorder);
                case Recorder.RecordType.ImageMobile:
                    return new RecorderImageMobile(context, recorder);
                case Recorder.RecordType.CreateMobile:
                    return new RecorderCreateMobile(context, recorder);
                case Recorder.RecordType.ScreenshotMobile:
                    return new RecorderScreenShotMobile(context, recorder);
                case Recorder.RecordType.Summary:
                    return new RecorderSummary(context, recorder);
                default:
                    return new RecorderOthers(context, recorder);
            }
        }
    }
}
