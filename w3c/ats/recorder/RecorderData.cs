﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderDataData
    {
        public string v1 = null;
        public string v2 = null;
    }


    class RecorderData : Recorder
    {
        public RecorderData(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderDataData datas = new RecorderDataData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderDataData>(jsonValue);
                AtsLogger.WriteAll("record-data -> v1 and v2");
                if (datas.v1 != null && datas.v2 != null)
                {
                    try
                    {
                        recorder.AddData(datas.v1, datas.v2);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-data-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_recorder_data_failed", ex.Message);
                    }

                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
