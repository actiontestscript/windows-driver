﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    
    class RecorderScreenShotMobileData
    {
        //public DriverInfo driverInfo;
        public int x = 0;
        public int y = 0;
        public int w = 1;
        public int h = 1;
        public string url = null;
        public string uri = null;
    }
    class RecorderScreenShotMobile : Recorder
    {
        public RecorderScreenShotMobile(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderScreenShotMobileData datas = new RecorderScreenShotMobileData();
        private DesktopResponse response = new DesktopResponse();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderScreenShotMobileData>(jsonValue);
                AtsLogger.WriteAll("record-screenshot-mobile -> {0}", datas.uri);
                try
                {
                    response.Image = VisualAction.GetScreenshot(datas.uri);
                    SetValue(response);
                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("record-screenshot-mobile-error -> {0}", ex.Message);
                }
                Send();
                return true;
            }
            Send();
            return false;
        }

    }
}
