﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderStatusData
    {
        public int error = -1;
        public long duration =  -1;

    }
    class RecorderStatus : Recorder
    {
        public RecorderStatus(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderStatusData datas = new RecorderStatusData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderStatusData>(jsonValue);
                if (datas.error != -1 || datas.duration != -1)
                {
                    try
                    {
                        recorder.Status(datas.error, datas.duration);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-status-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_recorder_status_failed", ex.Message);
                    }
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
