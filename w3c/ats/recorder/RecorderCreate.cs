﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderCreateData
    {
        public DriverInfo DriverInfo { get; set; }
        public string actionType = null;
        public int line = 0;
        public string script = null;
        public long timeLine = 0;
        public string channelName = null;

        public double channelDimensionX = 0;
        public double channelDimensionY = 0;
        public double channelDimensionWidth =1;
        public double channelDimensionHeight = 1;
        public Boolean sync = false;
        public Boolean stop = false;
    }
    class RecorderCreate : Recorder
    {
        public RecorderCreate(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderCreateData datas = new RecorderCreateData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderCreateData>(jsonValue);
                double[] channelDimension = { datas.channelDimensionX, datas.channelDimensionY, datas.channelDimensionWidth, datas.channelDimensionHeight };

                if (datas.actionType != null && datas.script != null && datas.channelName != null)
                {
                        AtsLogger.WriteAll("record-create -> name={0}, bound=[{1}]", datas.channelName, string.Join(",", channelDimension));
                        try
                        {
                            recorder.Create(datas.actionType, datas.line, datas.script, datas.timeLine, datas.channelName, channelDimension, datas.sync, datas.stop, datas.DriverInfo);
                            Send();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-create-error -> {0}", ex.Message);
                            SetMsgErrorCode("ats_recorder_create_failed", ex.Message);
                        }
                }
            }
            Send();
            return false;
        }
    }
}
