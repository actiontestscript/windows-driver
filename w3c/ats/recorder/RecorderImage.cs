﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderImageData
    {
        public DriverInfo driverInfo { get; set; }
        public double[] screenRect = null;
        public Boolean isRef =  false;

    }
    class RecorderImage : Recorder
    {
        public RecorderImage(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder){ }
        private RecorderImageData datas = new RecorderImageData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderImageData>(jsonValue);
                
                    try
                {

                    if (datas.driverInfo != null &&  datas.driverInfo.headless)
                    {
                        AtsLogger.WriteAll("record-image -> {0}", datas.driverInfo.screenshotUrl);
                        recorder.AddImage(datas.driverInfo.screenshotUrl );
                    }
                    else
                    {
                        AtsLogger.WriteAll("record-image -> bound=[{0}]", String.Join(",", datas.screenRect));
                        recorder.AddImage(datas.screenRect, datas.isRef);
                    }
                    Send();
                    return true;
                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("record-image-error -> {0}", ex.Message);
                    SetMsgErrorCode("ats_recorder_image_failed", ex.Message);
                }
            }
            Send();
            return false;
        }
    }
}
