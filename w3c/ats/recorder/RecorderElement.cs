﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderElementData
    {
        public Double[] elementBound = { 0D, 0D, 0D, 0D };
        public long searchDuration =  -1;
        public int numElements = -1;
        public string selector =  null;
        public string tag = null;
    }
    class RecorderElement : Recorder
    {
        public RecorderElement(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderElementData datas = new RecorderElementData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderElementData>(jsonValue);
                if (datas.numElements != -1)
                {
                    AtsLogger.WriteAll("record-element -> tag={0}", datas.tag);
                    try
                    {
                        recorder.AddElement(datas.elementBound, datas.searchDuration, datas.numElements, datas.selector, datas.tag);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-element-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_recorder_element_failed", ex.Message);
                    }
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
