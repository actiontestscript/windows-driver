﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderSummaryData
    {
        public Boolean passed = false;
        public int actions = -1;
        public string suiteName = null;
        public string testName = null;
        public string data = null;
        public TestError error = null;
    }


    class RecorderSummary : Recorder
    {
        public RecorderSummary(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderSummaryData datas = new RecorderSummaryData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderSummaryData>(jsonValue);
                if (datas.testName != null && datas.suiteName != null)
                {
                    AtsLogger.WriteAll("record-error-summary -> test={0}, suite={1}", datas.testName, datas.suiteName);
                    try
                    {
                        recorder.Summary(datas.passed, datas.actions, datas.suiteName, datas.testName, datas.data, datas.error);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-error-summary-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_json_data_null", ex.Message);
                    }
                }
                else
                {
                    AtsLogger.WriteAll("record-summary-data -> test={0}, suite={1}, passed={2}", datas.testName, datas.suiteName, datas.passed);
                    try
                    {
                        recorder.Summary(datas.passed, datas.actions, datas.suiteName, datas.testName, datas.data);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-summary-data-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_json_data_null", ex.Message);
                    }
                }
                Send();
                return true;
            }
            Send();
            return false;
        }
    }
}
