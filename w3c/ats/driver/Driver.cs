﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;
using windowsdriver.utils;
using FlaUI.Core;
using System.Diagnostics;
using System.Management;
using System.Text.RegularExpressions;

namespace windowsdriver.w3c.ats.driver
{
    public abstract class Driver : JsonResponse
    {
        protected const int errorCode = -4;
        protected const string UWP_PROTOCOLE = "uwp";
        protected const string PROC_PROTOCOLE = "proc";
        protected const string PROCESS_PROTOCOLE = "process";
        protected const string HANDLE_PROTOCOLE = "handle";
        protected const string WINDOW_PROTOCOLE = "window";

        protected Capabilities capabilities;
        protected VisualRecorder recorder;
        protected DesktopManager desktop;
        protected DesktopResponse desktopResponse;

        public enum DriverType
        {
            Capabilities = 0,
            RemoteDriver = 1,
            Application = 2,
            CloseWindows = 3,
            Close = 4,
            Ostracon = 5,
            Shapes = 6,
            Record = 7
        };

        public static readonly Dictionary<string, int> DriverTypeDict = new Dictionary<string, int>()
        {
            {"capabilities", 0},
            {"remotedriver", 1},
            {"application", 2},
            {"closewindows", 3},
            {"close", 4},
            {"ostracon", 5},
            {"shapes", 6},
            {"record", 7},
        };

        public static DriverType? GetEnumDriverType(string type)
        {
            type = type.ToLower();
            DriverType? driverType = null;

            foreach (var pair in DriverTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    driverType = (DriverType)pair.Value;
                    return driverType;
                }
            }
            return driverType;
        }
        

        protected Driver(HttpListenerContext context, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop ) : base(context)
        {
            this.capabilities = capabilities;
            this.recorder = recorder;
            this.desktop = desktop;
            this.desktopResponse = new DesktopResponse(context);
        }

        public virtual bool Execute()
        {
            return base.SetJsonValue();
        }

        protected void SendDesktopResponse(DesktopWindow[] paramValue)
        {
            desktopResponse.SetWindows(paramValue);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse(DesktopWindow paramValue)
        {
            desktopResponse.SetWindow(paramValue);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse()
        {
            desktopResponse.Send(desktopResponse);
        }

        public override void Dispose()
        {
            desktop = null;
            base.Dispose();
        }

        protected void WaitWindowReady(Application app)
        {
            var maxTry = 20;
            while (maxTry > 0)
            {
                try
                {
                    app.WaitWhileBusy(TimeSpan.FromSeconds(5));
                    return;
                }
                catch (InvalidOperationException)
                {
                    System.Threading.Thread.Sleep(200);
                    maxTry--;
                }
            }
        }

        protected Process GetProcessByInfo(string info)
        {
            ManagementObjectCollection processes = new ManagementObjectSearcher("select ProcessID,Caption,ExecutablePath from Win32_Process").Get();
            foreach (ManagementObject o in processes)
            {
                object exec = o["ExecutablePath"];
                if (exec != null)
                {
                    string procName = Regex.Replace(o["Caption"].ToString(), @".exe$", "");
                    string executablePath = exec.ToString().ToLower();

                    Regex infoRegex = new Regex(info);
                    if (string.Equals(info, procName, StringComparison.OrdinalIgnoreCase) || infoRegex.IsMatch(executablePath))
                    {
                        int.TryParse(o["ProcessID"].ToString(), out int procId);
                        return Process.GetProcessById(procId);
                    }
                }
            }
            return null;
        }

        protected static Process GetProcessByFilename(string fileName)
        {
            fileName = fileName.Replace("/", "\\");
            Process[] procs = Process.GetProcesses();
            foreach (Process p in procs)
            {
                try
                {
                    if (fileName.Equals(p.MainModule.FileName))
                    {
                        return p;
                    }
                }
                catch { }
            }
            return null;
        }

        protected void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            }
            catch { }

            searcher.Dispose();
        }
    }
}
