﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats.driver
{
    public class DriverFactory
    {
        public Driver Create(Driver.DriverType? driverType, HttpListenerContext context, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop)
        {
            switch (driverType)
            {
                case Driver.DriverType.Capabilities:
                    return new DriverCapabilities(context,capabilities,recorder,desktop);
                case Driver.DriverType.RemoteDriver:
                    return new DriverRemoteDriver(context, capabilities, recorder, desktop);
                case Driver.DriverType.Application:
                    return new DriverApplication(context, capabilities, recorder, desktop);
                case Driver.DriverType.CloseWindows:
                    return new DriverCloseWindows(context, capabilities, recorder, desktop);
                case Driver.DriverType.Close:
                    return new DriverClose(context, capabilities, recorder, desktop);
                case Driver.DriverType.Ostracon:
                    return new DriverOstracon(context, capabilities, recorder, desktop);
                case Driver.DriverType.Shapes:
                    return new DriverShapes(context, capabilities, recorder, desktop);
                default:
                    //                    return new DriverOthers(context);
                    return null;
            }
        }
    }
}
