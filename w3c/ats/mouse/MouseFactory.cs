﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.ats.mouse
{
    public class MouseFactory
    {
        public Mouse Create(Mouse.MouseType? mouseType, HttpListenerContext context, DesktopManager desktop)
        {
            switch (mouseType)
            {
                case Mouse.MouseType.Move:
                    return new MouseMove(context, desktop); 
                case Mouse.MouseType.Click:
                    return new MouseClick(context, desktop);
                case Mouse.MouseType.RightClick:
                    return new MouseRightClick(context, desktop);
                case Mouse.MouseType.MiddleClick:
                    return new MouseMiddleClick(context, desktop);
                case Mouse.MouseType.DoubleClick:
                    return new MouseDoubleClick(context, desktop);
                case Mouse.MouseType.Down:
                    return new MouseDown(context, desktop);
                case Mouse.MouseType.Drag:
                    return new MouseDrag(context, desktop);
                case Mouse.MouseType.Release:
                    return new MouseRelease(context, desktop);
                case Mouse.MouseType.Wheel:
                    return new MouseWheel(context, desktop);
                default:
                    return new MouseOthers(context, desktop);
            }
        }
    }
}
