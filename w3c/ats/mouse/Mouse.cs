﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.ats.mouse
{
    public abstract class Mouse : JsonResponse
    {
        protected const int errorCode = -5;

        protected DesktopManager desktop;

        public enum MouseType
        {
            Move = 0,
            Click = 1,
            RightClick = 2,
            MiddleClick = 3,
            DoubleClick = 4,
            Down = 5,
            Release = 6,
            Wheel = 7,
            Drag = 8
        };

        public static readonly Dictionary<string, int> mouseTypeDict = new Dictionary<string, int>()
        {
            {"move", 0},
            {"click", 1},
            {"rightclick", 2},
            {"middleclick", 3},
            {"doubleclick", 4},
            {"down", 5},
            {"release", 6},
            {"wheel", 7},
            {"drag", 8}
        };

        public static MouseType? getEnumMouseType(string type)
        {
            type = type.ToLower();
            MouseType? mouseType = null;

            foreach (var pair in mouseTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    mouseType = (MouseType)pair.Value;
                    return mouseType;
                }
            }
            return mouseType;
        }
        public Mouse(HttpListenerContext context, DesktopManager desktop) : base(context)
        {
            this.desktop = desktop;
            //this.desktopResponse = new DesktopResponse(context);
        }
        public virtual Boolean Execute()
        {
            return base.SetJsonValue();
        }

        public override void Dispose()
        {
            desktop = null;
            base.Dispose();
        }

    }
}
