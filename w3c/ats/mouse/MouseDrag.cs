﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Net;

namespace windowsdriver.w3c.ats.mouse
{
    class MouseDrag : Mouse
    {
        public MouseDrag(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }

        public override bool Execute()
        {
            if (base.Execute())
            {
                int dragOffsetX = 20;
                int dragOffsetY = 20;

                /*AtsElement current = desktop.GetElementFromPoint(FlaUI.Core.Input.Mouse.Position);
                if (current != null)
                {
                    dragOffsetX = Convert.ToInt32(current.Width / 2);
                    dragOffsetY = Convert.ToInt32(current.Height / 2);
                }*/

                FlaUI.Core.Input.Mouse.Down(FlaUI.Core.Input.MouseButton.Left);
                System.Threading.Thread.Sleep(200);
                FlaUI.Core.Input.Mouse.MoveBy(dragOffsetX, dragOffsetY);
            }
            Send();
            return false;
        }
    }
}
