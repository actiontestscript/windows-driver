﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using windowsdriver.Services.CommandProcessing;
using windowsdriver.Utils.Logger;

namespace windowsdriver.utils
{
    public class RemoteDriver
    {
        private const string HOST_KEYWORD = "#HOST#";
        private const string DRIVER_NOTE_FOLDER = "Driver_Notes";

        //TODO try to find a way to get this url automatically
        private const string URL_ATS_DRIVERS = "https://actiontestscript.org/releases/ats-drivers/windows/";

        private readonly Regex CHROME_REGEXP = new Regex("^ChromeDriver ([^\\s]*) .*");
        private readonly Regex FIREFOX_REGEXP = new Regex("^geckodriver ([^\\s]*)\\s?.*");
        private readonly Regex MSEDGE_REGEXP = new Regex("^Microsoft Edge WebDriver ([^\\s]*) .*");
        private readonly Regex OPERA_REGEXP = new Regex("^OperaDriver ([^\\s]*) .*");
        private readonly Regex IE_REGEXP = new Regex("^IEDriverServer.exe ([^\\s]*) .*");

        private readonly Regex STARTED_REGEXP = new Regex(".*was started successfully(.*)");
        private readonly Regex GECKO_STARTED_REGEXP = new Regex(".*Listening on (.*):(.*)");
        private readonly Regex IE_STARTED_REGEXP = new Regex(".*Listening on port (.*)");

        private readonly Regex START_ERROR_REGEXP = new Regex(".*port not available. Exiting.*");
        private readonly Regex GECKO_START_ERROR_REGEXP = new Regex(".*geckodriver.exe: error:(.*)");

        private readonly Regex SAP_REGEXP = new Regex("^ATS SapWebDriver ([^\\s]*)");// do not change this value
        private readonly Regex START_DRIVER_ERROR = new Regex("^AtsDriver start error:(.*)"); // do not change this value

        private string driverFile;
        private readonly string driverFolder;

        private int driverPort = 0;
        private Process driverProcess;

        private bool started = false;
        public string startError = null;

        private bool isSAP = false;
        private bool isIE = false;

        public string id = Guid.NewGuid().ToString();
        public string version = "0.0.0";
        public string url;
        public ApplicationInfo application;

        private string args = " --log-level=INFO";

        private string DriverName { get; set; }

        private readonly CommandResult commandResult;

        public RemoteDriver(string folder, string name, string path, CommandResult command)
        {
            driverFolder = folder;
            DriverName = name;
            commandResult = command;
            application = new ApplicationInfo(name, path);

            if (application.isActive)
            {
                CheckDriverVersion();
                Start();
            }
            else
            {
                AtsLogger.WriteError("Remote driver file not found -> {0}", application.path);
                startError = "application binary not found -> " + application.path;
            }
        }

        public void Dispose()
        {
            CloseDriverProcess();
        }

        public void UpdatePath(string p)
        {
            if (application.UpdatePath(p))
            {
                CheckDriverVersion();
                LoadVersion();
                Start();
            }
        }

        private void CheckDriverVersion()
        {
            driverFile = Path.Combine(driverFolder, application.remoteDriverName + ".exe");

            if (File.Exists(driverFile))
            {
                LoadVersion();

                if (application.matchDriver(version))
                {
                    AtsLogger.WriteAll("{0} (ver. {1}) is matching application {2} (ver. {3})", application.remoteDriverName, version, application.name, application.version);
                    return;
                }
                else
                {
                    AtsLogger.WriteWarning("{0} (ver. {1}) is not matching application {2} (ver. {3}) !", application.remoteDriverName, version, application.name, application.version);
                }
            }
            else
            {
                AtsLogger.WriteWarning("no remotedriver found for application {0} !", application.remoteDriverName + ".exe");
            }

            CloseDriverProcess();
            DownloadDriver();
        }

        public bool CheckApplication(string name)
        {
            return application.name == name;
        }

        public void DownloadDriver()
        {

            string file = Path.Combine(driverFolder, application.remoteDriverName);
            string url = URL_ATS_DRIVERS + application.name + "/" + application.driverVersion + ".zip";

            string zipFile = file + ".zip";
            string exeFile = file + ".exe";

            AtsLogger.WriteInfo("delete files and folders -> '{0}'", zipFile);

            try
            {
                if (File.Exists(zipFile))
                {
                    File.Delete(zipFile);
                }

                string noteFolder = Path.Combine(driverFolder, DRIVER_NOTE_FOLDER);
                if (Directory.Exists(noteFolder))
                {
                    AtsLogger.WriteInfo("delete folder -> " + noteFolder);
                    Directory.Delete(noteFolder, true);
                }
            }
            catch (Exception ex)
            {
                AtsLogger.WriteError("fail to delete old drivers -> {0}", ex.Message);
                return;
            }

            AtsLogger.WriteInfo("download driver -> '{0}'", url);

            using (var client = new WebClient())
            {
                IWebProxy wp = WebRequest.DefaultWebProxy;
                wp.Credentials = CredentialCache.DefaultCredentials;

                try
                {
                    client.Proxy = wp;
                    client.Headers.Add(HttpRequestHeader.UserAgent, "Ats-SystemDriver-Agent");
                    client.DownloadFile(url, zipFile);
                }
                catch (Exception e)
                {
                    AtsLogger.WriteError("fail to download remotedriver -> {0}", e.Message);
                    return;
                }
            }

            CloseDriverProcess();

            AtsLogger.WriteInfo("unzip driver archive -> '{0}'", zipFile);

            try
            {
                using (ZipArchive source = ZipFile.Open(zipFile, ZipArchiveMode.Read, null))
                {
                    try
                    {
                        if (File.Exists(exeFile))
                        {
                            File.Delete(exeFile);
                        }
                    }
                    catch (Exception ex) {}

                    foreach (ZipArchiveEntry entry in source.Entries)
                    {
                        if (!entry.FullName.StartsWith(DRIVER_NOTE_FOLDER))
                        {
                            string fullPath = Path.GetFullPath(Path.Combine(driverFolder, entry.FullName));

                            if (Path.GetFileName(fullPath).Length != 0)
                            {
                                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                                entry.ExtractToFile(fullPath, true);
                            }
                        }
                    }
                }

                File.Delete(zipFile);
            }
            catch (Exception ex)
            {
                AtsLogger.WriteError("unable to unzip remotedriver archive -> {0}", ex.Message);
                startError = ex.Message;
            }

            LoadVersion();
        }

        public void CloseDriverProcess()
        {
            if (driverProcess != null)
            {
                ProcessTree.KillProcessAndChildren(driverProcess.Id);

                driverProcess = null;
                driverPort = 0;
            }
        }

        internal void Start()
        {
            if (driverFile != null)
            {
                if (File.Exists(driverFile))
                {
                    if (driverProcess == null)
                    {
                        started = false;
                        startError = null;

                        driverPort = Network.FindFreeDriverPort();

                        driverProcess = new Process();

                        driverProcess.StartInfo.FileName = driverFile;
                        driverProcess.StartInfo.Arguments = argumentsWebDriver();
                        driverProcess.StartInfo.UseShellExecute = false;
                        driverProcess.StartInfo.RedirectStandardOutput = true;
                        driverProcess.StartInfo.RedirectStandardError = true;

                        driverProcess.StartInfo.CreateNoWindow = true;
                        driverProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                        driverProcess.EnableRaisingEvents = true;
                        driverProcess.Exited += new EventHandler(ProcessExited);

                        driverProcess.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
                        driverProcess.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

                        driverProcess.Start();

                        driverProcess.BeginErrorReadLine();
                        driverProcess.BeginOutputReadLine();

                        int maxTry = 70;
                        while (maxTry > 0 && startError == null && !started)
                        {
                            Thread.Sleep(200);
                            maxTry--;
                        }

                        driverProcess.OutputDataReceived -= OutputHandler;

                        if (started)
                        {
                            AtsLogger.WriteInfo("{0} (ver. {1}) started on port {2}", application.remoteDriverName, version, driverPort);
                        }
                        else
                        {
                            CloseDriverProcess();
                            AtsLogger.WriteError("fail to start {0} (ver. {1}) -> {2}", application.remoteDriverName, version, startError);
                        }
                    }
                }
                else
                {
                    AtsLogger.WriteError("unable to start driver, {0}.exe file is missing", application.remoteDriverName);
                    startError = "driver file '" + application.remoteDriverName + ".exe' not found in folder '" + driverFolder + "'";
                }
            }
        }

        internal string argumentsWebDriver()
        {
            var arguments = " --port=" + driverPort;

            if (commandResult.allowedIps != null && commandResult.allowedIps.Count > 0)
            {
                var uniqueIps = new HashSet<string>(commandResult.allowedIps);

                if (DriverName == "gecko" || DriverName == "firefox")
                {
                    arguments += " --host=0.0.0.0";
                    arguments += " --allow-hosts=" + string.Join(",", uniqueIps);
                }
                else
                {
                    arguments += " --allowed-ips=" + string.Join(",", uniqueIps);
                    arguments += " --whitelisted-ips=" + string.Join(",", uniqueIps);
                    arguments += " --allowed-origins=" + string.Join(",", uniqueIps);
                }
            }

            if (DriverName == "gecko" || DriverName == "firefox")
            {
                arguments += application.logOptions;
                return arguments;
            }

            return arguments + args;
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (sendingProcess != null && outLine.Data != null)
            {
                Match match = STARTED_REGEXP.Match(outLine.Data);
                if (match.Success || GECKO_STARTED_REGEXP.Match(outLine.Data).Success || IE_STARTED_REGEXP.Match(outLine.Data).Success)
                {
                    driverProcess.OutputDataReceived -= OutputHandler;

                    if (isSAP && match.Groups.Count > 1)
                    {
                        try
                        {
                            int port = Int32.Parse(match.Groups[1].Value.Replace("on port", "").Trim());
                            if (port > 0)
                            {
                                driverPort = port;
                            }
                        }
                        catch { }
                    }
                    else if (isIE)
                    {
                        try
                        {
                            int port = Int32.Parse(outLine.Data.Replace("Listening on port ", "").Trim());
                            if (port > 0)
                            {
                                driverPort = port;
                            }
                        }
                        catch { }
                    }

                    url = HOST_KEYWORD + ":" + driverPort;
                    started = true;

                }
                else if (START_ERROR_REGEXP.Match(outLine.Data).Success || GECKO_START_ERROR_REGEXP.Match(outLine.Data).Success)
                {
                    startError = "unable to start driver with port -> " + driverPort;
                }
                else
                {
                    match = START_DRIVER_ERROR.Match(outLine.Data);
                    if (match.Success)
                    {
                        startError = match.Groups[1].ToString();
                    }
                }
            }
        }

        private void ProcessExited(object sender, EventArgs e)
        {
            driverProcess = null;
        }

        //---------------------------------------------------------------------------------------
        // version management
        //---------------------------------------------------------------------------------------

        internal void LoadVersion()
        {
            using (Process proc = new Process())
            {
                proc.StartInfo.FileName = driverFile;
                proc.StartInfo.Arguments = "--version";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;

                proc.OutputDataReceived += new DataReceivedEventHandler(DriverVersionOutputDataReceived);
                proc.ErrorDataReceived += new DataReceivedEventHandler(DriverVersionOutputDataReceived);

                try
                {
                    proc.Start();
                    proc.BeginOutputReadLine();
                    proc.BeginErrorReadLine();
                    proc.WaitForExit();
                }
                catch
                {
                    System.IO.File.Delete(driverFile);
                }
            }
        }

        private void DriverVersionOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                string driverOutput = e.Data.Trim();

                Match match = FIREFOX_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;

                    //args = " --marionette-port 2828 --log info --log-no-truncate";
                    args = " --marionette-port 2828 -safe-mode --log info --log-no-truncate";

                    if (application.path != null)
                    {
                        args += " --binary \"" + application.path + "\"";
                    }

                    return;
                }

                match = CHROME_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;
                    return;
                }

                match = MSEDGE_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;
                    return;
                }

                match = OPERA_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;
                    return;
                }

                match = SAP_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;
                    isSAP = true;
                    return;
                }

                match = IE_REGEXP.Match(driverOutput);
                if (match.Success)
                {
                    version = match.Groups[1].Value;
                    isIE = true;
                    return;
                }
            }
        }
    }
}