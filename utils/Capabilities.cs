﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using windowsdriver.Services.CommandProcessing;

namespace windowsdriver.utils
{
    public class Capabilities
    {
        public Dictionary<string, string> data;

        private readonly List<RemoteDriver> drivers = new List<RemoteDriver>();

        private readonly string driversFolder;
        private readonly bool interactive = Environment.UserInteractive;
        private CommandResult commandResult;

        public Capabilities(DesktopManager desktop, CommandResult commandResult)
        {
            this.commandResult = commandResult;
            FileInfo fi = new FileInfo(Process.GetCurrentProcess().MainModule.FileName);
            driversFolder = fi.Directory.FullName;

            LoadData(desktop);
        }

        public static string GetAtsRecorderTempFolder()
        {
            string tempFolder = Path.GetTempPath();
            if (!tempFolder.EndsWith("\\"))
            {
                tempFolder += "\\";
            }
            return tempFolder + "ats_recorder";
        }

        public RemoteDriver GetRemoteDriver(string appName, string appPath)
        {
            if(appPath == null)
            {
                appPath = ApplicationInfo.GetBrowserPath(appName);
            }

            foreach (RemoteDriver rd in drivers)
            {
                if (rd.CheckApplication(appName))
                {
                    rd.UpdatePath(appPath);
                    return rd;
                }
            }

            RemoteDriver remoteDriver = new RemoteDriver(driversFolder, appName, appPath, commandResult);
            if (remoteDriver.url != null)
            {
                drivers.Add(remoteDriver);
            }

            return remoteDriver;
        }

        public DesktopData[] GetRemoteDriverUrl(string appName, string appPath)
        {
            string driverUrl = "";
            RemoteDriver driver = GetRemoteDriver(appName, appPath);

            if (driver != null)
            {
                driverUrl = driver.url;
            }
            return new DesktopData[] { new DesktopData("url", driverUrl) };
        }

        public void Dispose()
        {
            foreach (RemoteDriver rd in drivers)
            {
                rd.Dispose();
            }
        }

        private void LoadData(DesktopManager desktop)
        {
            data = new Dictionary<string, string> {
                { "MachineName", Environment.MachineName },
                { "UserHome", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) },
                { "DriverVersion", System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString() },
                { "DotNetVersion", GetFrameworkVersion().ToString() },
                { "ScreenWidth", desktop.DesktopWidth.ToString() },
                { "ScreenHeight", desktop.DesktopHeight.ToString() },
                { "VirtualWidth", desktop.DesktopWidth.ToString() },
                { "VirtualHeight", desktop.DesktopHeight.ToString() },
                { "Interactive", interactive.ToString() },
                { "Version", Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ReleaseId", "").ToString() }
            };

            try
            {
                string driveLetter = Path.GetPathRoot(System.Reflection.Assembly.GetExecutingAssembly().Location);
                DriveInfo dinf = new DriveInfo(driveLetter);
                if (dinf.IsReady)
                {
                    data.Add("DriveLetter", driveLetter);
                    data.Add("DiskTotalSize", dinf.TotalSize / 1024 / 1024 + " Mo");
                    data.Add("DiskFreeSpace", dinf.AvailableFreeSpace / 1024 / 1024 + " Mo");
                }
            }
            catch (Exception) { }

            ManagementObject os = new ManagementObjectSearcher("select * from Win32_OperatingSystem").Get().Cast<ManagementObject>().First();
            data.Add("BuildNumber", (string)os["BuildNumber"]);
            data.Add("Name", (string)os["Caption"]);
            data.Add("CountryCode", (string)os["CountryCode"]);
            os.Dispose();

            ManagementObject cpu = new ManagementObjectSearcher("select * from Win32_Processor").Get().Cast<ManagementObject>().First();
            data.Add("CpuSocket", (string)cpu["SocketDesignation"]);
            data.Add("CpuName", (string)cpu["Caption"]);
            data.Add("CpuArchitecture", "" + (ushort)cpu["Architecture"]);
            data.Add("CpuMaxClockSpeed", (uint)cpu["MaxClockSpeed"] + " Mhz");
            data.Add("CpuCores", "" + (uint)cpu["NumberOfCores"]);
            cpu.Dispose();

            ApplicationInfo app = new ApplicationInfo("chrome");
            if (app.isActive)
            {
                data.Add("ChromePath", app.path);
                data.Add("ChromeVersion", app.version);
                data.Add("ChromeDriverVersion", app.driverVersion);
            }

            app = new ApplicationInfo("firefox");
            if (app.isActive)
            {
                data.Add("FirefoxPath", app.path);
                data.Add("FirefoxVersion", app.version);
                data.Add("FirefoxDriverVersion", app.driverVersion);
            }

            app = new ApplicationInfo("opera");
            if (app.isActive)
            {
                data.Add("OperaPath", app.path);
                data.Add("OperaVersion", app.version);
                data.Add("OperaDriverVersion", app.driverVersion);
            }

            app = new ApplicationInfo("msedge");
            if (app.isActive)
            {
                data.Add("MsEdgePath", app.path);
                data.Add("MsEdgeVersion", app.version);
                data.Add("MsEdgeDriverVersion", app.driverVersion);
            }

            app = new ApplicationInfo("brave");
            if (app.isActive)
            {
                data.Add("BravePath", app.path);
                data.Add("BraveVersion", app.version);
                data.Add("BraveDriverVersion", app.driverVersion);
            }

        }
        public string GetValue(string key)
        {
            return data[key];
        }

        private static Version GetFrameworkVersion()
        {
            using (RegistryKey ndpKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full"))
            {
                if (ndpKey != null)
                {
                    int value = (int)(ndpKey.GetValue("Release") ?? 0);
                    if (value >= 528040)
                        return new Version(4, 8, 0);

                    if (value >= 461808)
                        return new Version(4, 7, 2);

                    if (value >= 461308)
                        return new Version(4, 7, 1);

                    if (value >= 460798)
                        return new Version(4, 7, 0);

                    if (value >= 394802)
                        return new Version(4, 6, 2);

                    if (value >= 394254)
                        return new Version(4, 6, 1);

                    if (value >= 393295)
                        return new Version(4, 6, 0);

                    if (value >= 379893)
                        return new Version(4, 5, 2);

                    if (value >= 378675)
                        return new Version(4, 5, 1);

                    if (value >= 378389)
                        return new Version(4, 5, 0);
                }
            }

            return new Version(0, 0, 0);
        }

        internal DesktopData[] GetAttributes()
        {
            DesktopData[] result = new DesktopData[data.Count];

            int loop = 0;
            foreach (var kvp in data)
            {
                result[loop] = new DesktopData(kvp.Key, kvp.Value);
                loop++;
            }

            return result;
        }
    }
}