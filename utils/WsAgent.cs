﻿

using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using WatsonWebsocket;

static class WsAgent{

    private static readonly string ATS_CLIENT_ID = "Ats-Client-Id";

    private static readonly string ATS_DRIVER_VERSION = "ats-driver-version";
    private static readonly string ATS_DRIVER_PORT = "ats-driver-port";
    private static readonly string ATS_AGENT_PORT = "ats-agent-port";
    private static readonly string ATS_AGENT_OUTPUT = "ats-agent-output";
    private static readonly string ATS_AGENT_WARNING = "ats-agent-warning";
    private static readonly string ATS_AGENT_ERROR = "ats-agent-error";
    private static readonly string ATS_AGENT_INFO = "ats-agent-info";
    private static readonly string ATS_AGENT_CONNECTED = "ats-agent-connected";

    private static string address;

    public static string UsedBy { get; set; }
 
    public static int Port
    {
        get { if (UsedBy == null) { return _port; } else { return 0; }  ; }
        set { _port = value; }
    }
    private static int _port = 0;

    public static bool Local { get; internal set; } = true;

    public static string Uuid { get; internal set; }

    private static string driverVersion;
    private static WatsonWsClient client;

    public static bool IsConnected { get; set; } = false;

    internal static void Connect(object sender, ConnectionEventArgs args)
    {
        if(UsedBy == null)
        {
            //context = aContext;
            address = args.Client.Ip;
            UsedBy = address;

            client = new WatsonWsClient(args.Client.Ip, args.Client.Port, false, args.Client.Guid);

            IsConnected = true;

            if (!Local)
            {
                Console.Clear();
            }
        }
    }

    internal static void Update(object sender, MessageReceivedEventArgs args)
    {
        try
        {
            string message = Encoding.UTF8.GetString(args.Data.Array);

            string[] data = message.Split(new char[] { '\t', });
            if( data[0].Equals("isConnected"))
            {
                var log = ATS_AGENT_CONNECTED + "|" + (IsConnected ? "true" : "false");
                client.SendAsync(log);
                //context?.Send(log);
            }
            else
            {
                UsedBy = data[0] + "@" + data[1];
                Uuid = data[2].Substring(0, 36);

                PrintLog(ATS_DRIVER_VERSION, driverVersion, false);

                Console.WriteLine("client connected : " + WsAgent.UsedBy + "@" + address);
            }
        }
        catch (Exception ex)
        {
            Console.Error.WriteLine(ex.Message.ToString());
        }
    }

    internal static void Disconnect(object sender, DisconnectionEventArgs args)
    {
        UsedBy = null;
        Uuid = null;
        IsConnected = false;
        client = null;

        Console.WriteLine("client disconnected");
    }

    private static void PrintLog(string type, string data, bool silent)
    {
        PrintLog(type, data, Console.Out, silent);
    }

    private static void PrintLog(string type, string data, TextWriter output, bool silent)
    {
        string log = type + "|" + data;

        if(client != null)
        {
            try
            {
                client.SendAsync(log);
                //context?.Send(log);
            }
            catch { }
        }

        if (!silent)
        {
            output.WriteLine(log);
            output.Flush();
        }
    }

    public static void LogInfo(string data, bool silent)
    {
        PrintLog(ATS_AGENT_INFO, data, silent);
    }

    public static void LogWarning(string data, bool silent)
    {
        PrintLog(ATS_AGENT_WARNING, data, silent);
    }

    public static void LogError(string data, bool silent)
    {
        PrintLog(ATS_AGENT_ERROR, data, Console.Error, silent);
    }

    public static void LogOutput(string data, bool silent)
    {
        PrintLog(ATS_AGENT_OUTPUT, data, silent);
    }

    internal static void SetData(string version, int port, int wsPort)
    {
        Port = wsPort;
        driverVersion = version;

        PrintLog(ATS_DRIVER_VERSION, driverVersion, !Local);
        PrintLog(ATS_DRIVER_PORT, port.ToString(), !Local);
        PrintLog(ATS_AGENT_PORT, wsPort.ToString(), !Local);
    }

    internal static bool Allowed(NameValueCollection headers)
    {
        if(!Local)
        {
            if (UsedBy != null)
            {
                    string[] values = headers.GetValues(ATS_CLIENT_ID);
                    if (values != null && values.Length > 0 && values[0] != Uuid)
                    {
                        return false;
                    }
            }
        }
        return true;
    }
}