﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;

namespace windowsdriver.utils
{
    class ProcessTree
    {
        public ProcessTree(Process process, List<int> procList)
        {
            this.Process = process;
            procList.Add(process.Id);
            InitChildren(procList);
        }

        // Recursively load children
        void InitChildren(List<int> procList)
        {
            this.ChildProcesses = new List<ProcessTree>();

            // retrieve the child processes
            var childProcesses = this.Process.GetChildProcesses();

            // recursively build children
            foreach (var childProcess in childProcesses)
                this.ChildProcesses.Add(new ProcessTree(childProcess, procList));
        }

        public Process Process { get; set; }

        public List<ProcessTree> ChildProcesses { get; set; }

        public int Id { get { return Process.Id; } }

        public string ProcessName { get { return Process.ProcessName; } }

        public long Memory { get { return Process.PrivateMemorySize64; } }

        public static void KillProcessAndChildren(int pid)
        {
            if (pid == 0)
            {
                return;
            }

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();

            foreach (ManagementObject mo in moc)
            {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            }
            catch (Exception)
            {
                // Process already exited.
            }
        }

    }
}
