﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;


namespace windowsdriver.utils.JsonUtils
{
    /// <summary>
    /// This class is used to get the value of a json node
    /// </summary>
    internal class JsonUtils
    {
        /// <summary>
        /// Get the value of a json node with key and default value
        /// </summary>  
        /// <typeparam name="T">The value type to get </typeparam>    
        /// <param name="node">The json node</param>
        /// <param name="key">The json key research</param>
        /// <param name="defaultValue">The default value if the key is not found</param>
        /// <returns>The value of the json node</returns>
        public static T GetJsonValue<T>(JObject node, string key, T defaultValue)
        {
            if (node.ContainsKey(key))
            {
                JToken valueToken = node[key];
                try
                {
                    return valueToken.Value<T>();
                }
                catch (Exception)
                {
                    return defaultValue;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Get the value of a json array node with key and default value
        /// </summary>  
        /// <typeparam name="T">The value type to get </typeparam>    
        /// <param name="node">The json node</param>
        /// <param name="key">The json key research</param>
        /// <param name="defaultValue">The default value if the key is not found</param>
        /// <returns>The array of the json node</returns>
        public static T[] GetJsonArray<T>(JObject node, string key, T[] defaultValue)
        {
            if (node.ContainsKey(key))
            {
                JToken arrayToken = node[key];
                try
                {
                    if (arrayToken.Type == JTokenType.Array)
                    {
                        JArray jsonArray = (JArray) arrayToken;
                        return jsonArray.ToObject<List<T>>().ToArray();
                    }
                    else
                    {
                        return defaultValue;
                    }

                }
                catch (Exception)
                {
                    return defaultValue;
                }
            }
            return defaultValue;
        }
    
        public static string GetJsonResponseNull()
        {
            var jsonValue = new Dictionary<string, object>
            {
                {"value", null }
            };

            return JsonConvert.SerializeObject(jsonValue);
        }

        public static bool CheckKeyPathExists(string jsonContent, List<string> keyPath)
        {
            try
            {
                    var jsonObject = JObject.Parse(jsonContent);
                    JToken currentToken = jsonObject;

                    foreach (var key in keyPath)
                    {
                        if (currentToken is JObject currentObject && currentObject.TryGetValue(key, out JToken nextToken))
                        {
                            currentToken = nextToken;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true; 
            }
            catch
            {
                return false;
            }
        }

        public static string readJsonContent(HttpListenerContext context)
        {
            string jsonContent="";
            try
            {
                using (var reader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding))
                {
                    jsonContent = reader.ReadToEnd();
                }
                return jsonContent;
            }
            catch 
            {
                return jsonContent;
            }
        }
    }

}
