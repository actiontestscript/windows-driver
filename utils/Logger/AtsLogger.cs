﻿
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;

namespace windowsdriver.Utils.Logger
{
    public static class AtsLogger
    {
        private static bool silent = false;
        
        public static void Init(bool b)
        {
           silent = b;
        }

        public static void WriteInfo(string format, params object[] values)
        {
            WsAgent.LogInfo(string.Format(format, values), silent);
        }

        public static void WriteError(string format, params object[] values)
        {
            WsAgent.LogError(string.Format(format, values), silent);
        }

        public static void WriteWarning(string format, params object[] values)
        {
            WsAgent.LogWarning(string.Format(format, values), silent);
        }

        public static void WriteAll(string format, params object[] values)
        {
            try
            {
                WsAgent.LogOutput(string.Format(format, values), silent);
            }
            catch
            {
                WsAgent.LogOutput("error writing logs -> " + format, silent);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------
    }
}