﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace windowsdriver.utils
{
    public class Network
    {
        public const int DEFAULT_PORT = 9700;

        public static int FindFreeDriverPort()
        {
            var findPort = DEFAULT_PORT + 10;

            while (IsPortInUse(findPort))
            {
                findPort++;
            }
            return findPort;
        }

        /*
        public static bool CheckFreePort(int port)
        {
            HttpListener listener = new HttpListener();
            try
            {
//                listener.Prefixes.Add("http://127.0.0.1:" + port + "/");
                listener.Prefixes.Add("http://localhost:" + port + "/");
                //listener.Prefixes.Add("http://192.168.0.156:" + port + "/");
                listener.Start();
                listener.Stop();

                return true;
            }
            catch
            {
                return false;
            }
        }
        */

        /// <summary>
        /// Retrieves a list of local IPv4 addresses assigned to the active network interfaces of the machine.
        /// </summary>
        /// <returns>
        /// A List of <see cref="IPAddress"/> objects representing the IPv4 addresses of the machine's active network interfaces,
        /// excluding loopback interfaces.
        /// </returns>
        /// <remarks>
        /// This method filters out inactive network interfaces and loopback interfaces. It only considers IPv4 addresses (AddressFamily.InterNetwork).
        /// The addresses are extracted from the unicast address information of each network interface.
        /// </remarks>
        public static List<IPAddress> GetLocalIpAddresses()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(netInterface => netInterface.OperationalStatus == OperationalStatus.Up)
                .Where(netInterface => netInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .SelectMany(netInterface => netInterface.GetIPProperties().UnicastAddresses)
                .Where(address => address.Address.AddressFamily == AddressFamily.InterNetwork)
                .Select(address => address.Address)
                .ToList();
        }

        /// <summary>
        /// Determines whether a given IP address is part of a specified network.
        /// </summary>
        /// <param name="ipAddress">The IP address to check, as a string.</param>
        /// <param name="networkIp">The IP address of the network, as a string. The subnet mask is inferred based on the class of this network address (A, B, or C).</param>
        /// <returns>
        /// <c>true</c> if the specified IP address is part of the network; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>
        /// This method parses both IP addresses and determines the class-based default CIDR subnet mask for the network IP.
        /// It then checks if the given IP address belongs to the same subnet as the network IP.
        /// This method assumes IPv4 addresses and classful network design.
        /// </remarks>
        public static bool IsIpInNetwork(string ipAddress, string networkIp)
        {
            var ip = IPAddress.Parse(ipAddress);
            var network = IPAddress.Parse(networkIp);
            var cidr = GetCidrFromClass(network);

            var mask = GetSubnetMask(cidr);

            var ipBytes = ip.GetAddressBytes();
            var networkBytes = network.GetAddressBytes();
            var maskBytes = mask.GetAddressBytes();


            for (var i = 0; i < ipBytes.Length; i++)
            {
                if ((ipBytes[i] & maskBytes[i]) != (networkBytes[i] & maskBytes[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines the CIDR (Classless Inter-Domain Routing) notation of the subnet mask 
        /// based on the class of the provided IPv4 address.
        /// </summary>
        /// <param name="ip">The IPv4 address as an <see cref="IPAddress"/> object.</param>
        /// <returns>
        /// The CIDR notation (as an integer) of the subnet mask corresponding to the class of the IP address.
        /// Returns 8 for Class A, 16 for Class B, and 24 for Class C.
        /// Returns 0 if the address does not belong to Class A, B, or C.
        /// </returns>
        /// <remarks>
        /// This method inspects the first octet of the IP address to determine its class.
        /// It assumes a traditional classful network architecture (IPv4).
        /// Addresses in Class D (224-239, multicast) and Class E (240-255, reserved for future use) return 0,
        /// as they are not used for standard host/network addressing.
        /// </remarks>
        private static int GetCidrFromClass(IPAddress ip)
        {
            var firstOctet = ip.GetAddressBytes().First();

            if (firstOctet >= 1 && firstOctet <= 126)
                return 8;  // Classe A
            if (firstOctet >= 128 && firstOctet <= 191)
                return 16; // Classe B
            if (firstOctet >= 192 && firstOctet <= 223)
                return 24; // Classe C
            
            return 0;
        }

        /// <summary>
        /// Generates an IPv4 subnet mask based on the given CIDR notation.
        /// </summary>
        /// <param name="cidr">The CIDR notation (number of leading 1 bits in the mask) as an integer.</param>
        /// <returns>
        /// An <see cref="IPAddress"/> representing the subnet mask.
        /// </returns>
        /// <remarks>
        /// The method calculates the subnet mask by shifting a bitmask according to the CIDR value.
        /// For example, a CIDR of 24 (typical for a Class C network) generates a subnet mask of 255.255.255.0.
        /// If the CIDR value is 0 or negative, the method returns a subnet mask of 0.0.0.0.
        /// Note that this method is designed for IPv4 addresses and CIDR notations.
        /// </remarks>
        private static IPAddress GetSubnetMask(int cidr)
        {
            var mask = cidr > 0 ? int.MaxValue << (32 - cidr) : 0;
            return new IPAddress(BitConverter.GetBytes(mask).Reverse().ToArray());
        }

        public static bool IsIpAllowed(string ipAddress, string[] allowedIps)
        {
            if (allowedIps.Length == 0)
            {
                return true;
            }

            foreach (var allowedIp in allowedIps)
            {
                if (IsIpInNetwork(ipAddress, allowedIp))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsPortInUse(int port)
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:" + port + "/");
            try
            {
                listener.Start();
                listener.Stop();
                return false;
            }
            catch
            {
                return true;
            }


            /*
            TcpListener tcpListener = null;
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, port);
                tcpListener.Start();
                return false;
            }
            catch (SocketException)
            {
                return true; 
            }
            finally
            {
                tcpListener?.Stop();
            }
            */
        }
    }
}

