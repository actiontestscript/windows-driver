﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace windowsdriver.utils
{
    internal class ShapesRecognition
    {
        private static readonly string ERROR_CODE = "error-code";
        private static readonly string ERROR_MESSAGE = "error-message";

        private static readonly string OUTPUT = "output-file";
        private static readonly string COUNT = "shapes-count";
        private static readonly string AVERAGE = "shapes-average";

        private static readonly string ATS_OPENCV_HOME = "ATS_OPENCV_HOME";
        private static readonly string EXEC_FILE = "ats-opencv.exe";

        private readonly string args;
        private string errorCode = null;
        private string errorMessage = null;

        private string exeFile = null;

        private string crop = null;

        private string getExeFile()
        {
            string exeFile;
            exeFile = exeFilePath(System.Environment.GetEnvironmentVariable(ATS_OPENCV_HOME, EnvironmentVariableTarget.Machine));
            if (exeFile == null)
            {
                exeFile = exeFilePath(System.Environment.GetEnvironmentVariable(ATS_OPENCV_HOME));
                if (exeFile == null)
                {
                    exeFile = exeFilePath(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"/ats-opencv");
                    if (exeFile == null)
                    {
                        exeFile = exeFilePath(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/ats/tools/ats-opencv");
                    }
                }
                exeFile.Replace(@"/", @"\" );
            }
            
            return exeFile;
        }
        public ShapesRecognition(string crop, int duration, int device, DesktopManager desktop, bool record)
        {
            exeFile = getExeFile();

            if (exeFile != null)
            {
                if (device == 0 || device < desktop.GetDevicesCount())
                {

                    StringBuilder sb = new StringBuilder();
                    sb.Append("-t ")
                        .Append(duration)
                        .Append(" -cap ")
                        .Append(device)
                        .Append(" -crop ")
                        .Append(crop)
                        .Append(" -rec ")
                        .Append(record)
                        .Append(" -fps 20");

                    args = sb.ToString().ToLower();
                }
                else
                {
                    errorCode = "-50";
                    errorMessage = "device not found";
                }
            }
            else
            {
                //TODO download ats-opencv from actiontestscript server
                errorCode = "-50";
                errorMessage = @"unable to find 'ats-opencv.exe' file, please go to ats-opencv README url : https://gitlab.com/actiontestscript/ats-opencv/-/blob/main/README.md#install-ats-opencv-install-compiled-binaries-files";
            }
        }
        public ShapesRecognition(string[] commandsData, DesktopManager desktop, bool record)
        {
            exeFile = getExeFile();

            if (exeFile != null)
            {
                _ = int.TryParse(commandsData[1], out int device);

                if (device == 0 || device < desktop.GetDevicesCount())
                {
                    crop = commandsData[2] + "," + commandsData[3] + "," + commandsData[4] + "," + commandsData[5];

                    StringBuilder sb = new StringBuilder();
                    sb.Append("-t ")
                        .Append(commandsData[0])
                        .Append(" -cap ")
                        .Append(device)
                        .Append(" -crop ")
                        .Append(crop)
                        .Append(" -rec ")
                        .Append(record)
                        .Append(" -fps 20");

                    args = sb.ToString().ToLower();
                }
                else
                {
                    errorCode = "-50";
                    errorMessage = "device not found";
                }
            }
            else
            {
                //TODO download ats-opencv from actiontestscript server
                errorCode = "-50";
                errorMessage = @"unable to find 'ats-opencv.exe' file, please go to ats-opencv README url : https://gitlab.com/actiontestscript/ats-opencv/-/blob/main/README.md#install-ats-opencv-install-compiled-binaries-files";
            }
        }

        private static string exeFilePath(string path)
        {
            if(path != null)
            {
                path += "\\" + EXEC_FILE;

                if (File.Exists(path))
                {
                    return path;
                }
            }
            return null;
        }

        public int GetErrorCode()
        {
            try
            {
                return Int32.Parse(errorCode);
            }
            catch { }

            return 0;
        }

        public string GetErrorMessage()
        {
            return errorMessage;
        }

        //internal new DesktopData[] LaunchProc(VisualRecorder recorder)
        internal DesktopData[] LaunchProc(VisualRecorder recorder)
        {
            if (errorCode == null && exeFile != null)
            {
                bool record = recorder.IsActivated();

                string path = null;
                string shapesCount = null;
                string shapesAverage = null;

                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = exeFile,
                        Arguments = args,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                try
                {
                    proc.Start();
                }catch(Exception e)
                {
                    return new DesktopData[] { new DesktopData("error-code", -50), new DesktopData("error-message", "unable to start ats-opencv : " + e.Message) };
                }

                while (!proc.StandardOutput.EndOfStream)
                {
                    
                    string line = proc.StandardOutput.ReadLine();
                    if (line.StartsWith(COUNT))
                    {
                        shapesCount = line.Substring(COUNT.Length + 1);
                    }
                    else if (line.StartsWith(AVERAGE))
                    {
                        shapesAverage = line.Substring(AVERAGE.Length + 1);
                    }
                    else if (line.StartsWith(OUTPUT))
                    {
                        path = line.Substring(OUTPUT.Length + 1);
                    }
                    else if (line.StartsWith(ERROR_CODE))
                    {
                        errorCode = line.Substring(ERROR_CODE.Length + 1);
                    }
                    else if (line.StartsWith(ERROR_MESSAGE))
                    {
                        errorMessage = line.Substring(ERROR_MESSAGE.Length + 1);
                    }
                }

                if (errorCode == null)
                {
                    if (record)
                    {
                        recorder.AddRecord(path);
                    }
                    return new DesktopData[] { new DesktopData("shapes-count", shapesCount), new DesktopData("shapes-average", shapesAverage) };
                }
            }

            return new DesktopData[] { new DesktopData("error-code", errorCode), new DesktopData("error-message", errorMessage) };
        }
    }
}