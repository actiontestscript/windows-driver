﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace windowsdriver.utils
{
    public class FindAppProp
    {

        #region getAppVersion
        public static string getAppVersion(string appPath)
        {
            if (File.Exists(appPath))
            {
                return FileVersionInfo.GetVersionInfo(appPath).FileVersion;
            }

            return null;
        }
        #endregion

        #region getAppPath
        /// <summary>
        /// Try to get the app path in different way
        /// </summary>
        /// <param name="pathBReg">Path in the windows register</param>
        /// <param name="suffix">Path without the application path</param>
        /// <param name="processname">Process name</param>
        /// <param name="pathContain">In addition for the search in the processes, element that contains the string of the path of the app</param>
        /// <param name="valueName">Value in the windows register</param>
        /// <returns>The path or defaultIfEmpty</returns>
        public static string getAppPath(string pathBReg, string suffix, string processname, string pathContain = "", string valueName = null)
        {
            string path = searchPathInAppDir(suffix);
            if (path is null || path == string.Empty)
            {
                path = searchPathInProcess(processname, pathContain);
            }
            else if (path is null || path == string.Empty)
            {
                path = searchPathInBreg(pathBReg, valueName);
            }
            return (path is null || path == string.Empty ? null : path);
        }
        #endregion

        #region searchPathInAppDir
        /// <summary>
        /// Search the app path in application directory
        /// </summary>
        /// <param name="suffix">Path without the application path</param>
        /// <returns>The path or defaultIfEmpty</returns>
        private static string searchPathInAppDir(string suffix)
        {
            var prefixes = new List<string> { Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) };
            var programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
            var programFilesx86 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

            string bregPathProgFile = @"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion";
            string bregKeyProgFile = "ProgramW6432Dir";

            if (programFilesx86 != programFiles)
            {
                prefixes.Add(programFiles);
            }
            else
            {
                var programFilesDirFromReg = Microsoft.Win32.Registry.GetValue(bregPathProgFile, bregKeyProgFile, null) as string;
                if (programFilesDirFromReg != null) prefixes.Add(programFilesDirFromReg);
            }

            prefixes.Add(programFilesx86);
            return prefixes.Distinct().
                    Select(prefix => Path.Combine(prefix, suffix)).
                    FirstOrDefault(File.Exists);
        }
        #endregion

        #region searchPathInProcess
        /// <summary>
        /// Search the app path in process
        /// </summary>
        /// <param name="processname">Process name</param>
        /// <returns>The path or defaultIfEmpty</returns>
        private static string searchPathInProcess(string processname, string pathContain = "")
        {
            Process[] processCollection = Process.GetProcesses();
            foreach (Process p in processCollection)
            {
                if (p.ProcessName.ToLower() == processname.ToLower())
                {
                    try
                    {
                        string fileName = p.MainModule.FileName;
                        if (fileName != null && (pathContain == "" || fileName.ToLower().Contains(pathContain.ToLower())))
                        {
                            return fileName;
                        }
                    }
                    catch { }
                }
            }
            return null;
        }
        #endregion

        #region searchPathInBreg
        /// <summary>
        /// Search the app path in windows register
        /// </summary>
        /// <param name="pathBReg">Path in the windows register</param>
        /// <returns>The path or defaultIfEmpty</returns>
        private static string searchPathInBreg(string pathBReg, string valueName = null)
        {
            var pathTmp = Microsoft.Win32.Registry.GetValue(pathBReg, valueName, null) as string;
            if (pathTmp != null && pathTmp is string)
            {
                var split = ((string)pathTmp).Split('\"');
                if (split.Length >= 2)
                {
                    return split[1];
                }
                else
                {
                    return (string)pathTmp;
                }
            }
            return null;
        }
        #endregion
    }
}
