﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Definitions;
using FlaUI.Core.EventHandlers;

namespace windowsdriver.desktop
{
    public class PopupHandle
    {
        public int Pid;
        public AutomationElement Elem;

        private StructureChangedEventHandlerBase RemoveEvent;

        public PopupHandle(DesktopManager desktop, int pid, AutomationElement elem)
        {
            this.Pid = pid;
            this.Elem = elem;

            RemoveEvent = elem.RegisterStructureChangedEvent(TreeScope.Element, (removed, id, obj) => desktop.PopupRemove(this));
        }

        public void Dispose()
        {
            RemoveEvent.Dispose();
            RemoveEvent = null;

            Elem = null;
        }

        public AtsElement[] GetListItems()
        {
            AutomationElement[] items = Elem.FindAllDescendants(Elem.ConditionFactory.ByControlType(ControlType.ListItem));
            int len = items.Length;

            AtsElement[] result = new AtsElement[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = new AtsElement(items[i]);
            }
            return result;
        }

        public AutomationElement[] GetListElements()
        {
            return Elem.FindAllDescendants(Elem.ConditionFactory.ByControlType(ControlType.ListItem));
        }

        public AutomationElement[] GetElements()
        {
            AutomationElement[] desc = Elem.FindAllDescendants();
            int len = desc.Length;
            AutomationElement[] result = new AutomationElement[len + 1];

            int i = 0;
            foreach (AutomationElement e in desc)
            {
                result[i] = e;
                i++;
            }

            result[len] = Elem;
            return result;
        }
    }
}
