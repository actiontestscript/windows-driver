﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using windowsdriver.record;
using windowsdriver.Utils.Logger;

[DataContract(Name = "com.ats.recorder.VisualAction")]
public class VisualAction : IVisualAction
{
    private const string START_SCRIPT = "StartScriptAction";
    public const string WEBP_TYPE = "webp";

    protected readonly List<Bitmap> bitmapsList = new List<Bitmap>();

    public VisualAction()
    {
        this.Error = 0;
    }

    public VisualAction(String name):this()
    {
        this.Script = name;
        this.ChannelBound = new TestBound(new double[] { 0, 0, 10, 10 });
        this.Type = START_SCRIPT;
    }

    public virtual void WriteAction(DotAmf.Serialization.DataContractAmfSerializer serializer, BufferedStream stream)
    {
        WriteAction(serializer, stream, this);
    }

    protected static void WriteAction(DotAmf.Serialization.DataContractAmfSerializer serializer, BufferedStream stream, VisualAction action)
    {
        serializer.WriteObject(stream, action);
        stream.Flush();
    }

    public static byte[] GetScreenshot(string uri)
    {
        return GetScreenshotStream(uri);
    }

    public static byte[] GetScreenshotStream(string uriString)
    {
        var uri = new Uri(uriString);
        String hostname = uri.Host;
        int port = uri.Port;

        try
        {
            var client = new TcpClient(hostname, port);

            var dataString = "hires";
            byte[] data = Encoding.ASCII.GetBytes(dataString);

            var headerString = "POST /screenshot HTTP/1.1\r\nUser-Agent: Windows Driver\r\nDate: " + DateTime.Now + "\r\nContent-Type: " + "text/plain" + "\r\nContent-Length: " + data.Length + "\r\n\r\n" + dataString;
            byte[] header = Encoding.ASCII.GetBytes(headerString);

            NetworkStream stream = client.GetStream();
            stream.Write(header, 0, header.Length);

            MemoryStream memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);

            stream.Close();
            client.Close();

            return ParseStream(memoryStream.ToArray());
        }
        catch (ArgumentNullException e)
        {
            AtsLogger.WriteError("ArgumentNullException: {0}", e);
        }
        catch (SocketException e)
        {
            AtsLogger.WriteError("SocketException: {0}", e);
        }

        return new byte[0];
    }

    private static byte[] ParseStream(byte[] stream)
    {
        string str = Encoding.ASCII.GetString(stream, 0, stream.Length);
        var stringArray = Regex.Split(str, "\r\n\r\n");
        var headerBytes = Encoding.ASCII.GetBytes(stringArray[0] + "\r\n\r\n");

        var screenshot = new byte[stream.Length - headerBytes.Length];
        Array.Copy(stream, headerBytes.Length, screenshot, 0, screenshot.Length);
        return screenshot;
    }

    public static Bitmap GetScreenshotImage(string uri)
    {
        Bitmap bmp;
        using (var ms = new MemoryStream(GetScreenshotStream(uri)))
        {
            bmp = new Bitmap(ms);
        }
        return bmp;
    }

    public VisualAction(bool stop, string type, int line, string script, long timeLine, string channelName, double[] channelBound, string imgType, Bitmap img) : this()
    {
        this.Type = type;
        this.Line = line;
        this.Script = script;
        this.TimeLine = timeLine;
        this.ChannelName = channelName;
        this.ChannelBound = new TestBound(channelBound);
        this.imageType = imgType;
        this.ImageRef = 0;
        this.Stop = stop;

        if(img != null)
        {
            this.bitmapsList.Add(img);
        }
    }

    public VisualAction(bool stop, string type, int line, string script, long timeLine, string channelName, double[] channelBound, string imageType, Bitmap img, PerformanceCounter cpu, PerformanceCounter ram, float netSent, float netReceived) : this()
    {
        this.Type = type;
        this.Line = line;
        this.Script = script;
        this.TimeLine = timeLine;
        this.ChannelName = channelName;
        this.ChannelBound = new TestBound(channelBound);
        this.bitmapsList.Add(img);
        this.imageType = imageType;
        this.ImageRef = 0;
        this.Stop = stop;
    }

    public VisualAction(VisualActionSync action) : this()
    {
        this.Script = action.Script;
        this.Stop = action.Stop;
        this.ChannelName = action.ChannelName;
        this.ChannelBound = action.ChannelBound;
        this.Data = action.Data;
        this.Record = action.Record;
        this.bitmapsList = action.bitmapsList;
        this.imageType = action.ImageType;
        this.Index = action.Index;
        this.Line = action.Line;
        this.Error = action.Error;
        this.Duration = action.Duration;
        this.TimeLine = action.TimeLine;
        this.Type = action.Type;
        this.Value = action.Value;
        this.ImageRef = action.ImageRef;
        this.Element = action.Element;
    }

    public virtual void AddImage(VisualRecorder recorder, double[] channelBound, bool isRef)
    { }

    public virtual void AddImage(VisualRecorder recorder, string url, double[] channelBound, bool isRef)
    { }

    public virtual void AddRecord(string path)
    {
        Record = File.ReadAllBytes(path);
        if (path.EndsWith("." + WEBP_TYPE))
        {
            imageType = WEBP_TYPE;
        }
        else if (path.EndsWith(".mp4"))
        {
            imageType = "mp4";
        }
    }

    public virtual void AddImage(Bitmap img)
    {
        if(img != null)
        {
            this.imageType = WEBP_TYPE;
            this.bitmapsList.Add(img);
        }
    }
    public void AddImage(string type)
    {
        this.imageType = type;
    }

    public void setValue(string v)
    {
        if (v != null && v.Length > 0)
        {
            this.Value = "value::" + v;
        }
        else
        {
            this.Value = null;
        }
    }

    public void setData(string v)
    {
        if (v != null && v.Length > 0)
        {
            this.Data = "data::" + v;
        }
        else
        {
            this.Data = null;
        }
    }

    public void setDuration(long d)
    {
        this.Duration = d;
    }

    public void setError(int e)
    {
        this.Error = e;
    }

    public void setElement(VisualElement e)
    {
        this.Element = e;
    }

    public int setIndex(int i)
    {
        this.Index = i;
        return i + 1;
    }

    public void UpdateElementPosition(string hpos, string hposValue, string vpos, string vposValue)
    {
        if(Element != null)
        {
            Element.UpdatePosition(hpos, hposValue, vpos, vposValue);
        }
    }

    [DataMember(Name = "data")]
    public string Data;

    [DataMember(Name = "element")]
    public VisualElement Element;

    [DataMember(Name = "channelBound")]
    public TestBound ChannelBound;

    [DataMember(Name = "script")]
    public string Script;

    [DataMember(Name = "stop")]
    public bool Stop;

    [DataMember(Name = "channelName")]
    public string ChannelName;

    [DataMember(Name = "record")]
    public byte[] Record = new byte[1];

    [DataMember(Name = "images")]
    public byte[][] Images
    {
        get {

            if(bitmapsList.Count > 0 && Element != null && Element.Rectangle != null)
            {
                /*int imageRef = 0;
                if (Error != 0 && bitmapsList.Count > 0)
                {
                    imageRef = bitmapsList.Count - 1;
                }
                else if (bitmapsList.Count > ImageRef)
                {
                    imageRef = ImageRef;
                }*/

                int imageRef = bitmapsList.Count - 1;
                Rectangle rect = Element.GetRectangle(); 
                rect.Offset(0, -8); // TODO find why we have to do offset

                try
                {
                    using (Graphics g = Graphics.FromImage(bitmapsList[imageRef]))
                    {
                        g.DrawRectangle(new Pen(Color.DarkViolet, 6.0f), rect);
                        g.Dispose();
                    }
                }
                catch (Exception) { }

            }

            byte[][] bytesArray = bitmapsList.ConvertAll<byte[]>(a => VisualRecorder.GetWebpImage(a)).ToArray();
            bitmapsList.Clear();

            GC.Collect();

            return bytesArray;
        }
        set { }
    }

    private string imageType = WEBP_TYPE;

    [DataMember(Name = "imageType")]
    public string ImageType
    {
        get { if (string.IsNullOrEmpty(imageType)) { return WEBP_TYPE; } else { return imageType; } }
        set { }
    }

    [DataMember(Name = "index")]
    public int Index;

    [DataMember(Name = "line")]
    public int Line;

    [DataMember(Name = "error")]
    public int Error;

    [DataMember(Name = "duration")]
    public long Duration;

    [DataMember(Name = "timeLine")]
    public long TimeLine;

    [DataMember(Name = "type")]
    public string Type;

    [DataMember(Name = "value")]
    public string Value;

    [DataMember(Name = "imageRef")]
    public int ImageRef;
}