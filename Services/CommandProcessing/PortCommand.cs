﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
using System.Net;
using System;
using windowsdriver.utils;

namespace windowsdriver.Services.CommandProcessing
{
    public class PortCommand : CommandBase
    {
        public override string CommandType => "port";

        //public override string HelpGlobal => "  port Set listen port system driver";
//        public override string HelpGlobal => "port Set listen port system driver\n           Usage : --port=9700 (9700 default port)\n           port between (1024-65535)";
        public override string HelpGlobal => "--port=PORT\t\t\tport to listen on. port between (1024-65535)";
        public override string HelpDetails => "--port=PORT\t\t\tport to listen on. port between (1024-65535)";

        public override void Execute(string command, CommandResult commandResult)
        {
            var portData = command.Split('=');
            if (portData.Length == 1) return;
            try
            {
                var port = int.Parse(portData[1]);
                if (port < 1024 || port > 65535)
                {
                    throw new OverflowException($"Port value must be between {Network.DEFAULT_PORT} and {IPEndPoint.MaxPort}, but {port} was given.");
                }
                commandResult.port = int.Parse(portData[1]);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid port format. Please select a numeric port value between {0} and {1}, Windowsdriver cannot be started!", Network.DEFAULT_PORT, IPEndPoint.MaxPort);
                Environment.Exit(-1);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }
        }
    }
}
