﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Linq;

namespace windowsdriver.Services.CommandProcessing
{
    public class UsageCommand : CommandBase
    {
        public override string CommandType => "help";
        public override string HelpGlobal => "--help\t\t\tprint this help";
        public override string HelpDetails => "--help\t\t\tprint this help";

        private CommandResult commandResult;

        public override void Execute(string command, CommandResult commandResult)
        {
            this.commandResult = commandResult;

            if (commandResult.indexOfHelp == 0)
            {
                PrintGlobalUsage();
            }
            else
            {
                //command usage
            }
            Environment.Exit(0);
        }

        private void PrintGlobalUsage()
        {
            var outWriter = Console.Out;
            outWriter.WriteLine("Usage: WindowsDriver [options]");
            outWriter.WriteLine("Options:");
            foreach (var cmd in commandResult.ListCommands.OrderBy(c => c.CommandType))
            {
                outWriter.WriteLine($"  {cmd.HelpGlobal}");
            }
            outWriter.WriteLine($"");
        }
    }
}
