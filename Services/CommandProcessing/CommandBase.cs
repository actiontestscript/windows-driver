﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

namespace windowsdriver.Services.CommandProcessing
{
    public abstract class CommandBase : ICommand
    {
        public abstract string CommandType { get; }
        public abstract string HelpGlobal { get; }
        public abstract string HelpDetails{ get; }

        protected string[] Args { get; set; }
        public virtual bool CanExecute(string command, string[] args, CommandResult commandResult)
        {
            this.Args = args;
            var prefixes = new[] { "--", "-", "/" };

            foreach (var prefix in prefixes)
            {
                if (!command.StartsWith(prefix)) continue;
                command = command.Substring(prefix.Length);
                
                if (commandResult.indexOfHelp > 0 && !command.Equals("help"))
                {
                    return false;
                }

                return command.ToLower().StartsWith(CommandType);
            }
            return command.ToLower().StartsWith(CommandType);
        }
        public abstract void Execute(string command, CommandResult commandResult);
    }
}
