﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using windowsdriver;
//using windowsdriver.executions.DataConverter;
using windowsdriver.utils;
using windowsdriver.utils.JsonUtils;
//using static windowsdriver.executions.DataConverter.RecordDC;
//using RecordDriverInfo = windowsdriver.executions.DataConverter.RecordDC;
//using static windowsdriver.w3c.Window;
//using windowsdriver.w3c;

// TODO A renommer  en tant que DrinverInfo quand suppression DataConverter

//namespace recorder
namespace executions.recorder
{
/*
    class DriverInfo
    {
        private readonly Boolean headless;
        private readonly String sessionId;
        private readonly String screenshotUrl;

        public DriverInfo(JObject node)
        {
            if (node.ContainsKey("driverInfo"))
            {
                JObject jsonValue = (JObject)node["driverInfo"];
                headless = JsonUtils.GetJsonValue(jsonValue, "headless", false);
                sessionId = JsonUtils.GetJsonValue(jsonValue, "sessionId", "");
                screenshotUrl = JsonUtils.GetJsonValue(jsonValue, "screenshotUrl", "");
            }
        }
        public Boolean getHeadless() { return headless; }
        public String getSessionId() { return sessionId; }
        public String getScreenshotUrl() { return screenshotUrl; }

    }
}//namespace

class RecordExecution : AtsExecution
{
    private enum RecordType
    {
        Stop = 0,
        Screenshot = 1,
        Start = 2,
        Create = 3,
        Image = 4,
        Value = 5,
        Data = 6,
        Status = 7,
        Element = 8,
        Position = 9,
        Download = 10,
        ImageMobile = 11,
        CreateMobile = 12,
        ScreenshotMobile = 13,
        Summary = 14,
        TestJson = 15,
    };

    Dictionary<string, int> DriverTypeDict = new Dictionary<string, int>()
    {
        {"stop", 0},
        {"screenshot", 1},
        {"start", 2},
        {"create", 3},
        {"image", 4},
        {"value", 5},
        {"data", 6},
        {"status", 7},
        {"element", 8},
        {"position", 9},
        {"download", 10},
        {"imagemobile", 11},
        {"createmobile", 12},
        {"screenshotmobile", 13},
        {"summary", 14},
        {"testJson", 15},
    };

    private RecordType? getEnumRecordType(string type)
    {
        type = type.ToLower();
        RecordType? recordType = null;
        foreach(var pair in DriverTypeDict)
        {
            if(string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
            {
                recordType = (RecordType) pair.Value;
                break;
            }
        }
        return recordType;
    }

    private readonly string GET = "GET";
    private readonly string DELETE = "DELETE";
    private readonly string POST = "POST";

    RecordDC dataConverter;

    public RecordExecution(string method, string stype, JObject node, VisualRecorder recorder) : base() {
        response.type = DesktopResponseType.W3CJSONRESPONSE;
        RecordExec(method, stype, node, recorder);
    }

    public RecordExecution(string method, string stype, string postData, VisualRecorder recorder) : base()
    {
        response.type = DesktopResponseType.JSON;
        string[] commandsData = new List<string>().ToArray();
        dataConverter = new RecordDC(postData);
        if (DriverTypeDict.TryGetValue(stype.ToLower(), out int sTypeVal))
        {
            RecordExec(method, sTypeVal, commandsData, recorder);
        }
        else
        {
            //TODO: Send exception
        }
    }

    public RecordExecution(string method, int stype, string[] commandsData, VisualRecorder recorder) : base()
    {
        response.type = DesktopResponseType.AMF;
        dataConverter = new RecordDC(commandsData);
        RecordExec(method, stype, commandsData, recorder);
    }

    

    public void RecordExec(string method, string type, JObject node, VisualRecorder recorder)
    {
        //msg par default
        response.ErrorCode = 200;
        response.ErrorMessage = "";

        RecordType? recordType = getEnumRecordType(type);
        if (recordType == null)
        {
            jsonResponse.SetMsgErrorCode("unsupported_operation");
            return;
            //response.SetError(500, "Not implemented");
        }

        else if (DELETE.Equals(method))
        {
            AtsLogger.WriteAll("record-delete");
            jsonResponse.SetMsgErrorCode("method_delete_not_implemented");
            return;
            //throw new Exception("The method DELETE is not implemented");
        }
        else if (GET.Equals(method))
        {
            //TODO  - A Remplacer quand ok json
            recorder.DriverInfo driverInfo = new recorder.DriverInfo(node);
            DriverInfo dI = new DriverInfo();
            dI.headless = driverInfo.getHeadless();
            dI.sessionId = driverInfo.getSessionId();
            dI.screenshotUrl = driverInfo.getScreenshotUrl();
            // TODO fin a Remplacer

            if (recordType == RecordType.Download)
            {
                response.type = DesktopResponseType.atsvFilePath;
                response.atsvFilePath = recorder.GetDownloadFile();

                AtsLogger.WriteAll("record-download: {0}", response.atsvFilePath);
            }
        }
        else if (POST.Equals(method))
        {
            //TODO  - A Remplacer quand ok json
            recorder.DriverInfo driverInfo = new recorder.DriverInfo(node);
            DriverInfo dI = new DriverInfo();
            dI.headless = driverInfo.getHeadless();
            dI.sessionId = driverInfo.getSessionId();
            dI.screenshotUrl = driverInfo.getScreenshotUrl();
            // TODO fin a Remplacer
            if (recordType == RecordType.Download)
            {
                response.type = DesktopResponseType.atsvFilePath;
                response.atsvFilePath = recorder.GetDownloadFile();

                AtsLogger.WriteAll("record-download: {0}", response.atsvFilePath);
            }
            else if (recordType == RecordType.Stop)
            {
                recorder.Stop();
                response.ErrorCode = 200;
                response.ErrorMessage = null;

                AtsLogger.WriteAll("record-stop");
            }
            else if (recordType == RecordType.Summary)
            {
                Boolean passed = JsonUtils.GetJsonValue<Boolean>(node, "passed", false);
                int actions = JsonUtils.GetJsonValue<int>(node, "actions", 0);
                string suiteName = JsonUtils.GetJsonValue<string>(node, "suiteName", null);
                string testName = JsonUtils.GetJsonValue<string>(node, "testName", null);
                string data = JsonUtils.GetJsonValue<string>(node, "data", "");
                string errorScript = JsonUtils.GetJsonValue<string>(node, "errorScript", null);
                int errorLine = JsonUtils.GetJsonValue<int>(node, "errorLine", 0);
                string errorMessage = JsonUtils.GetJsonValue<string>(node, "errorMessage", "");

                if (testName != null && suiteName != null && suiteName != errorScript)
                {
                    AtsLogger.WriteAll("record-error-summary: test={0}, suite={1}", testName, suiteName);
                    try
                    {
                        recorder.Summary(passed, actions, suiteName, testName, data, errorScript, errorLine, errorMessage);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-error-summary-error: {0}", ex.Message);
                        jsonResponse.SetMsgErrorCode("ats_json_data_null",ex.Message );
                    }
                }
                else
                {
                    AtsLogger.WriteAll("record-summary-data: test={0}, suite={1}, passed={2}", testName, suiteName, passed);
                    try
                    {
                        recorder.Summary(passed, actions, suiteName, testName, data);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-summary-data-error: {0}", ex.Message);
                        jsonResponse.SetMsgErrorCode("ats_json_data_null", ex.Message);
                    }
                }
            }
            else if (recordType == RecordType.TestJson)
            {
                jsonResponse.SetMsgErrorCode("ats_error_browser_not_found");
                //setJsonResponseSuccess();
                //this.SetJsonValueObject(new JsonResponseErrorValue("error", "message", "stacktrace", "et datas"));
            }
            else
            {
                if (recordType == RecordType.Screenshot)
                {
                    #region Screenshot
                    (DtoScreenshot dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoScreenshot>(dataConverter.ArrayToDtoScreenshot);
                    response.ErrorCode = responseCode;
                    response.ErrorMessage = responseMsg;

                    if (dto != null)
                    {
                        AtsLogger.WriteAll("record-screenshot: bound=[{0}, {1}, {2}, {3}]", dto.x, dto.y, dto.w, dto.h);
                        try
                        {
                            response.Image = recorder.ScreenCapture(dto.x, dto.y, dto.w, dto.h);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-screenshot-error: {0}", ex.Message);
                        }
                    }
                    #endregion
                }
                else if (recordType == RecordType.ScreenshotMobile)
                {
                    (DtoScreenshotMobile dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoScreenshotMobile>(dataConverter.ArrayToDtoScreenshotMobile);
                    response.type = DesktopResponseType.AMF;
                    response.ErrorCode = responseCode;
                    response.ErrorMessage = responseMsg;

                    if (dto != null)
                    {
                        AtsLogger.WriteAll("record-screenshot-mobile: {0}", dto.uri);
                        try
                        {
                            response.Image = VisualAction.GetScreenshot(dto.uri);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-screenshot-mobile-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Start)
                {
                    AtsLogger.WriteAll("record-start");
                    ActionStart(recorder, node);
                }
                else if (recordType == RecordType.Create)
                {
                    #region ActionCreate
                    string actionType = JsonUtils.GetJsonValue<string>(node, "actionType", null);
                    int line = JsonUtils.GetJsonValue<int>(node, "line", 0);
                    string script = JsonUtils.GetJsonValue<string>(node, "script", null);
                    long timeLine = JsonUtils.GetJsonValue<long>(node, "timeLine", 0);
                    string channelName = JsonUtils.GetJsonValue<string>(node, "channelName", null);
                    double[] channelDimension = new double[4];
                    channelDimension[0] = JsonUtils.GetJsonValue<int>(node, "channelDimensionX", 0);
                    channelDimension[1] = JsonUtils.GetJsonValue<int>(node, "channelDimensionY", 0);
                    channelDimension[2] = JsonUtils.GetJsonValue<int>(node, "channelDimensionWidth", 1);
                    channelDimension[3] = JsonUtils.GetJsonValue<int>(node, "channelDimensionHeight", 1);
                    Boolean sync = JsonUtils.GetJsonValue<Boolean>(node, "sync", false);
                    Boolean stop = JsonUtils.GetJsonValue<Boolean>(node, "stop", false);

                    if (actionType != null && script != null && channelName != null)
                    {
                        AtsLogger.WriteAll("record-create: name={0}, bound=[{1}]", channelName, string.Join(",", channelDimension));
                        try
                        {
                            //TODA A remplacer quand json ok
                            //recorder.Create(actionType, line, script, timeLine, channelName, channelDimension, sync, stop, driverInfo);
                            recorder.Create(actionType, line, script, timeLine, channelName, channelDimension, sync, stop, dI);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-create-error: {0}", ex.Message);
                        }

                        #endregion
                    }
                }
                else if (recordType == RecordType.CreateMobile)
                {
                    string actionType = JsonUtils.GetJsonValue<string>(node, "actionType", null);
                    int line = JsonUtils.GetJsonValue<int>(node, "line", 0);
                    string script = JsonUtils.GetJsonValue<string>(node, "script", null);
                    long timeLine = JsonUtils.GetJsonValue<long>(node, "timeLine", 0);
                    string channelName = JsonUtils.GetJsonValue<string>(node, "channelName", null);
                    double[] channelDimension = new double[4];
                    channelDimension[0] = JsonUtils.GetJsonValue<int>(node, "channelDimensionX", 0);
                    channelDimension[1] = JsonUtils.GetJsonValue<int>(node, "channelDimensionY", 0);
                    channelDimension[2] = JsonUtils.GetJsonValue<int>(node, "channelDimensionWidth", 1);
                    channelDimension[3] = JsonUtils.GetJsonValue<int>(node, "channelDimensionHeight", 1);
                    string url = JsonUtils.GetJsonValue<string>(node, "url", null);
                    Boolean sync = JsonUtils.GetJsonValue<Boolean>(node, "sync", false);
                    Boolean stop = JsonUtils.GetJsonValue<Boolean>(node, "stop", false);
                    if (actionType != null && script != null && channelName != null && url != null)
                    {
                        AtsLogger.WriteAll("record-create-mobile: {0}", url);
                        try
                        {
                            recorder.CreateMobile(actionType, line, script, timeLine, channelName, channelDimension, url, sync, stop);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-create-mobile-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Image)
                {

                    double[] screenRect = JsonUtils.GetJsonArray<double>(node, "screenRect", null);
                    Boolean isRef = JsonUtils.GetJsonValue<Boolean>(node, "isRef", false);

                    try
                    {
                        //if (dto.DriverInfo != null && dto.DriverInfo.headless)
                        if (driverInfo.getHeadless())
                        {
                            AtsLogger.WriteAll("record-image: {0}", driverInfo.getScreenshotUrl());
                            recorder.AddImage(driverInfo.getScreenshotUrl());
                        }
                        else
                        {
                            AtsLogger.WriteAll("record-image: bound=[{0}]", String.Join(",", screenRect));
                            recorder.AddImage(screenRect, isRef);
                        }
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-image-error: {0}", ex.Message);
                    }

                }
                else if (recordType == RecordType.ImageMobile)
                {
                    double[] screenRect = JsonUtils.GetJsonArray<double>(node, "screenRect", null);
                    Boolean isRef = JsonUtils.GetJsonValue<Boolean>(node, "isRef", false);
                    string url = JsonUtils.GetJsonValue<string>(node, "url", null);
                    if (url != null)
                    {
                        AtsLogger.WriteAll("record-image-mobile: url={0}, bound=[{1}]", url, String.Join(",", screenRect));
                        try
                        {
                            recorder.AddImage(url, screenRect, isRef);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-image-mobile-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Value)
                {
                    string v = JsonUtils.GetJsonValue<string>(node, "v", null);
                    AtsLogger.WriteAll("record-value: v1");
                    if (v != null)
                    {
                        try
                        {
                            recorder.AddValue(v);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-value-error: {0}", ex.Message);
                        }
                    }
                }
                else if (recordType == RecordType.Data)
                {
                    string v1 = JsonUtils.GetJsonValue<string>(node, "v1", null);
                    string v2 = JsonUtils.GetJsonValue<string>(node, "v2", null);

                    AtsLogger.WriteAll("record-data: v1 and v2");
                    if (v1 != null && v2 != null)
                    {
                        try
                        {
                            recorder.AddData(v1, v2);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-data-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Status)
                {

                    int error = JsonUtils.GetJsonValue<int>(node, "error", -1);
                    long duration = JsonUtils.GetJsonValue<long>(node, "duration", -1);
                    if (error != -1 && duration != -1)
                    {
                        try
                        {
                            recorder.Status(error, duration);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-status-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Element)
                {

                    Double[] elementBound = JsonUtils.GetJsonArray<Double>(node, "elementBound", new Double[] { 0D, 0D, 0D, 0D });
                    long searchDuration = JsonUtils.GetJsonValue<long>(node, "searchDuration", -1);
                    int numElements = JsonUtils.GetJsonValue<int>(node, "numElements", -1);
                    string searchCriterias = JsonUtils.GetJsonValue<string>(node, "searchCriterias", null);
                    string tag = JsonUtils.GetJsonValue<string>(node, "tag", null);

                    if (numElements != -1)
                    {
                        AtsLogger.WriteAll("record-element: tag={0}", tag);
                        try
                        {
                            recorder.AddElement(elementBound, searchDuration, numElements, searchCriterias, tag);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-element-error: {0}", ex.Message);
                        }
                    }

                }
                else if (recordType == RecordType.Position)
                {
                    string hPos = JsonUtils.GetJsonValue<string>(node, "hpos", null);
                    string hPosValue = JsonUtils.GetJsonValue<string>(node, "hposValue", null);
                    string vPos = JsonUtils.GetJsonValue<string>(node, "vpos", null);
                    string vPosValue = JsonUtils.GetJsonValue<string>(node, "vposValue", null);
                    if (hPos != null && vPos != null)
                    {
                        AtsLogger.WriteAll("record-position: hpos={0}, vpos={1}", hPosValue, vPosValue);

                        try
                        {
                            recorder.AddPosition(hPos, hPosValue, vPos, vPosValue);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-position-error: {0}", ex.Message);
                        }
                    }
                }

                
 //                   else
 //                   {
 //                       response.type = DesktopResponseType.Text;
 //                       response.ErrorCode = 400;
 //                       response.ErrorMessage = "In Recorder, the action " + recordType + " for method " + method + " is not implemented";
 //                   }
                

            }
        }
    }

    #region RecordExecutionExec
    public void RecordExec(string method, int type, string[] commandsData, VisualRecorder recorder)
    {
        RecordType recordType = (RecordType)type;
        if (DELETE.Equals(method))
        {
            AtsLogger.WriteAll("record-delete");
            throw new Exception("The method DELETE is not implemented");
        }
        else if (GET.Equals(method))
        {
            if (recordType == RecordType.Download)
            {
                response.type = DesktopResponseType.atsvFilePath;
                response.atsvFilePath = recorder.GetDownloadFile();

                AtsLogger.WriteAll("record-download: {0}", response.atsvFilePath);
            }
        }
        else if (POST.Equals(method))
        {
            if (recordType == RecordType.Stop)
            {
                recorder.Stop();
                response.ErrorCode = 200;
                response.ErrorMessage = dataConverter.SerialiseSuccess();

                AtsLogger.WriteAll("record-stop");
            }
            else if (recordType == RecordType.Summary)
            {
                #region Summary
                (DtoSummary dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoSummary>(dataConverter.ArrayToDtoSummary);
                response.ErrorCode = responseCode;
                response.ErrorMessage = responseMsg;

                if (dto != null)
                {
                    if (!dto.passed && (dto.errorMessage != null && dto.errorMessage != string.Empty))
                    {
                        AtsLogger.WriteAll("record-error-summary: test={0}, suite={1}", dto.testName, dto.suiteName);
                        try
                        {
                            recorder.Summary(dto.passed, dto.actions, dto.suiteName, dto.testName, dto.data, dto.errorScript, dto.errorLine, dto.errorMessage);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-error-summary-error: {0}", ex.Message);
                        }
                    }
                    else
                    {
                        AtsLogger.WriteAll("record-summary-data: test={0}, suite={1}, passed={2}", dto.testName, dto.suiteName, dto.passed);
                        try
                        {
                            recorder.Summary(dto.passed, dto.actions, dto.suiteName, dto.testName, dto.data);
                        }
                        catch (Exception ex)
                        {
                            AtsLogger.WriteError("record-summary-data-error: {0}", ex.Message);
                        }
                    }
                }

                #endregion
            }
            else
            {
                if (dataConverter.HasData())
                {
                    if (recordType == RecordType.Screenshot)
                    {
                        #region Screenshot
                        (DtoScreenshot dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoScreenshot>(dataConverter.ArrayToDtoScreenshot);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-screenshot: bound=[{0}, {1}, {2}, {3}]", dto.x, dto.y, dto.w, dto.h);
                            try
                            {
                                response.Image = recorder.ScreenCapture(dto.x, dto.y, dto.w, dto.h);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-screenshot-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.ScreenshotMobile)
                    {
                        #region ScreenshotMobile
                        (DtoScreenshotMobile dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoScreenshotMobile>(dataConverter.ArrayToDtoScreenshotMobile);
                        response.type = DesktopResponseType.AMF;
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-screenshot-mobile: {0}", dto.uri);
                            try
                            {
                                response.Image = VisualAction.GetScreenshot(dto.uri);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-screenshot-mobile-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Start)
                    {
                        AtsLogger.WriteAll("record-start");
                        ActionStart(recorder);
                    }
                    else if (recordType == RecordType.Create)
                    {
                        #region ActionCreate
                        (DtoCreate dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoCreate>(dataConverter.ArrayToDtoCreate);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-create: name={0}, bound=[{1}]", dto.channelName, string.Join(",", dto.channelDimension));
                            try
                            {
                                recorder.Create(dto.actionType, dto.line, dto.script, dto.timeLine, dto.channelName, dto.channelDimension, dto.sync, dto.stop, dto.driverInfo);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-create-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.CreateMobile)
                    {
                        #region CreateMobile
                        (DtoCreateMobile dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoCreateMobile>(dataConverter.ArrayToDtoCreateMobile);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-create-mobile: {0}", dto.url);
                            try
                            {
                                recorder.CreateMobile(dto.actionType, dto.line, dto.script, dto.timeLine, dto.channelName, dto.channelDimension, dto.url, dto.sync, dto.stop);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-create-mobile-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Image)
                    {
                        #region Image
                        (DtoImage dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoImage>(dataConverter.ArrayToDtoImage);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            try
                            {
                                if (dto.DriverInfo != null && dto.DriverInfo.headless)
                                {
                                    AtsLogger.WriteAll("record-image: {0}", dto.DriverInfo.screenshotUrl);
                                    recorder.AddImage(dto.DriverInfo.screenshotUrl);
                                }
                                else
                                {
                                    AtsLogger.WriteAll("record-image: bound=[{0}]", String.Join(",", dto.screenRect));
                                    recorder.AddImage(dto.screenRect, dto.isRef);
                                }
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-image-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.ImageMobile)
                    {
                        #region ImageMobile
                        (DtoImageMobile dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoImageMobile>(dataConverter.ArrayToDtoImageMobile);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-image-mobile: url={0}, bound=[{1}]", dto.url, String.Join(",", dto.screenRect));
                            try
                            {
                                recorder.AddImage(dto.url, dto.screenRect, dto.isRef);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-image-mobile-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Value)
                    {
                        #region Value
                        (DtoValue dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoValue>(dataConverter.ArrayToDtoValue);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-value: v1");
                            try
                            {
                                recorder.AddValue(dto.v);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-value-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Data)
                    {
                        #region Data
                        (DtoData dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoData>(dataConverter.ArrayToDtoData);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-data: v1 and v2");
                            try
                            {
                                recorder.AddData(dto.v1, dto.v2);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-data-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Status)
                    {
                        #region Status
                        (DtoStatus dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoStatus>(dataConverter.ArrayToDtoStatus);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            try
                            {
                                recorder.Status(dto.error, dto.duration);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-status-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Element)
                    {
                        #region Element
                        (DtoElement dto, string responseMsg, int responseCode) = dataConverter.GetDataElement();
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-element: tag={0}", dto.tag);
                            try
                            {
                                recorder.AddElement(dto.elementBound, dto.searchDuration, dto.numElements, dto.searchCriterias, dto.tag);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-element-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                    else if (recordType == RecordType.Position)
                    {
                        #region Position
                        // (DtoPosition dto, string responseMsg, int responseCode) = dataConverter.GetDataPosition();
                        (DtoPosition dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoPosition>(dataConverter.ArrayToDtoPosition);
                        response.ErrorCode = responseCode;
                        response.ErrorMessage = responseMsg;

                        if (dto != null)
                        {
                            AtsLogger.WriteAll("record-position: hpos={0}, vpos={1}", dto.hposValue, dto.vposValue);

                            try
                            {
                                recorder.AddPosition(dto.hpos, dto.hposValue, dto.vpos, dto.vposValue);
                            }
                            catch (Exception ex)
                            {
                                AtsLogger.WriteError("record-position-error: {0}", ex.Message);
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    response.type = DesktopResponseType.Text;
                    response.ErrorCode = 400;
                    response.ErrorMessage = "In Recorder, the action " + recordType + " for method " + method + " is not implemented";
                }
            }
        }
    }
    #endregion

    #region ActionStart

    private void ActionStart(VisualRecorder recorder, JObject node)
    {
        string tempFolder = Capabilities.GetAtsRecorderTempFolder();
        long freeSpace = 0;

        #region GetFreeSpace
        try
        {
            DriveInfo drive = new DriveInfo(new FileInfo(tempFolder).Directory.Root.FullName);
            freeSpace = drive.AvailableFreeSpace;
        }
        catch { }
        #endregion

        if (freeSpace > 100000000)
        {
            #region directory management
            try
            {
                if (Directory.Exists(tempFolder))
                {
                    DirectoryInfo folder = new DirectoryInfo(tempFolder);
                    foreach (FileInfo file in folder.EnumerateFiles())
                    {
                        file.Delete();
                    }
                }
                else
                {
                    Directory.CreateDirectory(tempFolder);
                }
            }
            catch { }
            #endregion

            string id = JsonUtils.GetJsonValue<string>(node, "id", null);
            string fullName = JsonUtils.GetJsonValue<string>(node, "fullName", null);
            string description = JsonUtils.GetJsonValue<string>(node, "description", null);
            string author = JsonUtils.GetJsonValue<string>(node, "author", null);
            string groups = JsonUtils.GetJsonValue<string>(node, "groups", null);
            string preRequisites = JsonUtils.GetJsonValue<string>(node, "preRequisites", null);
            string externalId = JsonUtils.GetJsonValue<string>(node, "externalId", null);
            int videoQuality = JsonUtils.GetJsonValue<int>(node, "videoQuality", 2);
            string started = JsonUtils.GetJsonValue<string>(node, "started", null);

            if (id != null && fullName != null && started!=null)
            {
                response.atsvFilePath = tempFolder + "\\" + fullName;
                recorder.Start(tempFolder, id, fullName, description, author, groups, preRequisites, externalId, videoQuality, started);
            }

        }
        else
        {
            response.ErrorCode = -50;
            response.ErrorMessage = "Not enough space available on disk : " + (freeSpace / 1024 / 1024) + " Mo";
        }

    }

    private void ActionStart(VisualRecorder recorder)
    {
        string tempFolder = Capabilities.GetAtsRecorderTempFolder();
        long freeSpace = 0;

        #region GetFreeSpace
        try
        {
            DriveInfo drive = new DriveInfo(new FileInfo(tempFolder).Directory.Root.FullName);
            freeSpace = drive.AvailableFreeSpace;
        }
        catch { }
        #endregion

        if (freeSpace > 100000000)
        {
            #region directory management
            try
            {
                if (Directory.Exists(tempFolder))
                {
                    DirectoryInfo folder = new DirectoryInfo(tempFolder);
                    foreach (FileInfo file in folder.EnumerateFiles())
                    {
                        file.Delete();
                    }
                }
                else
                {
                    Directory.CreateDirectory(tempFolder);
                }
            }
            catch { }
            #endregion

            (DtoStart dto, string responseMsg, int responseCode) = dataConverter.GetData<DtoStart>(dataConverter.ArrayToDtoStart);

            response.ErrorCode = responseCode;
            response.ErrorMessage = responseMsg;

            if (dto != null)
            {
                response.atsvFilePath = tempFolder + "\\" + dto.fullName;
                recorder.Start(tempFolder, dto.id, dto.fullName, dto.description, dto.author, dto.groups, dto.prereq, dto.externalId, dto.videoQuality, dto.started);
            }
        }
        else
        {
            response.ErrorCode = -50;
            response.ErrorMessage = "Not enough space available on disk : " + (freeSpace / 1024 / 1024) + " Mo";
        }
    }
    #endregion
*/
}