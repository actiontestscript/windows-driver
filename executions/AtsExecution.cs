﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using DotAmf.Serialization;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using windowsdriver;
using windowsdriver.items;
using windowsdriver.Utils.Logger;
using windowsdriver.w3c;

class AtsExecution
{
    private static readonly DataContractAmfSerializer AmfSerializer = new DataContractAmfSerializer(typeof(DesktopResponse), new[] { typeof(AppData), typeof(DesktopData), typeof(AtsElement), typeof(DesktopWindow), typeof(DesktopElement)});

    protected DesktopResponse response;
    protected object jsonValueObject;
    protected JsonResponse jsonResponse;

    public AtsExecution()
    {
        response = new DesktopResponse();
        jsonResponse = new JsonResponse();
    }

    public AtsExecution(int error, bool atsAgent, string message)
    {
        response = new DesktopResponse(error, atsAgent, message);
    }

    /// <summary>
    /// Set objet for json serialization W3C other DesktopResponse
    /// </summary>
    /// <param name="jsonValueObject"></param>
    public void SetJsonValueObject(object jsonValueObject)
    {
        this.jsonValueObject = jsonValueObject;
    }


    public virtual bool Run(HttpListenerContext context)
    {
        try
        {
            if (response.type == DesktopResponseType.AMF ||
                response.type == DesktopResponseType.JSON ||
                response.type == DesktopResponseType.StopServer ||
                response.type == DesktopResponseType.Text)
            {
                using (HttpListenerResponse resp = context.Response)
                {
                    if (response.type == DesktopResponseType.AMF)
                    {
                        resp.ContentType = "application/x-amf";
                        AmfSerializer.WriteObject(resp.OutputStream, response);
                    }
                    else if (response.type == DesktopResponseType.StopServer)
                    {
                        AtsLogger.WriteInfo("Closing windowsdriver");
                        return false;
                    }
                    else if (response.type == DesktopResponseType.JSON)
                    {
                        byte[] bytes = (response.ErrorMessage != null ? System.Text.Encoding.UTF8.GetBytes(response.ErrorMessage) : new byte[0]);

                        resp.ContentType = "application/json";
                        resp.ContentLength64 = bytes.Length;

                        resp.OutputStream.Write(bytes, 0, bytes.Length);

                    }
                    else if (response.type == DesktopResponseType.Text)
                    {
                        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(response.ErrorMessage);

                        resp.ContentType = "text/plain";
                        resp.ContentLength64 = bytes.Length;

                        resp.OutputStream.Write(bytes, 0, bytes.Length);
                    }

                    response.Dispose();
                    resp.Close();
                }
            }
            else if (response.type == DesktopResponseType.W3CJSON || response.type == DesktopResponseType.W3CJSONRESPONSE)
            {
                try
                {
                    jsonResponse.SetHttpListenerContext(context);
                    if(response.type == DesktopResponseType.W3CJSON)
                        jsonResponse.SetValue(response);
                    else
                        if( !jsonResponse.isValueDefine() ) jsonResponse.SetValue(jsonValueObject);
                    //= new JsonResponse(context) { value = response };
                    //serialisation des objects.
                    jsonResponse.Send();

                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("SEVERE !\n" + ex.StackTrace);
                }
                return true;
            }
            else if (response.atsvFilePath != null)
            {
                string fileName = response.atsvFilePath;

                FileInfo fi = new FileInfo(fileName);
                if (fi.Length > 0)
                {
                    FileStream input = WaitForFile(fileName);

                    if (input != null)
                    {
                        UploadThread t = new UploadThread(context, input, fileName);

                        Thread thr = new Thread(new ThreadStart(t.exec));
                        thr.Start();

                        return true;
                    }
                }

                using (HttpListenerResponse resp = context.Response)
                {
                    resp.StatusCode = (int)HttpStatusCode.NoContent;
                    resp.StatusDescription = "Unable to get Filestream available for upload -> " + fileName;
                }
            }
            else
            {
                throw new Exception("No content found");
            }
        }
        catch (Exception ex)
        {
            AtsLogger.WriteError("SEVERE !\n" + ex.StackTrace);
        }

        return true;
    }

    public class UploadThread
    {
        private HttpListenerContext context;
        private FileStream input;
        private string fileName;

        public UploadThread(HttpListenerContext context, FileStream input, string fileName)
        {
            this.context = context;
            this.input = input;
            this.fileName = fileName;
        }

        public void exec()
        {
            using (var response = context.Response)
            {
                try
                {
                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                    response.ContentLength64 = input.Length;
                    response.AddHeader("Date", DateTime.Now.ToString("r"));
                    response.AddHeader("Last-Modified", File.GetLastWriteTime(fileName).ToString("r"));

                    byte[] buffer = new byte[1024 * 64];
                    int nbytes;
                    while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        response.OutputStream.Write(buffer, 0, nbytes);

                    input.Close();

                    response.OutputStream.Flush();
                    response.Close();

                }
                catch (Exception e)
                {
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    response.StatusDescription = e.Message + "\n" + e.StackTrace;
                }
            }
        }
    }

    private FileStream WaitForFile(string fullPath)
    {
        for (int numTries = 0; numTries < 10; numTries++)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(fullPath, FileMode.Open);
                return fs;
            }
            catch (IOException)
            {
                if (fs != null)
                {
                    fs.Dispose();
                }
                Thread.Sleep(50);
            }
        }

        return null;
    }
}