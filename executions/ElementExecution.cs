﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.Core.Input;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using windowsdriver;
//using windowsdriver.executions.DataConverter;
using windowsdriver.items;
using windowsdriver.utils.JsonUtils;
using windowsdriver.Utils.Logger;

class ElementExecution : AtsExecution
{
    private enum ElementType
    {
        Childs = 0,
        Parents = 1,
        Find = 2,
        Attributes = 3,
        Select = 4,
        FromPoint = 5,
        Script = 6,
        Root = 7,
        LoadTree = 8,
        ListItems = 9,
        DialogBox = 10,
        Focus = 11,
        ContextMenu = 12
    };

    readonly Dictionary<string, int> elementTypeDict = new Dictionary<string, int>()
    {
        {"Childs", 0},
        {"Parents", 1},
        {"Find", 2},
        {"Attributes", 3},
        {"Select", 4},
        {"FromPoint", 5},
        {"Script", 6},
        {"Root", 7},
        {"LoadTree", 8},
        {"ListItems", 9},
        {"DialogBox", 10},
        {"Focus", 11},
        {"ContextMenu", 12}
    };

    private ElementType? getEnumElementType(string type)
    {
        type = type.ToLower();
        ElementType elementType;

        foreach (var pair  in elementTypeDict)
        {
            if(string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
            {
                elementType = (ElementType) pair.Value;
                return elementType;
            }
        }
        return null;
    }

    private readonly Executor executor;

    #region Constructor
    public ElementExecution(string type, JObject node, DesktopManager desktop) : base()
    {
        response.type = DesktopResponseType.W3CJSON;
        executor = ElementExec(type, node, desktop);
    }

    public ElementExecution(int stype, string[] commandsData, DesktopManager desktop) : base()
    {
        executor = ElementExec(stype, commandsData, desktop);
    }

    public ElementExecution(string stype, string postData, DesktopManager desktop) : base()
    {
        response.SetError(500, "Not implemented");
        return;
    }
    #endregion

    #region ElementExec

    private Executor ElementExec(string type, JObject node, DesktopManager desktop)
    {
        //string elemType = type.ToLower();
        ElementType? elemType = getEnumElementType(type);
        if (elemType == null) { response.SetError(500, "Not implemented"); }

        else if (elemType == ElementType.Find)
        {
            
            string tag = JsonUtils.GetJsonValue<string>(node, "tag", null);


            if (tag != null)
            {
                AtsLogger.WriteAll("element-find -> {0}", tag);
                int handle = JsonUtils.GetJsonValue<int>(node, "handle", -1);
                string[] attributes = JsonUtils.GetJsonArray<string>(node, "attributes", new string[0]);
                return new FindExecutor(response, desktop, handle, tag, attributes);
                //return new FindExecutor(response, desktop, handle, tag, new List<string>(commandsData).GetRange(2, commandsData.Length - 2).ToArray());
            }
        }
        else if (elemType == ElementType.ContextMenu)
        {
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", -1);
            return new ContextMenuExecutor(response, desktop, pid);
        }
        else if (elemType == ElementType.LoadTree)
        {
            int handle = JsonUtils.GetJsonValue<int>(node, "handle", 0);
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", 0);
            return new LoadTreeExecutor(response, desktop, handle, pid);
        }
        else if (elemType == ElementType.FromPoint)
        {
            return new FromPointExecutor(response, desktop);
        }
        else if (elemType == ElementType.DialogBox)
        {
            int pId = JsonUtils.GetJsonValue<int>(node, "pId", -1);
            return new ModalDialogExecutor(response, desktop, pId);
        }
        else
        {
            string id = JsonUtils.GetJsonValue<string>(node, "id", null);
            AtsElement element = CachedElements.Instance.GetElementById(id);
            if (element == null)
            {
                response.SetError(-73, "cached element not found");
            }
            else
            {
                if (elemType == ElementType.Parents)
                {
                    return new ElementExecutor(response, element, desktop);
                }
                else if (elemType == ElementType.Root)
                {
                    return new ChildsExecutor(response, element, "*", new string[0], desktop);
                }
                else if (elemType == ElementType.Attributes)
                {
                    string attribute = JsonUtils.GetJsonValue<string>(node, "attribute", null);
                    if (attribute != null)
                    {
                        return new AttributesExecutor(response, element, attribute, desktop);
                    }
                    else
                    {
                        return new AttributesExecutor(response, element, null, desktop);
                    }
                }
                else if (elemType == ElementType.Script)
                {
                    string script = JsonUtils.GetJsonValue<string>(node, "script", null);
                    if (script != null)
                    {
                        return new ScriptExecutor(response, element, script, desktop);
                    }
                }
                else if (elemType == ElementType.ListItems)
                {
                    return new ListItemsExecutor(response, element, desktop);
                }
                else if (elemType == ElementType.Focus )
                {
                    return new FocusExecutor(response, element, desktop);
                }
                else if (elemType == ElementType.Childs )
                {

                    if (id != null)
                    {
                        string tag = JsonUtils.GetJsonValue<string>(node, "tag", null);
                        string[] attributes = JsonUtils.GetJsonArray<string>(node, "attributes", null);
                        return new ChildsExecutor(response, element, tag, attributes, desktop);
                    }
                    
                }
                else if (elemType == ElementType.Select)
                {
                    string selectType = JsonUtils.GetJsonValue<string>(node, "type", null);
                    string selectValue = JsonUtils.GetJsonValue<string>(node, "value", null);
                    string selectRegExp = JsonUtils.GetJsonValue<string>(node, "regexp", null);
                    if(selectRegExp != null) return new SelectExecutor(response, element, selectType, selectValue, selectRegExp, desktop);
                    else return new SelectExecutor(response, element, selectType, selectValue, desktop);
                }
            }
        }

        return new EmptyExecutor(response);
    }



    private Executor ElementExec(int type, string[] commandsData, DesktopManager desktop)
    {
        ElementType elemType = (ElementType)type;

        if (elemType == ElementType.Find)
        {
            if (commandsData.Length > 1)
            {
                AtsLogger.WriteAll("element-find -> {0}", commandsData[1]);

                _ = int.TryParse(commandsData[0], out int handle);
                return new FindExecutor(response, desktop, handle, commandsData[1], new List<string>(commandsData).GetRange(2, commandsData.Length - 2).ToArray());
            }
        }
        else if (elemType == ElementType.ContextMenu)
        {
            _ = int.TryParse(commandsData[0], out int pid);
            return new ContextMenuExecutor(response, desktop, pid);
        }
        else if (elemType == ElementType.LoadTree)
        {
            int handle = 0;
            if (commandsData.Count() > 0)
                _ = int.TryParse(commandsData[0], out handle);

            int pid = 0;
            if (commandsData.Count() > 1)
                _ = int.TryParse(commandsData[1], out pid);

            return new LoadTreeExecutor(response, desktop, handle, pid);
        }
        else if (elemType == ElementType.FromPoint)
        {
            return new FromPointExecutor(response, desktop);
        }
        else if (elemType == ElementType.DialogBox)
        {
            return new ModalDialogExecutor(response, desktop, commandsData);
        }
        else
        {
            AtsElement element = CachedElements.Instance.GetElementById(commandsData[0]);
            if (element == null)
            {
                response.SetError(-73, "cached element not found");
            }
            else
            {
                if (elemType == ElementType.Parents)
                {
                    return new ElementExecutor(response, element, desktop);
                }
                else if (elemType == ElementType.Root)
                {
                    return new ChildsExecutor(response, element, "*", new string[0], desktop);
                }
                else if (elemType == ElementType.Attributes)
                {
                    if (commandsData.Length > 1)
                    {
                        return new AttributesExecutor(response, element, commandsData[1], desktop);
                    }
                    else
                    {
                        return new AttributesExecutor(response, element, null, desktop);
                    }
                }
                else if (elemType == ElementType.Script)
                {
                    if (commandsData.Length > 1)
                    {
                        return new ScriptExecutor(response, element, commandsData[1], desktop);
                    }
                }
                else if (elemType == ElementType.ListItems)
                {
                    return new ListItemsExecutor(response, element, desktop);
                }
                else if (elemType == ElementType.Focus)
                {
                    return new FocusExecutor(response, element, desktop);
                }
                else if (commandsData.Length > 1)
                {
                    if (elemType == ElementType.Childs)
                    {
                        return new ChildsExecutor(response, element, commandsData[1], new List<string>(commandsData).GetRange(2, commandsData.Length - 2).ToArray(), desktop);
                    }

                    if (elemType == ElementType.Select && commandsData.Length > 2)
                    {
                        if (commandsData.Length > 3)
                        {
                            return new SelectExecutor(response, element, commandsData[1], commandsData[2], commandsData[3], desktop);
                        }
                        else
                        {
                            return new SelectExecutor(response, element, commandsData[1], commandsData[2], desktop);
                        }
                    }
                }
            }
        }
        return new EmptyExecutor(response);
    }
    #endregion

    public override bool Run(HttpListenerContext context)
    {
        executor.Run();
        return base.Run(context);
    }

    private abstract class Executor
    {
        protected readonly DesktopResponse response;
        public Executor(DesktopResponse response)
        {
            this.response = response;
        }
        public abstract void Run();

        public AtsElement[] GetAtsElementsArray(HashSet<AtsElement> elements)
        {
            AtsElement[] result = new AtsElement[elements.Count];
            elements.CopyTo(result);

            return result;
        }
    }

    private class EmptyExecutor : Executor
    {
        public EmptyExecutor(DesktopResponse response) : base(response) { }
        public override void Run() { }
    }

    private class DesktopExecutor : Executor
    {
        readonly DesktopManager desktop;
        readonly string tag;
        readonly string[] criterias;

        public DesktopExecutor(DesktopResponse response, DesktopManager desktop) : base(response)
        {
            this.desktop = desktop;
        }

        public DesktopExecutor(DesktopResponse response, DesktopManager desktop, string tag, string[] criterias) : base(response)
        {
            this.desktop = desktop;
            this.tag = tag;
            this.criterias = criterias;
        }

        public override void Run()
        {
            if (string.IsNullOrEmpty(tag))
            {
                response.Elements = new AtsElement[1] { desktop.DesktopElement };
            }
            else
            {
                response.Elements = GetAtsElementsArray(desktop.GetElements(tag, criterias)); ;
            }
        }
    }

    private class LoadTreeExecutor : Executor
    {
        private readonly int handle;
        private readonly int pid;
        private readonly DesktopManager desktop;

        public LoadTreeExecutor(DesktopResponse response, DesktopManager desktop, int handle, int pid) : base(response)
        {
            this.handle = handle;
            this.desktop = desktop;
            this.pid = pid;
        }

        public override void Run()
        {
            //AtsLogger.Instance.writeLog("find window with handle -> " + handle);
            DesktopWindow window = desktop.GetWindowByHandle(handle);
            if (window == null)
            {
                window = desktop.GetWindowIndexByPid(this.pid, 0);
            }

            if (window != null)
            {
                window.Focus();
                Task<AtsElement[]> task = Task.Run(() =>
                {
                    return window.GetElementsTree(desktop);
                });

                task.Wait(TimeSpan.FromSeconds(40));
                response.Elements = task.Result;
            }
            else
            {
                response.Elements = new AtsElement[0];
            }
        }
    }

    private class FindExecutor : Executor
    {
        private readonly int handle;
        private readonly string tag;
        private readonly string[] attributes;
        private readonly DesktopManager desktop;

        public FindExecutor(DesktopResponse response, DesktopManager desktop, int handle, string tag, string[] attributes) : base(response)
        {
            this.handle = handle;
            this.tag = tag;
            this.attributes = attributes;
            this.desktop = desktop;
        }

        public override void Run()
        {
            DesktopWindow window = desktop.GetWindowByHandle(handle);
            if (window != null)
            {
                window.Focus();
                response.Elements = GetAtsElementsArray(window.GetElements(false, tag, attributes, null, desktop));
            }
            else
            {
                response.Elements = new AtsElement[0];
            }
        }
    }

    private class FromPointExecutor : Executor
    {
        private readonly DesktopManager desktop;

        public FromPointExecutor(DesktopResponse response, DesktopManager desk) : base(response)
        {
            desktop = desk;
        }

        public override void Run()
        {
            AtsElement elem = desktop.GetElementFromPoint(Mouse.Position);
            if (elem != null)
            {
                response.Elements = new AtsElement[1] { elem };
            }
            else
            {
                response.Elements = new AtsElement[0];
            }
        }
    }

    private class ContextMenuExecutor : Executor
    {
        protected DesktopManager desktop;
        private readonly int processId;

        public ContextMenuExecutor(DesktopResponse response, DesktopManager desktop, int pid) : base(response)
        {
            this.desktop = desktop;
            this.processId = pid;
        }

        public override void Run()
        {
            AutomationElement[] elements = desktop.GetContextMenu();

            Stack<AtsElement> stack = new Stack<AtsElement>();

            foreach (AutomationElement element in elements)
            {
                int idxMenuItem = 0;
                string baseId = string.Empty;

                foreach (AutomationElement item in element.FindAllChildren())
                {
                    AtsElement menuItem = new AtsElement(item, idxMenuItem);
                    stack.Push(menuItem);
                    idxMenuItem++;
                }
            }
            response.Elements = stack.ToArray();
            desktop = null;
        }
    }

    private class ModalDialogExecutor : Executor
    {
        protected DesktopManager desktop;
        private readonly int pId;

        public ModalDialogExecutor(DesktopResponse response, DesktopManager desktop, int pId) : base(response)
        {
            this.desktop = desktop;
//            this.pId = pId; //n etait pas present avant Json.
        }

        public ModalDialogExecutor(DesktopResponse response, DesktopManager desktop, string[] commandsData) : base(response)
        {
            int.TryParse((commandsData.Length > 0 ? commandsData[0] : "-1"), out this.pId);
            this.desktop = desktop;
        }

        public override void Run()
        {
            AtsElement[] elems = new AtsElement[0];

            AutomationElement pane = desktop.GetFirstModalPane();
            if (pane != null)
            {
                PropertyCondition winCondition = pane.ConditionFactory.ByControlType(FlaUI.Core.Definitions.ControlType.Window);
                AutomationElement dialog = pane.FindFirstChild(winCondition);
                int maxTry = 20;
                while (dialog == null && maxTry > 0)
                {
                    dialog = pane.FindFirstChild(winCondition);
                    Thread.Sleep(200);
                    maxTry--;
                }

                if (dialog != null)
                {
                    elems = new AtsElement[] { new AtsElement(desktop, dialog, null) };
                    dialog.Focus();
                }
            } 
            else
            {
                elems = desktop.GetMsgBoxs(pId);
            }

            response.Elements = elems;
            Dispose();
        }

        public void Dispose()
        {
            desktop = null;
        }
    }

    private class ElementExecutor : Executor
    {
        protected AtsElement element;
        protected DesktopManager desktop;

        public ElementExecutor(DesktopResponse response, AtsElement element, DesktopManager desktop) : base(response)
        {
            this.element = element;
            this.desktop = desktop;
        }

        public override void Run()
        {
            response.Elements = element.GetParents();
            Dispose();
        }

        public void Dispose()
        {
            element = null;
            desktop = null;
        }
    }

    private class ChildsExecutor : ElementExecutor
    {
        private readonly string tag;
        private readonly string[] attributes;

        public ChildsExecutor(DesktopResponse response, AtsElement element, string tag, string[] attributes, DesktopManager desktop) : base(response, element, desktop)
        {
            this.tag = tag;
            this.attributes = attributes;
        }

        public override void Run()
        {
            try
            {
                response.Elements =
                    GetAtsElementsArray(
                        element.GetElements(false, tag, attributes, element.Element, desktop));
            }
            catch
            {
                response.Elements = new AtsElement[0];
            }
        }
    }

    private class ListItemsExecutor : ElementExecutor
    {
        private AtsElement[] items;

        public ListItemsExecutor(DesktopResponse response, AtsElement element, DesktopManager desktop) : base(response, element, desktop)
        {
            this.items = element.GetListItems(desktop);
        }

        public override void Run()
        {
            if (items.Length == 0)
            {
                element.TryExpand();
                items = element.GetListItems(desktop);
            }

            foreach (AtsElement it in items)
            {
                it.LoadListItemAttributes();
            }
            response.Elements = items;
            Keyboard.Type('\t');
        }
    }

    private class AttributesExecutor : ElementExecutor
    {
        private readonly string propertyName;

        public AttributesExecutor(DesktopResponse response, AtsElement element, string propertyName, DesktopManager desktop) : base(response, element, desktop)
        {
            this.propertyName = propertyName;
        }

        public override void Run()
        {
            element.LoadProperties();
            response.Attributes = element.GetProperty(propertyName);

            Dispose();
        }
    }

    private class ScriptExecutor : ElementExecutor
    {
        private readonly string script;

        private void Execute()
        {
            element.ExecuteScript(script);
        }

        public ScriptExecutor(DesktopResponse response, AtsElement element, string script, DesktopManager desktop) : base(response, element, desktop)
        {
            this.script = script;
        }

        public override void Run()
        {
            Thread t = new Thread(Execute);
            t.Start();

            if (!t.Join(new TimeSpan(0, 0, 5)))
            {
                t.Abort();
            }

            //response.Data = new DesktopData[0];
            Dispose();
        }
    }

    private class FocusExecutor : ElementExecutor
    {
        public FocusExecutor(DesktopResponse response, AtsElement element, DesktopManager desktop) : base(response, element, desktop)
        {
        }

        public override void Run()
        {
            try
            {
                element.Focus();
            }
            catch { }

            element.LoadProperties();

            response.Attributes = element.Attributes;
            Dispose();
        }
    }

    private class SelectExecutor : ElementExecutor
    {
        private readonly bool regexp = false;
        private readonly string type;
        private readonly string value;

        public SelectExecutor(DesktopResponse response, AtsElement element, string type, string value, DesktopManager desktop) : base(response, element, desktop)
        {
            this.type = type;
            this.value = value;
        }

        public SelectExecutor(DesktopResponse response, AtsElement element, string type, string value, string regexp, DesktopManager desktop) : base(response, element, desktop)
        {
            this.type = type;
            this.value = value;
            _ = bool.TryParse(regexp, out this.regexp);
        }

        public override void Run()
        {
            string error = null;
            if ("index".Equals(type))
            {
                _ = int.TryParse(value, out int index);
                error = element.SelectItem(index, desktop);
            }
            else
            {
                bool byValue = "value".Equals(type);
                if (regexp)
                {
                    Regex rx = new Regex(@value);
                    if (byValue)
                    {
                        error = element.SelectItem((AutomationElement e) => { return e.Patterns.Value.IsSupported && rx.IsMatch(e.Patterns.Value.ToString()); }, desktop);
                    }
                    else
                    {
                        error = element.SelectItem((AutomationElement e) => { return rx.IsMatch(e.Name); }, desktop);
                    }
                }
                else
                {
                    if (byValue)
                    {
                        error = element.SelectItem((AutomationElement e) => { return e.Patterns.Value.IsSupported && e.Patterns.Value.ToString() == value; }, desktop);
                    }
                    else
                    {
                        error = element.SelectItem((AutomationElement e) => { return e.Name == value; }, desktop);
                    }
                }
            }

            if (error != null)
            {
                element.SafeClick();
                response.ErrorCode = -199;
            }

            response.ErrorMessage = error;
        }
    }
}