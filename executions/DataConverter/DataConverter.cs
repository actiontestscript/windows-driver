﻿using Newtonsoft.Json;
using System.Linq;
using System.Net;

namespace executions.DataConverter
//namespace windowsdriver.executions.DataConverter
{
    /*
    public class DriverInfo
    {
        public DriverInfo()
        {
            sessionId = "";
            screenshotUrl = "";
            headless = false;
        }

        public string sessionId { get; set; }
        public string screenshotUrl { get; set; }
        public bool headless { get; set; }
    }

    internal interface IDto { }

    internal abstract class DataConverter
    {
        internal delegate U DlArrayToDto<U>(string[] data) where U : IDto, new();

        protected string[] dataTab = null;
        protected string dataStr = null;

        protected DataConverter(string[] data)
        {
            dataTab = data;
        }

        protected DataConverter(string data)
        {
            dataStr = data;
        }

        #region GetData
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal (T, string, int) GetData<T>(DlArrayToDto<T> FuncToDto) where T : IDto, new()
        {
            T dto = default(T);
            string responseMsg = string.Empty;
            int responseCode = (int)HttpStatusCode.OK;

            if (dataStr != null)
            {
                //var obj = new 
                //{
                //value = new T(),
                //driverInfo = new DriverInfo()
                
//                {
//                    sessionId = "",
//                    screenshotUrl = "",
//                    headless = false
//                }
                
                //};
                try
                {
                    var obj = new
                    {
                        value = new T(),
                    };

                    var deserializedData = JsonConvert.DeserializeAnonymousType(dataStr, obj);
                    dto = deserializedData.value;

                    responseMsg = SerialiseSuccess();
                }
                catch (JsonException ex)
                {
                    responseCode = 400;
                    responseMsg = SerialiseError(ex.Message, responseCode);
                }
                
//                                {
//                                    var deserializedData = JsonConvert.DeserializeAnonymousType(dataStr, obj);
//                                    dto = JsonConvert.DeserializeAnonymousType(dataStr, obj).value;
//                                    dto.ApplyDriverInfo(deserializedData.driverInfo);
//
//                                    responseMsg = SerialiseSuccess();
//                                }

//                catch (JsonException ex)
//                {
//                    responseCode = 400;
//                    responseMsg = SerialiseError(ex.Message, responseCode);
//                }
                
            }
            else
            {
                dto = FuncToDto(dataTab);
            }

            return (dto, responseMsg, responseCode);
        }
        #endregion

        #region SerialiseSuccess
        internal string SerialiseSuccess()
        {
            return JsonConvert.SerializeObject(
                        new
                        {
                            value = "null"
                        }
                    );
        }
        #endregion

        #region SerialiseError
        internal string SerialiseError(string errorMessage, int responseCode)
        {
            return JsonConvert.SerializeObject(
                new
                {
                    value = new
                    {
                        error = new
                        {
                            error = responseCode,
                            message = "invalid argument",
                            stacktrace = errorMessage,
                        }
                    }
                }
            );
        }
        #endregion

        #region HasData
        internal bool HasData()
        {
            return (dataStr != null && dataStr != string.Empty) || (dataTab != null && dataTab.Count() > 0);
        }
        #endregion
    }
*/
}
