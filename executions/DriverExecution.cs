﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Text.RegularExpressions;
using System.Threading;
using windowsdriver;
using windowsdriver.utils;
using windowsdriver.Utils.Logger;

class DriverExecution : AtsExecution
{
    private const int errorCode = -4;
    private const string UWP_PROTOCOLE = "uwp";
    private const string PROC_PROTOCOLE = "proc";
    private const string PROCESS_PROTOCOLE = "process";
    private const string HANDLE_PROTOCOLE = "handle";
    private const string WINDOW_PROTOCOLE = "window";

    private enum DriverType
    {
        Capabilities = 0,
        RemoteDriver = 1,
        Application = 2,
        CloseWindows = 3,
        Close = 4,
        Ostracon = 5,
        Shapes = 6,
        Record = 7
    };

    Dictionary<string, int> driverTypeDict = new Dictionary<string, int>()
    {
        {"capabilities", 0},
        {"remotedriver", 1},
        {"application", 2},
        {"closewindows", 3},
        {"close", 4},
        {"ostracon", 5},
        {"shapes", 6},
//        {"record", 7},
    };

    private void WaitWindowReady(Application app)
    {
        int maxTry = 20;
        while (maxTry > 0)
        {
            try
            {
                app.WaitWhileBusy(TimeSpan.FromSeconds(5));
                return;
            }
            catch (InvalidOperationException)
            {
                System.Threading.Thread.Sleep(200);
                maxTry--;
            }
        }
    }

    #region Constructors
    public DriverExecution(string stype, string postData, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop) : base()
    {
        string[] commandsData = new List<string>().ToArray();

        if (driverTypeDict.TryGetValue(stype.ToLower(), out int sTypeVal))
        {
            DriverExec(sTypeVal, commandsData, capabilities, recorder, desktop);
        }
        else
        {
            response.SetError(errorCode, "unknown driver command");
        }
    }

    //TODO: to remove when w3c notation will be used 
    public DriverExecution(int stype, string[] commandsData, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop) : base()
    {
        DriverExec(stype, commandsData, capabilities, recorder, desktop);
    }
    #endregion

    #region DriverExecutionExec
    public void DriverExec(int t, string[] commandsData, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop)
    {
        DriverType type = (DriverType)t;

        if (type == DriverType.Capabilities)
        {
            response.Attributes = capabilities.GetAttributes();
        }
        else if (type == DriverType.RemoteDriver)
        {
            response.Attributes = capabilities.GetRemoteDriverUrl(commandsData[0], null);
        }
        else if (type == DriverType.Application)
        {
            ActionApplication(commandsData, desktop);
        }
        else if (type == DriverType.CloseWindows)
        {
            ActionCloseWindows(commandsData, desktop);
        }
        else if (type == DriverType.Close)
        {
            response.type = DesktopResponseType.StopServer;
        }
        else if (type == DriverType.Ostracon)
        {
            _ = int.TryParse(commandsData[0], out int handle);
            desktop.HandleRemove(handle, true);
        }
        else if (type == DriverType.Shapes)
        {
            ShapesRecognition recog = new ShapesRecognition(commandsData, desktop, recorder.IsActivated());
            if (recog.GetErrorCode() == 0)
            {
                response.Attributes = recog.LaunchProc(recorder);
            }
            else
            {
                response.SetError(recog.GetErrorCode(), recog.GetErrorMessage());
            }
        }
        /*
        else if (type == DriverType.Record)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                ShapesRecognition recog = new ShapesRecognition(commandsData, desktop, recorder.IsActivated());
                recog.LaunchProc(recorder);
            }).Start();

            response.Attributes = new DesktopData[] { new DesktopData("record", "started") };
        }
        */
        else
        {
            response.SetError(errorCode, "unknown driver command");
        }
    }
    #endregion

    #region ActionApplication
    private void ActionApplication(string[] commandsData, DesktopManager desktop)
    {
        if (commandsData.Length > 0)
        {
            _ = bool.TryParse(commandsData[0], out bool attach);
            string appName = commandsData[1];

            int protocoleSplitIndex = appName.IndexOf("://");
            if (protocoleSplitIndex > 0)
            {
                string applicationProtocol = appName.Substring(0, protocoleSplitIndex).ToLower();
                string applicationData = appName.Substring(protocoleSplitIndex + 3);

                if (applicationProtocol.Equals(UWP_PROTOCOLE))
                {
                    string[] uwpUrlData = applicationData.Split('/');
                    if (uwpUrlData.Length > 1)
                    {
                        string packageName = uwpUrlData[0];
                        string windowName = uwpUrlData[1];

                        if (windowName.Length > 0)
                        {
                            string appId = "App";
                            int exclamPos = packageName.IndexOf("!");
                            if (exclamPos > -1)
                            {
                                appId = packageName.Substring(exclamPos + 1);
                                packageName = packageName.Substring(0, exclamPos);
                            }

                            if (!packageName.Contains("_"))
                            {
                                packageName = UwpApplications.getApplicationId(packageName);
                            }

                            if (packageName != null)
                            {
                                try
                                {
                                    Application app = Application.LaunchStoreApp(packageName + "!" + appId);
                                    app.WaitWhileBusy(TimeSpan.FromSeconds(7));
                                    app.WaitWhileMainHandleIsMissing(TimeSpan.FromSeconds(7));

                                    Process uwpProcess = Process.GetProcessById(app.ProcessId);

                                    if (uwpProcess != null)
                                    {
                                        int maxTry = 5;
                                        DesktopWindow window = null;
                                        while (window == null && maxTry > 0)
                                        {
                                            System.Threading.Thread.Sleep(100);
                                            window = desktop.GetWindowByTitle(windowName);
                                            maxTry--;
                                        }

                                        if (window != null)
                                        {
                                            window.UpdateApplicationData(uwpProcess);
                                            response.SetWindow( window );
                                           }
                                        else
                                        {
                                            response.SetError(errorCode, "window with name '" + windowName + "' not found");
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    response.SetError(errorCode, "cannot start UWP application : " + e.Message);
                                }
                            }
                            else
                            {
                                response.SetError(errorCode, "malformed uwp url (package name not found) : " + applicationData);
                            }
                        }
                        else
                        {
                            response.SetError(errorCode, "malformed uwp url (missing window name) : " + applicationData);
                        }
                    }
                    else
                    {
                        response.SetError(errorCode, "malformed uwp url (sould be 'uwp://[UwpApplicationName]/[window name])' : " + applicationData);
                    }
                }
                else if (applicationProtocol.Equals(HANDLE_PROTOCOLE))
                {
                    try
                    {
                        response.SetWindow(desktop.GetWindowByHandle(applicationData) );
                    }
                    catch (Exception)
                    {
                        response.SetError(errorCode, "unable to find window with handle : " + applicationData);
                    }
                }
                else if (applicationProtocol.Equals(WINDOW_PROTOCOLE))
                {
                    try
                    {
                        response.SetWindow(desktop.GetWindowByName(applicationData));
                    }
                    catch (Exception)
                    {
                        response.SetError(errorCode, "unable to find window with title like : " + applicationData);
                    }
                }
                else if (applicationProtocol.Equals(PROCESS_PROTOCOLE) || applicationProtocol.Equals(PROC_PROTOCOLE))
                {
                    Process appProcess = null;
                    int maxTry = 40;
                    while (appProcess == null && maxTry > 0)
                    {
                        System.Threading.Thread.Sleep(500);
                        appProcess = GetProcessByInfo(applicationData);
                        maxTry--;
                    }

                    if (appProcess != null)
                    {
                        try
                        {
                            response.SetWindow(desktop.GetWindowByProcess(appProcess) );
                        }
                        catch (Exception)
                        {
                            response.SetError(errorCode, "unable to find window with process : " + appProcess.ProcessName + " (" + appProcess.Id + ")");
                        }
                    }
                    else
                    {
                        response.SetError(errorCode, "unable to find process matching : " + applicationData);
                    }
                }
                else
                {
                    applicationData = Uri.UnescapeDataString(Regex.Replace(applicationData, @"^/", ""));
                    bool batScript = applicationData.ToLower().EndsWith(".bat") || applicationData.ToLower().EndsWith(".cmd");

                    Process proc = null;

                    if (attach)
                    {
                        proc = GetProcessByFilename(applicationData);
                    }

                    if (proc == null)
                    {
                        if (File.Exists(applicationData))
                        {
                            string commandArgs = "";
                            if (commandsData.Length > 2)
                            {
                                int newLen = commandsData.Length - 2;
                                string[] args = new string[newLen];
                                Array.Copy(commandsData, 2, args, 0, newLen);
                                commandArgs = String.Join(" ", args);
                            }

                            ProcessStartInfo startInfo = null;

                            if (batScript)
                            {
                                startInfo = new ProcessStartInfo("cmd.exe");
                                startInfo.Arguments = "/k " + applicationData;
                                if (commandArgs.Length > 0)
                                {
                                    startInfo.Arguments += " " + commandArgs;
                                }
                            }
                            else
                            {
                                startInfo = new ProcessStartInfo();

                                if (applicationData.ToLower().EndsWith(".ps1"))
                                {
                                    startInfo.UseShellExecute = false;
                                    startInfo.FileName = @"C:\windows\system32\windowspowershell\v1.0\powershell.exe";
                                    startInfo.Arguments = string.Format("-File \"{0}\"", applicationData);
                                }
                                else
                                {
                                    startInfo.FileName = applicationData;
                                    startInfo.Arguments = commandArgs;
                                }
                            }

                            startInfo.WorkingDirectory = Directory.GetParent(applicationData).FullName;

                            try
                            {
                                Application app = Application.Launch(startInfo);
                                WaitWindowReady(app);

                                if (app.HasExited)
                                {
                                    response.SetError(errorCode, "the process has exited, you may try another way to start this application (UWP ?)");
                                }
                                else
                                {
                                    proc = Process.GetProcessById(app.ProcessId);
                                }
                            }
                            catch (Exception e)
                            {
                                response.SetError(errorCode, "cannot start application : " + e.ToString() + " - " + e.Message);
                            }
                        }
                        else
                        {
                            response.SetError(errorCode, "exec file not found : " + applicationData);
                            return;
                        }
                    }

                    if (proc != null)
                    {
                        DesktopWindow window = null;

                        if (batScript)
                        {
                            window = desktop.GetConsoleWindow(proc);
                        }
                        else
                        {
                            window = desktop.GetAppMainWindow(proc);
                        }

                        if (window != null)
                        {
                            window.UpdateApplicationData(proc);
                            response.SetWindow( window );
                        }
                        else
                        {
                            response.SetError(errorCode, "unable to find window for application : " + applicationData);
                        }
                    }
                }
            }
            else
            {
                response.SetError(errorCode, "malformed application url [" + appName + "]");
            }
        }
        else
        {
            response.SetError(errorCode, "no application path data");
        }
    }
    #endregion

    #region ActionCloseWindows
    private void ActionCloseWindows(string[] commandsData, DesktopManager desktop)
    {
        int.TryParse(commandsData[0], out int pid);
        int.TryParse(commandsData[1], out int handle);

        if (handle > 0)
        {
            DesktopWindow winapp = desktop.GetWindowByHandle(handle);
            if (winapp != null)
            {
                winapp.Close();
                Thread.Sleep(500);
            }
        }

        if (pid > 0)
        {
            Process proc = Process.GetProcessById(pid);
            if (proc != null && proc.MainModule != null && proc.MainModule.ModuleName == "WindowsTerminal.exe")
            {
                AtsLogger.WriteAll("Windows terminal detected ...");
                return;
            }

            try
            {
                List<DesktopWindow> wins = desktop.GetOrderedWindowsByPid(pid);
                while (wins.Count > 0)
                {
                    wins[0].Close();
                    wins.RemoveAt(0);
                }
            }
            catch { }

            try
            {
                Process procx = Process.GetProcessById(pid);
                if (procx != null)
                {
                    int maxTry = 30;
                    while (maxTry > 0 && !procx.HasExited)
                    {
                        Thread.Sleep(100);
                        maxTry--;
                    }

                    procx = Process.GetProcessById(pid);
                    if (procx != null)
                    {
                        procx.Close();
                        Thread.Sleep(500);

                        procx = Process.GetProcessById(pid);
                        procx?.Kill();
                    }
                }
            }
            catch { }
        }
        else
        {
            response.SetError(errorCode, "pid must be greater than 0");
        }

    }
    #endregion

    private Process GetProcessByInfo(string info)
    {
        ManagementObjectCollection processes = new ManagementObjectSearcher("select ProcessID,Caption,ExecutablePath from Win32_Process").Get();
        foreach (ManagementObject o in processes)
        {
            object exec = o["ExecutablePath"];
            if (exec != null)
            {
                string procName = Regex.Replace(o["Caption"].ToString(), @".exe$", "");
                string executablePath = exec.ToString().ToLower();

                Regex infoRegex = new Regex(info);
                if (string.Equals(info, procName, StringComparison.OrdinalIgnoreCase) || infoRegex.IsMatch(executablePath))
                {
                    int.TryParse(o["ProcessID"].ToString(), out int procId);
                    return Process.GetProcessById(procId);
                }
            }
        }
        return null;
    }

    private static Process GetProcessByFilename(string fileName)
    {
        fileName = fileName.Replace("/", "\\");
        Process[] procs = Process.GetProcesses();
        foreach (Process p in procs)
        {
            try
            {
                if (fileName.Equals(p.MainModule.FileName))
                {
                    return p;
                }
            }
            catch { }
        }
        return null;
    }

    private void KillProcessAndChildren(int pid)
    {
        ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * From Win32_Process Where ParentProcessID=" + pid);
        ManagementObjectCollection moc = searcher.Get();
        foreach (ManagementObject mo in moc)
        {
            KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
        }
        try
        {
            Process proc = Process.GetProcessById(pid);
            proc.Kill();
        }
        catch { }

        searcher.Dispose();
    }
}