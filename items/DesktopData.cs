﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

[DataContract(Name = "com.ats.executor.drivers.desktop.DesktopData")]
public class DesktopData
{
    private string Name = "";
    private string _value = "";

    [JsonProperty(PropertyName = "data")]
    [DataMember(Name = "data", IsRequired = true)]
    public string Data
    {
        get
        {
            return new StringBuilder().Append(Name).Append("\n").Append(Value).ToString();
        }
        set { }
    }

    [JsonProperty(PropertyName = "value")]
    [DataMember(Name = "value", IsRequired = true)]
    public string Value
    {
        get
        {
            if (_value == null)
            {
                return "";
            }
            return _value.Replace("\r", "").Replace("\n", ""); ;
        }
        set {
            _value = value;
        }
    }

    public DesktopData(string n)
    {
        Name = n;
    }

    public DesktopData(string name, string v) : this(name)
    {
        Value = v;
    }

    public DesktopData(string name, bool b) : this(name)
    {
        Value = b.ToString();
    }

    public DesktopData(string name, int i) : this(name)
    {
        Value = i.ToString();
    }

    public void SetValue(string v)
    {
        Value = v;
    }

    public void SetValue(double d)
    {
        Value = d.ToString();
    }

    public bool Match(string n)
    {
        return Name != null && Name.Equals(n);
    }
}