﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.Core.Definitions;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using windowsdriver;
using windowsdriver.items;

[DataContract(Name = "com.ats.element.AtsElement")]
public class AtsElement
{
    private static readonly string UnknownTag = "Unknown";
    private static readonly string GridTag = "Grid";
    private static readonly string WindowTag = "Window";
    private DesktopData[] _attributes = null;
    private AtsElement[] _elements = null;

    [JsonProperty(PropertyName = "id")]
    [DataMember(Name = "id")]
    public string Id {get;set;}

    [JsonProperty(PropertyName = "tag")]
    [DataMember(Name = "tag")]
    public string Tag {get;set;} = "*";

    [JsonProperty(PropertyName = "clickable")]
    [DataMember(Name = "clickable")]
    public bool Clickable { get; set; } = true;

    [JsonProperty(PropertyName = "x")]
    [DataMember(Name = "x")]
    public double X { get; set; } = 0;

    [JsonProperty(PropertyName = "y")]
    [DataMember(Name = "y")]
    public double Y { get; set; } = 0;

    [JsonProperty(PropertyName = "width")]
    [DataMember(Name = "width")]
    public double Width { get; set; } = -1;

    [JsonProperty(PropertyName = "height")]
    [DataMember(Name = "height")]
    public double Height { get; set; } = -1;

    [JsonProperty(PropertyName = "visible")]
    [DataMember(Name = "visible")]
    public Boolean Visible { get; set; } = false;

    [JsonProperty(PropertyName = "password")]
    [DataMember(Name = "password")]
    public Boolean Password { get; set; } = false;

    [JsonProperty(PropertyName = "numChildren")]
    [DataMember(Name = "numChildren")]
    public int NumChildren { get; set; } = 0;

    [JsonProperty(PropertyName = "attributes", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "attributes")]
    public DesktopData[] Attributes
    {
        get
        {
            if(_attributes == null)
            {
             return null;
            }
            else return _attributes;
        }
        set
        {
            _attributes = value;
        }
    }
    
    [JsonProperty(PropertyName = "children", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "children")]
    public AtsElement[] Children
    {
        get
        {
            if (_elements == null)
            {
                return null;
            }
            else return _elements;
        }
        set
        {
            _elements = value;
        }
    }
    
    public AutomationElement Element;

    //List of objects for which we will generate a flat list rather than a hierarchical one
    private readonly List<string> lstFlatChildElements = new List<string>() { "Tree" };

    #region Constructors
    public AtsElement(DesktopManager desktop)
    {
        Id = "0";
        Width = desktop.DesktopWidth;
        Height = desktop.DesktopHeight;
        Visible = true;
        Attributes = new DesktopData[0];
        Children = new AtsElement[0];
    }

    public AtsElement(AutomationElement elem)
    {
        Element = elem;
        Attributes = new DesktopData[0];
        Id = Guid.NewGuid().ToString();
        Tag = GetTag(Element);
        if (CheckElementVisible())
        {
            Visible = true;
            Password = IsPassword(Element);
            CachedElements.Instance.Add(Id, this);
        }
    }

    public AtsElement(AutomationElement elem, int menuIdx) : this(elem)
    {
        if (Visible)
        {
            string text = TruncString(elem.Name, 52);
            List<DesktopData> lstAttr = new List<DesktopData>();

            Id = CreateMenuID(text, menuIdx);
            Tag = "Menu";

            lstAttr.Add(new DesktopData("Text", text));
            try
            {
                lstAttr.Add(new DesktopData("AutomationId", elem.AutomationId));
            }
            catch { }

            lstAttr.Add(new DesktopData("ElementId", Id));
            Attributes = lstAttr.ToArray();
        }
    }

    /// <summary>
    /// Create a new AtsElement and his children
    /// </summary>
    /// <param name="desktop"></param>
    /// <param name="elem">FLAUI element to inspect</param>
    /// <param name="lstChildren">if we pass this parameter we will generate a flat list for this element and his children</param>
    public AtsElement(DesktopManager desktop, AutomationElement elem, Stack<AtsElement> lstChildren) : this(elem)
    {
        bool createFlatList = lstChildren == null && lstFlatChildElements.Contains(Tag) ;

        if (Visible)
        {
            AutomationElement[] childs = FindAllChildren(elem);
            NumChildren = childs.Length;

            Stack<AtsElement> stackedChildren = (lstChildren != null ? lstChildren : new Stack<AtsElement>());
            if (NumChildren > 0)
            {
                if (createFlatList)
                {
                    lstChildren = new Stack<AtsElement>();
                }

                for (int i = 0; i < NumChildren; i++)
                {
                    AtsElement e = null;
                    e = new AtsElement(desktop, childs.ElementAt(i), lstChildren);

                    if (e.Visible)
                    {
                        if (lstChildren != null)
                        {
                            lstChildren.Push(e);
                        } 
                        else
                        {
                            stackedChildren.Push(e);
                        }
                    }
                }
                
                if (createFlatList)
                {
                    Children = new AtsElement[lstChildren.Count];
                    lstChildren.ToArray().CopyTo(Children, 0);
                    lstChildren = null;
                } 
                else
                {
                    if (lstChildren == null && stackedChildren != null)
                    {
                        Children = new AtsElement[stackedChildren.Count];
                        stackedChildren.ToArray().CopyTo(Children, 0);
                    }
                }

                Array.Clear(childs, 0, childs.Length);

                return;
            }
        }

        Children = new AtsElement[0];
    }
    #endregion

    private bool CheckElementVisible()
    {
        if (Element == null)
        {
            return false;
        }

            if (Element.Properties.IsOffscreen.IsSupported && Element.IsOffscreen)
            {
                return false;
            }


        if (Element.Patterns.LegacyIAccessible.IsSupported)
        {
            try
            {
                AccessibilityState state = Element.Patterns.LegacyIAccessible.Pattern.State.Value;
                if (state.HasFlag(AccessibilityState.STATE_SYSTEM_INVISIBLE))
                {
                    return false;
                }
            }
            catch { }
        }

        int[] elemRect = GetElementRectangle(Element);

        if (elemRect == null)// second chance to get bounding rectangle
        {
            Thread.Sleep(20);
            elemRect = GetElementRectangle(Element);
        }

        if (elemRect == null) // last chance to get bounding rectangle
        {
            Thread.Sleep(20);
            elemRect = GetElementRectangle(Element);
        }

        if (elemRect == null)
        {
            //try
            //{
                if (Element.FindFirstChild() != null)
                {
                    elemRect = GetElementRectangle(Element.Parent);
                }
            /*}
            catch (Exception e)
            {
            }*/
        }

        if (elemRect != null)
        {
            X = elemRect[0];
            Y = elemRect[1];
            Width = elemRect[2];
            Height = elemRect[3];
        }

        return Width > 0 && Height > 0;
    }

    private int[] GetElementRectangle(AutomationElement elem)
    {
        try
        {
            Rectangle rec = elem.Properties.BoundingRectangle;
            return new int[] { rec.X, rec.Y, rec.Width, rec.Height };
        }
        catch {}

        return null;
    }

    #region FindAllChildren
    /// <summary>
    /// Get the children list of the element Automation element
    /// </summary>
    /// <param name="elem">The parent element</param>
    /// <returns>List of children of elem</returns>
    public static AutomationElement[] FindAllChildren(AutomationElement elem)
    {
        AutomationElement[] list = null;
        int maxTry = 40;

        while (maxTry > 0)
        {
            try
            {
                list = elem.FindAllChildren();
                maxTry = 0;
            }
            catch
            {
                Thread.Sleep(50);
                list = new AutomationElement[0];
                maxTry--;
            }
        }

        return list;
    }
    #endregion

    private string CreateMenuID(string text, int idx)
    {
        return string.Join("_", "MENU@", text.ToUpper().Replace(' ', '_'), idx.ToString());
    }

    public AtsElement(AutomationElement elem, string[] attributes) : this(elem)
    {
        CalculateAttributes(attributes);
    }

    public void CalculateAttributes(string[] attributes)
    {
        Attributes = Properties.AddProperties(attributes, Password, Visible, Element);
    }

    public AtsElement[] GetListItems(DesktopManager desktop)
    {
        AutomationElement[] listItems = Element.FindAllDescendants(Element.ConditionFactory.ByControlType(ControlType.ListItem));

        int len = listItems.Length;
        if (len > 0)
        {
            AtsElement[] result = new AtsElement[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = new AtsElement(listItems[i]);
            }
            return result;
        }
        return desktop.GetPopupListAtsElement();
    }

    public AutomationElement[] GetListItemElements(DesktopManager desktop)
    {
        AutomationElement[] listItems = Element.FindAllDescendants(Element.ConditionFactory.ByControlType(ControlType.ListItem));
        if (listItems.Length > 0)
        {
            return listItems;
        }

        listItems = desktop.GetContextMenuItems();
        if (listItems.Length > 0)
        {
            return listItems;
        }

        return desktop.GetPopupListElements();
    }

    public void LoadListItemAttributes()
    {
        Attributes = new DesktopData[2];
        try
        {
            Attributes[0] = new DesktopData("text", TruncString(Element.Name, 52));
            Attributes[1] = new DesktopData("value", TruncString(Element.Patterns.LegacyIAccessible.Pattern.Value, 52));
        }
        catch { }
    }

    private string TruncString(string myStr, int THRESHOLD)
    {
        if (myStr.Length > THRESHOLD)
            return myStr.Substring(0, THRESHOLD) + "...";
        return myStr;
    }


    //-----------------------------------------------------------------------------------------
    // Find element from AtsElement
    //-----------------------------------------------------------------------------------------

    public AtsElement FindElement(string tag, string propertyName, string propertyValue, DesktopManager desktop)
    {
        HashSet<AtsElement> elems = GetElements(false, tag, new string[] { propertyName }, Element, desktop);
        foreach (AtsElement elem in elems)
        {
            if (elem.Attributes.Length > 0 && propertyValue.Equals(elem.Attributes[0].Value))
            {
                return elem;
            }
        }
        return null;
    }

    //-----------------------------------------------------------------------------------------
    // Select
    //-----------------------------------------------------------------------------------------

    public void SafeClick()
    {
        try
        {
            Element.Click();
        }
        catch
        {
            SelectOrClick(Element);
        }
    }

    private AutomationElement SelectFirstItem(DesktopManager desktop)
    {
        ExpandElement();
        Thread.Sleep(100);

        AutomationElement[] items = GetListItemElements(desktop);
        for (int i = 0; i < items.Length; i++)
        {
            Keyboard.Type(new[] { VirtualKeyShort.UP });
            //Keyboard.Type(new[] { VirtualKeyShort.PRIOR, VirtualKeyShort.HOME });
        }

        return GetSelectedItem(null, desktop);
    }

    private AutomationElement GetSelectedItem(AutomationElement current, DesktopManager desktop)
    {
        Thread.Sleep(136);

        AutomationElement[] items = GetListItemElements(desktop);
        foreach (AutomationElement item in items)
        {
            try
            {
                AccessibilityState state = item.Patterns.LegacyIAccessible.Pattern.State.Value;

                bool isSelectedOrFocused = false;
                try
                {
                    isSelectedOrFocused = state.HasFlag(AccessibilityState.STATE_SYSTEM_SELECTED);
                }
                catch { }

                if (!isSelectedOrFocused)
                {
                    try
                    {
                        isSelectedOrFocused = state.HasFlag(AccessibilityState.STATE_SYSTEM_FOCUSED);
                    }
                    catch { }
                }

                if (isSelectedOrFocused)
                {
                    if (item.Equals(current))
                    {
                        return null;
                    }
                    return item;
                }
            }
            catch { }
        }
        return null;
    }

    internal string SelectItem(int index, DesktopManager desktop)
    {
        AutomationElement currentItem = SelectFirstItem(desktop);

        int currentIndex = 0;
        while (currentItem != null)
        {
            if (currentIndex == index)
            {
                SelectOrClick(currentItem);
                return null;
            }

            Keyboard.Type(VirtualKeyShort.DOWN);
            currentItem = GetSelectedItem(currentItem, desktop);

            currentIndex++;
        }
        return "item index not found";
    }

    private static void SelectOrClick(AutomationElement item)
    {
        try
        {
            Rectangle rec = item.BoundingRectangle;
            Mouse.Position = new Point((rec.Width / 2) + rec.X, (rec.Height / 2) + rec.Y);
            Mouse.Click();
        }
        catch (Exception)
        {
            Keyboard.Type(VirtualKeyShort.ENTER);
        }
    }

    internal string SelectItem(Predicate<AutomationElement> predicate, DesktopManager desktop)
    {
        AutomationElement currentItem = SelectFirstItem(desktop);

        while (currentItem != null)
        {
            if (predicate(currentItem))
            {
                SelectOrClick(currentItem);
                return null;
            }

            Keyboard.Type(VirtualKeyShort.DOWN);
            currentItem = GetSelectedItem(currentItem, desktop);
        }

        return "item not found";
    }

    internal bool TryExpand()
    {
        Element.FocusNative();
        Thread.Sleep(100);

        try
        {
            if (Element.Patterns.ExpandCollapse.IsSupported)
            {
                Element.Patterns.ExpandCollapse.Pattern.Expand();
                Thread.Sleep(140);
                return true;
            }
        }
        catch { }

        return false;
    }

    private bool ExpandElement()
    {
        Element.FocusNative();
        Thread.Sleep(100);

        if (Element.Patterns.ExpandCollapse.IsSupported)
        {
            Element.Patterns.ExpandCollapse.Pattern.Expand();
            Thread.Sleep(140);
            return true;
        }
        else
        {
            AutomationElement dropDown = Element.FindFirstChild(
                        new OrCondition(
                            Element.ConditionFactory.ByControlType(ControlType.Button),
                            Element.ConditionFactory.ByControlType(ControlType.SplitButton)));

            if (dropDown != null)
            {
                Mouse.Position = dropDown.GetClickablePoint();
                Mouse.LeftClick();
            }
            else
            {
                Element.Click();
            }
        }

        return false;
    }

    //-----------------------------------------------------------------------------------------
    // Script
    //-----------------------------------------------------------------------------------------

    private static readonly ExecuteFunction FocusFunction = new ExecuteFunction("Focus");
    private static readonly ExecuteFunction InvokeFunction = new ExecuteFunction("Invoke");
    private static readonly ExecuteFunction SetValueFunction = new ExecuteFunction("SetValue");
    private static readonly ExecuteFunction SelectTextFunction = new ExecuteFunction("SelectText");
    private static readonly ExecuteFunction SelectIndexFunction = new ExecuteFunction("SelectIndex");
    private static readonly ExecuteFunction SelectTextItemFunction = new ExecuteFunction("SelectTextItem");
    private static readonly ExecuteFunction SelectDataItemFunction = new ExecuteFunction("SelectDataItem");

    private class ExecuteFunction
    {
        private string value;
        private readonly string name;
        private readonly int nameLength = 0;

        public ExecuteFunction(string n)
        {
            name = n + "(";
            nameLength = n.Length + 1;
        }

        public bool Match(string data)
        {
            if (data.StartsWith(name, StringComparison.OrdinalIgnoreCase))
            {
                int closeParenthesis = data.LastIndexOf(")");
                if (closeParenthesis >= nameLength)
                {
                    value = data.Substring(nameLength, closeParenthesis - nameLength);
                    return true;
                }
            }
            return false;
        }

        public string ValueAsString()
        {
            return value;
        }

        public int ValueAsInt()
        {
            _ = int.TryParse(value, out int index);
            return index;
        }
    }

    internal void ExecuteScript(string script)
    {
        LoadProperties();

        if (script != null)
        {
            if (InvokeFunction.Match(script))
            {
                Invoke();
            }
            else if (FocusFunction.Match(script))
            {
                Focus();
            }
            else if (SetValueFunction.Match(script))
            {
                if (Element.Patterns.Value.IsSupported)
                {
                    Element.Patterns.Value.Pattern.SetValue(SetValueFunction.ValueAsString());
                }
            }
            else if (SelectTextFunction.Match(script))
            {
                SelectText(SelectTextFunction.ValueAsString());
            }
            else if (SelectIndexFunction.Match(script))
            {
                SelectText(SelectTextFunction.ValueAsInt());
            }
            else if (SelectTextItemFunction.Match(script))
            {
                string text = SelectTextItemFunction.ValueAsString();

                AutomationElement windowParent = Element.Parent;
                while (windowParent != null)
                {
                    if (windowParent.Properties.ControlType.IsSupported && windowParent.ControlType.Equals(ControlType.Window))
                    {
                        ExpandElement();
                        AutomationElement[] listItems = windowParent.FindAllDescendants(windowParent.ConditionFactory.ByControlType(ControlType.ListItem));

                        foreach (AutomationElement item in listItems)
                        {
                            ListBoxItem listItem = item.AsListBoxItem();
                            if (listItem.Name.Equals(text))
                            {
                                listItem.ScrollIntoView();

                                Rectangle rect = listItem.BoundingRectangle;
                                Mouse.Position = new Point(rect.X + (rect.Width / 2), rect.Y + (rect.Height / 2));
                                Mouse.Click();

                                break;
                            }
                        }
                        break;
                    }
                    windowParent = windowParent.Parent;
                }
            }
            else if (SelectDataItemFunction.Match(script))
            {
                string text = SelectDataItemFunction.ValueAsString();
                AutomationElement[] rows = Element.FindAllChildren();

                foreach (AutomationElement row in rows)
                {
                    AutomationElement[] cells = row.FindAllDescendants(row.ConditionFactory.ByControlType(ControlType.DataItem));
                    if (cells.Length > 0)
                    {
                        AutomationElement cell = cells[1];
                        cell.Focus();
                        cell.Click();
                        Thread.Sleep(50);
                        if (cell.Name.Contains(text))
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    public virtual void Focus()
    {
        AutomationElement parent = Element;
        while (!parent.Patterns.Window.IsSupported && parent.Parent != null)
        {
            parent = parent.Parent;
        }

        if (parent.Patterns.Window.IsSupported)
        {
            try
            {
                parent.AsWindow().WaitUntilEnabled(TimeSpan.FromMilliseconds(10000d));
            }
            catch { }
        }

        Element.Focus();
        Element.FocusNative();
    }

    public virtual void Invoke()
    {
        if (Element.Patterns.Invoke.IsSupported)
        {
            Element.Patterns.Invoke.Pattern.Invoke();
        }
    }

    public virtual void SelectText(string value)
    {
        ExpandElement();
        Element.AsComboBox().Select(value);
        Thread.Sleep(140);
        Element.AsComboBox().SelectedItem.Click();
    }

    public virtual void SelectText(int index)
    {
        ExpandElement();
        Element.AsComboBox().Select(index);
        Thread.Sleep(140);
        Element.AsComboBox().SelectedItem.Click();
    }

    //-----------------------------------------------------------------------------------------
    // Text
    //-----------------------------------------------------------------------------------------

    public void TextClear()
    {
        Element.FocusNative();

        try
        {
            Element.AsTextBox().Text = "";
            Keyboard.Type(new[] { VirtualKeyShort.SPACE, VirtualKeyShort.BACK });
            return;
        }
        catch { }

        try
        {
            Keyboard.TypeSimultaneously(new[] { VirtualKeyShort.CONTROL, VirtualKeyShort.KEY_A });
            Keyboard.Type(new[] { VirtualKeyShort.SPACE, VirtualKeyShort.BACK });
        }
        catch { }

        /*if (Element.Properties.IsKeyboardFocusable.IsSupported && Element.Properties.IsKeyboardFocusable)
        {
            
            AccessibilityState state = Element.Patterns.LegacyIAccessible.Pattern.State.Value;
            if (state.HasFlag(AccessibilityState.STATE_SYSTEM_FOCUSED))
            {
                try
                {
                    Keyboard.Type(new[] { VirtualKeyShort.SPACE, VirtualKeyShort.BACK });
                }
                catch { }
            }
        }*/
    }

    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------

    public static string GetTag(AutomationElement elem)
    {
        try
        {
            return elem.Properties.ControlType.ToString();
        }
        catch { }
        return UnknownTag;
    }

    public static bool IsPassword(AutomationElement elem)
    {
        try
        {
            return elem.Properties.IsPassword;
        }
        catch { }
        return false;
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    public virtual AtsElement[] GetElementsTree(DesktopManager desktop)
    {
        return new AtsElement[0];
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    private static HashSet<AtsElement> elements;
    public virtual HashSet<AtsElement> GetElements(bool isWeb, string tag, string[] attributes, AutomationElement root, DesktopManager desktop)
    {
        elements = desktop.GetPopupsElements();

        if ("*".Equals(tag) || string.IsNullOrEmpty(tag))
        {
            LoadDescendants(elements, isWeb, root);
        }
        else
        {
            LoadDescendantsByControleType(elements, isWeb, root, tag);
        }

        elements.RemoveWhere(e => e.Visible == false);

        int len = attributes.Length;

        if (len > 0)
        {
            string[] newAttributes = new string[len];
            for (int i = 0; i < len; i++)
            {
                string[] attributeData = attributes[i].Split('\t');
                newAttributes[i] = attributeData[0];
            }
            elements.AsParallel().ForAll(e => e.CalculateAttributes(newAttributes));
        }

        return elements;
    }

    public static void LoadDescendants(HashSet<AtsElement> items, bool isWeb, AutomationElement root)
    {
        ChildWalker(items, isWeb, root);
    }

    private static void LoadDescendantsByControleType(HashSet<AtsElement> items, bool isWeb, AutomationElement root, String tag)
    {
        if (UnknownTag.Equals(tag))
        {
            UndefinedChildWalker(items, isWeb, root);
        }
        else
        {
            ChildWalker(items, isWeb, root, tag);
        }
    }

    private static void ChildWalker(HashSet<AtsElement> list, bool isWeb, AutomationElement parent, String tag)
    {
        AutomationElement[] children = parent.FindAllChildren();
        AutomationElement child;

        for (int i = 0; i < children.Length; i++)
        {
            child = children[i];
            if (tag.Equals(GetTag(child)))
            {
                list.Add(new AtsElement(child));
            }
            ChildWalker(list, isWeb, children[i], tag);
        }
    }

    private static void UndefinedChildWalker(HashSet<AtsElement> list, bool isWeb, AutomationElement parent)
    {
        AutomationElement[] children = parent.FindAllChildren();
        foreach (AutomationElement child in children)
        {
            if (!child.Properties.ControlType.IsSupported)
            {
                list.Add(new AtsElement(child));
            }
            UndefinedChildWalker(list, isWeb, child);
        }
    }

    static void ChildWalker(HashSet<AtsElement> list, bool isWeb, AutomationElement parent)
    {
        AutomationElement[] children = parent.FindAllChildren();
        foreach (AutomationElement child in children)
        {
            list.Add(new AtsElement(child));
            ChildWalker(list, isWeb, child);
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------

    private static readonly string[] propertiesBase = new string[] {
                Properties.Name,
                Properties.NativeWindowHandle,
                Properties.AutomationId,
                Properties.ClassName,
                Properties.HelpText,
                Properties.ItemStatus,
                Properties.ItemType,
                Properties.AriaRole,
                Properties.AriaProperties,
                Properties.AcceleratorKey,
                Properties.AccessKey,
                Properties.IsEnabled,
                Properties.IsPassword,
                Properties.IsVisible,
                Properties.Text,
                Properties.Value,
                Properties.IsReadOnly,
                Properties.SelectedItems,
                Properties.IsSelected,
                Properties.Toggle,
                Properties.RangeValue,
                Properties.HorizontalScrollPercent,
                Properties.VerticalScrollPercent,
                Properties.FillColor,
                Properties.FillPatternColor,
                Properties.FillPatternStyle,
                Properties.AnnotationTypeName,
                Properties.Author,
                Properties.DateTime,
                Properties.ChildId,
                Properties.Description,
                Properties.Help,
                Properties.BoundingX,
                Properties.BoundingY,
                Properties.BoundingWidth,
                Properties.BoundingHeight,
                Properties.BoundingRectangle};

    private static readonly string[] propertiesGrid = new List<string>(propertiesBase) { Properties.RowCount, Properties.ColumnCount }.ToArray();
    private static readonly string[] propertiesWindow = new List<string>(propertiesBase) { Properties.ProcessId }.ToArray();

    public void LoadProperties()
    {
        if (GridTag.Equals(Tag))
        {
            Attributes = Properties.AddProperties(propertiesGrid, Password, Visible, Element);
        }
        else if (WindowTag.Equals(Tag))
        {
            Attributes = Properties.AddProperties(propertiesWindow, Password, Visible, Element);
        }
        else
        {
            Attributes = Properties.AddProperties(propertiesBase, Password, Visible, Element);
        }

    }

    internal DesktopData[] GetProperty(string name)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();

        if (name == null)
        {
            return Attributes;
        }

        foreach (DesktopData data in Attributes)
        {
            if (data.Match(name))
            {
                return new DesktopData[] { data };
            }
        }

        return new DesktopData[0];
    }

    //-----------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------

    internal AtsElement[] GetParents()
    {
        bool recalc = false;
        LoadProperties();
        Stack<AtsElement> parents = new Stack<AtsElement>();

        AutomationElement parent = Element.Parent;
        while (parent != null)
        {
            AtsElement parentElement = new AtsElement(parent);
            parentElement.LoadProperties();

            if (recalc)
            {
                AtsElement lastElem = parents.Pop();
                lastElem.X = parentElement.X;
                lastElem.Y = parentElement.Y;
                lastElem.Height = parentElement.Height;
                lastElem.Width = parentElement.Width;

                if (lastElem.Attributes.Length >= 24)
                {
                    lastElem.Attributes[19].SetValue(parentElement.X);
                    lastElem.Attributes[20].SetValue(parentElement.Y);
                    lastElem.Attributes[21].SetValue(parentElement.Width);
                    lastElem.Attributes[22].SetValue(parentElement.Height);
                    lastElem.Attributes[23].SetValue(parentElement.X + "," + parentElement.Y + "," + parentElement.Width + "," + parentElement.Height);
                }

                parents.Push(lastElem);
            }

            parents.Push(parentElement);
            parent = parent.Parent;

            recalc = (parentElement.X == 1 && parentElement.Y == 1 && parentElement.Height == 1 && parentElement.Width == 1);
        }

        if (parents.Count > 0)
        {
            parents.Pop();// remove Windows desktop
        }

        if (parents.Count > 0)
        {
            parents.Pop();// remove main windows of application
        }

        //TODO get full innertext

        return parents.ToArray();
    }

    private static class Properties
    {
        public const string Name = "Name";
        public const string NativeWindowHandle = "NativeWindowHandle";
        public const string AutomationId = "AutomationId";
        public const string ClassName = "ClassName";
        public const string HelpText = "HelpText";
        public const string ItemStatus = "ItemStatus";
        public const string ItemType = "ItemType";
        public const string AriaRole = "AriaRole";
        public const string AriaProperties = "AriaProperties";
        public const string AcceleratorKey = "AcceleratorKey";
        public const string AccessKey = "AccessKey";
        public const string IsEnabled = "IsEnabled";
        public const string IsPassword = "IsPassword";
        public const string IsVisible = "IsVisible";
        public const string Text = "Text";
        public const string Value = "Value";
        public const string IsReadOnly = "IsReadOnly";
        public const string IsSelected = "IsSelected";
        public const string SelectedItems = "SelectedItems";
        public const string Toggle = "Toggle";
        public const string RangeValue = "RangeValue";
        public const string HorizontalScrollPercent = "HorizontalScrollPercent";
        public const string VerticalScrollPercent = "VerticalScrollPercent";
        public const string FillColor = "FillColor";
        public const string FillPatternColor = "FillPatternColor";
        public const string FillPatternStyle = "FillPatternStyle";
        public const string AnnotationTypeName = "AnnotationTypeName";
        public const string Author = "Author";
        public const string DateTime = "DateTime";
        public const string ColumnCount = "ColumnCount";
        public const string RowCount = "RowCount";
        public const string ProcessId = "ProcessId";
        public const string ChildId = "ChildId";
        public const string Description = "Description";
        public const string Help = "Help";
        public const string BoundingX = "BoundingX";
        public const string BoundingY = "BoundingY";
        public const string BoundingWidth = "BoundingWidth";
        public const string BoundingHeight = "BoundingHeight";
        public const string BoundingRectangle = "BoundingRectangle";

        public static DesktopData[] AddProperties(string[] attributes, bool isPassword, bool isVisible, AutomationElement element)
        {
            List<DesktopData> result = new List<DesktopData>();

            FrameworkAutomationElementBase.IProperties propertyValues = element.Properties;
            FrameworkAutomationElementBase.IFrameworkPatterns patternValues = element.Patterns;

            for (int i = 0; i < attributes.Length; i++)
            {
                AddProperty(attributes[i], isPassword, isVisible, element, propertyValues, patternValues, ref result);
            }

            return result.ToArray();
        }

        public static string GetName(FrameworkAutomationElementBase.IProperties prop, FrameworkAutomationElementBase.IFrameworkPatterns pat)
        {
            try
            {
                return prop.Name.Value;
            }
            catch { }

            try
            {
                return pat.LegacyIAccessible.Pattern.Name.Value;
            }
            catch { }

            return "";
        }

        public static void AddProperty(string propertyName, bool isPassword, bool isVisible, AutomationElement element, FrameworkAutomationElementBase.IProperties propertyValues, FrameworkAutomationElementBase.IFrameworkPatterns patternValues, ref List<DesktopData> properties)
        {
            switch (propertyName)
            {
                case BoundingX:
                case BoundingY:
                case BoundingWidth:
                case BoundingHeight:
                case BoundingRectangle:
                    Rectangle rect = element.BoundingRectangle;
                    if (BoundingRectangle.Equals(propertyName))
                    {
                        StringBuilder sb = null;
                        if (rect.X == 0 && rect.Y == 0 && rect.Width == 0 && rect.Height == 0)
                        {
                            sb = new StringBuilder().Append(element.Parent.BoundingRectangle.X).Append(",").Append(element.Parent.BoundingRectangle.Y).Append(",").Append(element.Parent.BoundingRectangle.Width).Append(",").Append(element.Parent.BoundingRectangle.Height);
                        }
                        else
                        {
                            sb = new StringBuilder().Append(rect.X).Append(",").Append(rect.Y).Append(",").Append(rect.Width).Append(",").Append(rect.Height);
                        }
                        properties.Add(new DesktopData(BoundingRectangle, sb.ToString()));
                    }
                    else if (BoundingX.Equals(propertyName))
                    {
                        if (rect.X == 0 && rect.Y == 0 && rect.Width == 0 && rect.Height == 0)
                        {
                            properties.Add(new DesktopData(BoundingX, element.Parent.BoundingRectangle.X));
                        } 
                        else
                        {
                            properties.Add(new DesktopData(BoundingX, rect.X));
                        }
                    }
                    else if (BoundingY.Equals(propertyName))
                    {
                        if (rect.X == 0 && rect.Y == 0 && rect.Width == 0 && rect.Height == 0)
                        {
                            properties.Add(new DesktopData(BoundingY, element.Parent.BoundingRectangle.Y));
                        }
                        else
                        {
                            properties.Add(new DesktopData(BoundingY, rect.Y));
                        }
                    }
                    else if (BoundingWidth.Equals(propertyName))
                    {
                        if (rect.X == 0 && rect.Y == 0 && rect.Width == 0 && rect.Height == 0)
                        {
                            properties.Add(new DesktopData(BoundingWidth, element.Parent.BoundingRectangle.Width));
                        }
                        else
                        {
                            properties.Add(new DesktopData(BoundingWidth, rect.Width));
                        }
                    }
                    else if (BoundingHeight.Equals(propertyName))
                    {
                        if (rect.X == 0 && rect.Y == 0 && rect.Width == 0 && rect.Height == 0)
                        {
                            properties.Add(new DesktopData(BoundingHeight, element.Parent.BoundingRectangle.Height));
                        }
                        else
                        {
                            properties.Add(new DesktopData(BoundingHeight, rect.Height));
                        }
                    }
                    break;
                case Name:
                    properties.Add(new DesktopData(propertyName, GetName(propertyValues, patternValues)));
                    break;
                case ProcessId:
                    if (propertyValues.ProcessId.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, propertyValues.ProcessId.ToString()));
                    }
                    break;
                case NativeWindowHandle:
                    if (propertyValues.NativeWindowHandle.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, propertyValues.NativeWindowHandle.ToString()));
                    }
                    break;
                case AutomationId:
                    CheckProperty(propertyName, propertyValues.AutomationId, ref properties);
                    break;
                case ClassName:
                    CheckProperty(propertyName, propertyValues.ClassName, ref properties);
                    break;
                case HelpText:
                    CheckProperty(propertyName, propertyValues.HelpText, ref properties);
                    break;
                case ItemStatus:
                    CheckProperty(propertyName, propertyValues.ItemStatus, ref properties);
                    break;
                case ItemType:
                    CheckProperty(propertyName, propertyValues.ItemType, ref properties);
                    break;
                case AriaRole:
                    CheckProperty(propertyName, propertyValues.AriaRole, ref properties);
                    break;
                case AriaProperties:
                    CheckProperty(propertyName, propertyValues.AriaProperties, ref properties);
                    break;
                case AcceleratorKey:
                    CheckProperty(propertyName, propertyValues.AcceleratorKey, ref properties);
                    break;
                case AccessKey:
                    CheckProperty(propertyName, propertyValues.AccessKey, ref properties);
                    break;
                case IsEnabled:
                    properties.Add(new DesktopData(propertyName, element.Properties.IsEnabled));
                    break;
                case IsPassword:
                    properties.Add(new DesktopData(propertyName, isPassword));
                    break;
                case IsVisible:
                    properties.Add(new DesktopData(propertyName, isVisible));
                    break;
                case RowCount:
                    properties.Add(new DesktopData(propertyName, element.AsGrid().RowCount));
                    break;
                case ColumnCount:
                    properties.Add(new DesktopData(propertyName, element.AsGrid().ColumnCount));
                    break;
                case Text:

                    string txt;
                    try
                    {
                        txt = patternValues.Text.Pattern.DocumentRange.GetText(999999999);
                        txt = patternValues.Text2.Pattern.DocumentRange.GetText(999999999);
                    }
                    catch
                    {
                        break;
                    }

                    properties.Add(new DesktopData(propertyName, txt));

                    break;
                case Value:
                    if (isPassword)
                    {
                        properties.Add(new DesktopData(propertyName, "#PASSWORD_VALUE#"));
                    }
                    else if (patternValues.Value.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Value.Pattern.Value.ValueOrDefault));
                    }
                    else if (patternValues.LegacyIAccessible.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.LegacyIAccessible.Pattern.Value.ValueOrDefault));
                    }
                    break;
                case IsReadOnly:
                    if (patternValues.Value.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Value.Pattern.IsReadOnly));
                    }
                    break;
                case SelectedItems:
                    if (patternValues.Selection.IsSupported)
                    {
                        List<string> items = new List<string>();
                        foreach (AutomationElement item in patternValues.Selection.Pattern.Selection.ValueOrDefault)
                        {
                            if (item.Patterns.SelectionItem.IsSupported && item.Properties.Name.IsSupported)
                            {
                                items.Add(item.Name);
                            }
                        }
                        properties.Add(new DesktopData(propertyName, String.Join(",", items)));
                    }
                    break;
                case IsSelected:

                    if (patternValues.SelectionItem.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.SelectionItem.Pattern.IsSelected));
                    }
                    break;
                case Toggle:
                    if (patternValues.Toggle.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Toggle.Pattern.ToggleState.ToString()));
                    }
                    break;
                case RangeValue:
                    if (patternValues.RangeValue.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, Convert.ToInt32(patternValues.RangeValue.Pattern.Value)));
                    }
                    break;
                case HorizontalScrollPercent:
                    if (patternValues.Scroll.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, Convert.ToInt32(patternValues.Scroll.Pattern.HorizontalScrollPercent.ValueOrDefault)));
                    }
                    break;
                case VerticalScrollPercent:
                    if (patternValues.Scroll.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, Convert.ToInt32(patternValues.Scroll.Pattern.VerticalScrollPercent.ValueOrDefault)));
                    }
                    break;
                case FillColor:
                    if (patternValues.Styles.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Styles.Pattern.FillColor.ValueOrDefault));
                    }
                    break;
                case FillPatternColor:
                    if (patternValues.Styles.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Styles.Pattern.FillPatternColor.ValueOrDefault));
                    }
                    break;
                case FillPatternStyle:
                    if (patternValues.Styles.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Styles.Pattern.FillPatternStyle.ValueOrDefault));
                    }
                    break;
                case AnnotationTypeName:
                    if (patternValues.Annotation.IsSupported && patternValues.Annotation.Pattern.AnnotationTypeName.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Annotation.Pattern.AnnotationTypeName));
                    }
                    break;
                case Author:
                    if (patternValues.Annotation.IsSupported && patternValues.Annotation.Pattern.Author.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Annotation.Pattern.Author));
                    }
                    break;
                case DateTime:
                    if (patternValues.Annotation.IsSupported && patternValues.Annotation.Pattern.DateTime.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.Annotation.Pattern.DateTime));
                    }
                    break;
                case ChildId:
                    if (patternValues.LegacyIAccessible.IsSupported && patternValues.LegacyIAccessible.Pattern.ChildId.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.LegacyIAccessible.Pattern.ChildId));
                    }
                    break;
                case Description:
                    if (patternValues.LegacyIAccessible.IsSupported && patternValues.LegacyIAccessible.Pattern.Description.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.LegacyIAccessible.Pattern.Description));
                    }
                    break;
                case Help:
                    if (patternValues.LegacyIAccessible.IsSupported && patternValues.LegacyIAccessible.Pattern.Help.IsSupported)
                    {
                        properties.Add(new DesktopData(propertyName, patternValues.LegacyIAccessible.Pattern.Help));
                    }
                    break;
            }
        }

        private static void CheckProperty(string name, AutomationProperty<string> property, ref List<DesktopData> properties)
        {
            try
            {
                properties.Add(new DesktopData(name, property.ValueOrDefault));
            }
            catch { }
        }
    }
}