﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Definitions;
using FlaUI.Core.Input;
using FlaUI.Core.Patterns;
using FlaUI.Core.WindowsAPI;
using FlaUI.UIA3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using windowsdriver;
using windowsdriver.utils;
using static FlaUI.Core.FrameworkAutomationElementBase;

[DataContract(Name = "com.ats.executor.drivers.desktop.DesktopWindow")]
public class DesktopWindow : AtsElement
{
    [JsonProperty(PropertyName = "pid")]
    [DataMember(Name = "pid")]
    public int Pid { get; set; }

    [JsonProperty(PropertyName = "handle")]
    [DataMember(Name = "handle")]
    public int Handle { get; set; }

    [JsonProperty(PropertyName = "app", NullValueHandling = NullValueHandling.Include)]
    [DataMember(Name = "app", IsRequired = true)]
    public AppData App { get; set; }

    public readonly bool isIE = false;
    public readonly bool isWebBrowser = false;

    private const string MAXIMIZE = "maximize";
    private const string REDUCE = "reduce";
    private const string RESTORE = "restore";
    private const string CLOSE = "close";

    protected bool isWindow = false;
    protected bool isDialog = false;

    private bool isMaximized = false;

    public DesktopWindow(DesktopManager desktop) : base(desktop)
    {
        Tag = "Window";
        isWindow = true;
        isDialog = false;
        isWebBrowser = true;

        Pid = 0;
        Handle = 0;
    }

    public DesktopWindow(AutomationElement elem, Rectangle deskRect) : base(elem)
    {
        Tag = @"Desktop";
        App = new AppData();
    }

    public DesktopWindow(AutomationElement elem) : this(elem, "_"){
        App = new AppData(elem);
    }

    public DesktopWindow(AutomationElement elem, string appName) : base(elem)
    {
        Tag = "Window";

        IProperties elementProperties = null;
        int pid = 0;
        IntPtr handle = IntPtr.Zero;

        int maxTry = 20;

        while (elementProperties == null && pid == 0 && handle == IntPtr.Zero && maxTry > 0)
        {
            try
            {
                elementProperties = Element.Properties;
                pid = elementProperties.ProcessId;
                handle = elementProperties.NativeWindowHandle;
            }
            catch
            {
                elementProperties = null;
                handle = IntPtr.Zero;
                pid = 0;

                Thread.Sleep(100);
                maxTry--;
            }
        }

        //-----------------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------------------------

        Pid = pid;
        Handle = handle.ToInt32();
        App = new AppData(appName);

        try
        {
            isDialog = Element.Properties.IsDialog;
        }
        catch { }

        //-----------------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------------------------

        if (elem.Patterns.Window.IsSupported)
        {
            isWindow = true;
            elementProperties.ClassName.TryGetValue(out string className);

            if ("IEFrame".Equals(className))
            {
                isIE = true;
            }
            else if ("MozillaWindowClass".Equals(className) || className.StartsWith("Chrome_WidgetWin"))
            {
                isWebBrowser = true;
            }

            try
            {
                Thread.Sleep(200);
                IWindowPattern pattern = elem.Patterns.Window.Pattern;
                isMaximized = WindowVisualState.Maximized == pattern.WindowVisualState.Value;
            }
            catch { }

            Attributes = new DesktopData[1];
            Attributes[0] = new DesktopData("Title", elem.Name);
        }
    }

    public void UpdateProcessData()
    {
        Process proc = Process.GetProcessById(Pid);
        UpdateApplicationData(proc);
    }

    internal void UpdateApplicationData(Process process)
    {
        App = new AppData(process);
    }

    public void CloseModalWindows()
    {
        int maxtry = 10;
        Window[] wins = Element.AsWindow().ModalWindows;
        while (wins != null && wins.Length > 0)
        {
            CloseModalWindows(wins);
            wins = Element.AsWindow().ModalWindows;
            maxtry--;
        }
    }

    private void CloseModalWindows(Window[] wins)
    {
        if (wins != null)
        {
            foreach (Window w in wins)
            {
                CloseModalWindows(w, w.ModalWindows);
            }
        }
    }

    private void CloseModalWindows(Window win, Window[] wins)
    {
        CloseModalWindows(wins);
        win.Close();
        Thread.Sleep(300);
    }

    public override AtsElement[] GetElementsTree(DesktopManager desktop)
    {
        Stack<AutomationElement> popupChildren = desktop.GetPopupDescendants(Element.Properties.ProcessId);
        Queue<AtsElement> listElements = new Queue<AtsElement> { };

        //---------------------------------------------------------------------
        // try to find a modal window
        //---------------------------------------------------------------------

        AutomationElement[] children = Element.FindAllChildren();
        for (int i = 0; i < children.Length; i++)
        {
            AutomationElement child = children[i];
            IFrameworkPatterns patterns = child.Patterns;
            if (patterns.Window.IsSupported && patterns.Window.Pattern.IsModal)
            {
                foreach (AutomationElement popup in popupChildren)
                {
                    listElements.Enqueue(new AtsElement(desktop, popup, null));
                }

                listElements.Enqueue(new AtsElement(desktop, child, null));

                return listElements.ToArray();
            }
            else
            {
                try
                {
                    if (((UIA3FrameworkAutomationElement)patterns).IsDialog)
                    {
                        listElements.Enqueue(new AtsElement(desktop, child, null));
                    }
                }
                catch { }
            }
        }

        foreach (AutomationElement child in popupChildren.Concat(children))
        {
            if (child.Properties.ClassName.TryGetValue(out string className))
            {
                if ("Intermediate D3D Window".Equals(className) || "MozillaCompositorWindowClass".Equals(className))
                {
                    continue;
                }
            }

            AtsElement elem = new AtsElement(desktop, child, null);
            if (elem.Visible)
            {
                listElements.Enqueue(elem);
            }
        }
        List<AtsElement> lst = listElements.ToList();
        lst.Sort(new SortByArea());

        return lst.ToArray();
    }

    public override HashSet<AtsElement> GetElements(bool isWeb, string tag, string[] attributes, AutomationElement root, DesktopManager desktop)
    {
        //---------------------------------------------------------------------
        // try to find a modal window
        //---------------------------------------------------------------------

        AutomationElement[] children = Element.FindAllChildren(Element.ConditionFactory.ByControlType(ControlType.Window));
        for (int i = 0; i < children.Length; i++)
        {
            AutomationElement child = children[i];
            if (child.Patterns.Window.IsSupported && child.Patterns.Window.Pattern.IsModal)
            {
                return base.GetElements(isWebBrowser, tag, attributes, child, desktop);
            }
        }

        //return base.GetElements(tag, attributes, Element, desktop, desktop.GetPopupDescendants(Element.Properties.ProcessId));
        return base.GetElements(isWebBrowser, tag, attributes, Element, desktop);
    }

    public virtual void Resize(int w, int h)
    {
        try
        {
            Element.Patterns.Transform.Pattern.Resize(w, h);
        }
        catch { }
    }

    public virtual void Move(int x, int y)
    {
        try
        {
            Element.Patterns.Transform.Pattern.Move(x, y);
        }
        catch { }
    }

    public virtual string CloseWindow()
    {
        try
        {
            Element.AsWindow().Close();
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return null;
    }

    public virtual void Close()
    {
        if (isWindow)
        {
            if (isIE)
            {
                AutomationElement[] tabs = Element.FindAll(TreeScope.Descendants, Element.ConditionFactory.ByControlType(ControlType.TabItem));
                if (tabs.Length > 1)
                {
                    for (int i = tabs.Length - 1; i > 0; i--)
                    {
                        AutomationElement tab = tabs[i];
                        tab.Patterns.SelectionItem.Pattern.Select();

                        int maxTry = 10;
                        while (maxTry > 0)
                        {
                            AutomationElement closeButton = tab.FindFirstChild(tab.ConditionFactory.ByControlType(ControlType.Button));
                            if (closeButton == null)
                            {
                                Thread.Sleep(350);
                                maxTry--;
                            }
                            else
                            {
                                closeButton.WaitUntilClickable(TimeSpan.FromMilliseconds(350.00));
                                closeButton.AsButton().Invoke();
                                maxTry = 0;
                            }
                        }
                    }
                }
            }

            try
            {
                CloseModalWindows();
                Element.AsWindow().Close();
            }
            catch { }

        }

        //Dispose();
    }

    /*private bool CloseModalPopups()
    {
        bool findModal = false;

        AutomationElement[] dialogs = desktop.GetDialogChildren(Pid);
        for (int i = 0; i < dialogs.Length; i++)
        {
            AutomationElement dialog = dialogs[i];
            if (!dialog.Equals(Element))
            {
                dialog.AsWindow().Close();
                findModal = true;
            }
        }

        if (Element.IsAvailable)
        {
            dialogs = Element.FindAllChildren(Element.ConditionFactory.ByControlType(ControlType.Window));
            for (int i = 0; i < dialogs.Length; i++)
            {
                AutomationElement dialog = dialogs[i];
                if (dialog.Patterns.Window.IsSupported && dialog.Patterns.Window.Pattern.IsModal)
                {
                    dialog.AsWindow().Close();
                    findModal = true;
                }
            }
        }
        
        return findModal;
    }*/

    public override void Focus()
    {
        Element.SetForeground();
        base.Focus();
    }

    public void GotoUrl(string s, DesktopManager desktop)
    {
        ToFront();
        //windows 10 and older
        AutomationElement toolBar = Element.FindFirst(TreeScope.Descendants, Element.ConditionFactory.ByControlType(ControlType.ToolBar).And(Element.ConditionFactory.ByAutomationId("1001")));
        if (toolBar != null)
        {
            toolBar.Click();

            AutomationElement addressBand = Element.FindFirst(TreeScope.Descendants, Element.ConditionFactory.ByControlType(ControlType.Pane).And(Element.ConditionFactory.ByClassName("Address Band Root")));
            if (addressBand != null)
            {
                AutomationElement editBox = addressBand.FindFirst(TreeScope.Descendants, toolBar.ConditionFactory.ByControlType(ControlType.Edit));
                if (editBox != null)
                {
                    editBox.AsTextBox().Enter(s);
                    Keyboard.Type(VirtualKeyShort.ENTER);
                }
            }
            return;
        }

        //windows 11
        if (toolBar == null)
        {
            AtsElement elem = FindElement("Group", "AutomationId", "PART_AutoSuggestBox", desktop);
            if(elem != null)
            {
                elem = elem.FindElement("Edit", "AutomationId", "TextBox", desktop);
                if(elem != null)
                {
                    elem.Element.AsTextBox().Enter(s);
                    Thread.Sleep(500);
                    Keyboard.Type(VirtualKeyShort.ENTER);
                }
            }
        }
    }

    internal void Navigate(string s, DesktopManager desktop)
    {
        AtsElement elem = FindElement("AppBar", "AutomationId", "NavigationCommands", desktop);
        if (elem != null)
        {
            switch (s)
            {
                case WindowExecution.NEXT:

                    elem.FindElement("Button", "AutomationId", "forwardButton", desktop)?.SafeClick();
                    break;

                case WindowExecution.BACK:

                    elem.FindElement("Button", "AutomationId", "backButton", desktop)?.SafeClick();
                    break;

                case WindowExecution.REFRESH:

                    elem.FindElement("Button", "AutomationId", "refreshButton", desktop)?.SafeClick();
                    break;
            }
        }
    }

    public virtual void ToFront()
    {
        if (isWindow)
        {
            if (!HasModalChild() && Element.Patterns.Window.IsSupported && Element.Patterns.Window.Pattern.WindowVisualState.IsSupported)
            {
                double w = Element.AsWindow().ActualWidth;
                double h = Element.AsWindow().ActualHeight;

                if (isMaximized)
                {
                    ChangeState(WindowVisualState.Maximized);
                }
                else
                {
                    ChangeState(WindowVisualState.Normal);
                }

                if (Element.AsWindow().ActualWidth != w || Element.AsWindow().ActualHeight != h)
                {
                    Resize(Convert.ToInt32(w), Convert.ToInt32(h));
                }
            }

            Element.AsWindow().SetForeground();
            Element.AsWindow().Focus();
            Element.AsWindow().FocusNative();
        }
        else if (isDialog)
        {
            Element.SetForeground();
            Element.Focus();
            Element.FocusNative();
        }
    }

    private void ChangeState(WindowVisualState state)
    {
        try
        {
            Element.Patterns.Window.Pattern.SetWindowVisualState(state);
        }
        catch { }
    }

    public virtual void SetMouseFocus(DesktopManager desktop)
    {
        ToFront();
        desktop.Focus();

        Element.SetForeground();
        Element.FocusNative();

        Mouse.Position = new Point((int)X + 1, (int)Y + 1);
        Mouse.Down(MouseButton.Left);
        Thread.Sleep(250);
        Mouse.Up(MouseButton.Left);

        //Element.Click();
    }

    private bool HasModalChild()
    {
        AutomationElement[] children = Element.FindAllChildren(Element.ConditionFactory.ByControlType(ControlType.Window));
        for (int i = 0; i < children.Length; i++)
        {
            AutomationElement child = children[i];
            if (child.Patterns.Window.IsSupported && child.Patterns.Window.Pattern.IsModal)
            {
                return true;
            }
        }
        return false;
    }

    public virtual void ChangeState(string value)
    {
        if (isWindow)
        {
            switch (value)
            {
                case MAXIMIZE:
                    if (Element.Patterns.Window.Pattern.CanMaximize && !isMaximized)
                    {
                        Element.Patterns.Window.Pattern.SetWindowVisualState(WindowVisualState.Maximized);
                        isMaximized = true;
                    }
                    break;
                case REDUCE:
                    if (Element.Patterns.Window.Pattern.CanMinimize)
                    {
                        Element.Patterns.Window.Pattern.SetWindowVisualState(WindowVisualState.Minimized);
                        isMaximized = false;
                    }
                    break;
                case RESTORE:
                    Element.Patterns.Window.Pattern.SetWindowVisualState(WindowVisualState.Normal);
                    isMaximized = false;
                    break;
                case CLOSE:
                    Close();
                    break;
            }
        }
    }
}